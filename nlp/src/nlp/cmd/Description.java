package nlp.cmd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nlp.cmd.Argument.Required;

/**
 * Represents a description, which describes what a <code>Command</code> is, and how a <code>Command</code> is run.
 */
public class Description{
	
	private final String tab = "     ";
	
	private Command cmd;
	private String desc;
	private List<Argument> args;
	
	/**
	 * Constructs a <code>Description</code> based on the <code>Command</code>.
	 * 
	 * @param cmd The <code>Command</code> for which the description is being constructed
	 * @param desc The description of the <code>Command</code>
	 * @param args The arguments for the <code>Command</code>
	 * 
	 * @see nlp.cmd.Command
	 * @see nlp.cmd.Argument
	 */
	Description(Command cmd, String desc, Argument... args){
		this.cmd = cmd;
		this.desc = desc;
		if (args != null && args.length > 0) this.args = new ArrayList<Argument>(Arrays.asList(args));
	}
	
	/**
	 * Returns the description of the <code>Command</code>.
	 * 
	 * @return the actual description of the <code>Command</code>
	 */
	public String get(){
		return desc;
	}
	
	/**
	 * Returns the arguments of the <code>Command</code>.
	 * 
	 * @return a list of arguments for the <code>Command</code>
	 */
	public List<Argument> getArguments(){
		return args;
	}
	
	/**
	 * Returns a full description of the <code>Command</code>.
	 * 
	 * @return the full description of the <code>Command</code>
	 * 
	 * @see nlp.cmd.Command
	 * @see nlp.cmd.Argument
	 */
	public String toString(){
		String fullDesc = desc + "\n\n", fullOpts = "\n" + tab;
		List<String> aliases = cmd.getAliases();
		
		if (aliases != null && !aliases.isEmpty() && aliases.size() > 1){
			aliases.remove(0);
			if (cmd instanceof PermissionCommand){
				aliases.remove("promote");
				aliases.remove("demote");
			}
			fullDesc += "Aliases: " + aliases.toString() + "\n\n";
		}
		
		fullDesc += "Usage: " + cmd.getCommand();
		if (args != null && !args.isEmpty()){
			fullDesc += " ";
			
			for (int a = 0; a < args.size(); a++){
				Argument arg = args.get(a);
				Required requirement = arg.getRequirement();
				
				if (arg.getOptions().size() < 1) fullDesc += (requirement == Required.ALWAYS) ? "<"+arg.get()+"> " : 
						((requirement == Required.OPTIONAL) ? "["+arg.get()+"] " : "[<"+arg.get()+">] ");
				else fullDesc += (requirement == Required.ALWAYS) ? "<" : ((requirement == Required.OPTIONAL) ? "[" : "[<");
				fullOpts += arg.get()+" - "+arg.getDescription()+"\n"+tab;
				
				List<Option> opts = arg.getOptions();
				for (int o = 0; o < opts.size(); o++){
					fullDesc += ((o > 0) ? "/" : "") + opts.get(o).get();
					fullOpts += tab+opts.get(o).get()+" - "+opts.get(o).getDescription()+"\n"+tab;
				}
				
				if (arg.getOptions().size() > 0) fullDesc += (requirement == Required.ALWAYS) ? "> " : ((requirement == Required.OPTIONAL) ?  "] " : ">] ");
			}
		}
		fullDesc += fullOpts;
		return fullDesc;
	}
}
