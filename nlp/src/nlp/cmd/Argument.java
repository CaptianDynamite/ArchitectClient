package nlp.cmd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents an argument, which determines how a <code>Command</code> executes.
 */
public class Argument{

	private String arg;
	private String desc;
	private List<Option> opts;
	private Required req;
	
	/**
	 * Constructs a new <code>Argument</code> for a <code>Command</code>.
	 * 
	 * @param arg The name of the argument
	 * @param desc The description of the argument
	 * @param req Whether or not the argument is required
	 * 
	 * @see nlp.cmd.Description
	 */
	Argument(String arg, String desc, Required req, Option... opts){
		this.arg = arg;
		this.desc = desc;
		this.req = req;
		this.opts = new ArrayList<Option>(Arrays.asList(opts));
	}
	
	/**
	 * Returns the argument name.
	 * 
	 * @return the name of the argument
	 */
	public String get(){
		return arg;
	}
	
	/**
	 * Returns the argument description.
	 * 
	 * @return the description of the argument
	 */
	public String getDescription(){
		return desc;
	}
	
	/**
	 * Returns whether or not it's required.
	 * 
	 * @return whether or not the argument is required
	 */
	public Required getRequirement(){
		return req;
	}
	
	/**
	 * Returns a list of options.
	 * 
	 * @return a list of options for the argument
	 */
	public List<Option> getOptions(){
		return opts;
	}
	
	public enum Required{
		ALWAYS, SOMETIMES, OPTIONAL
	}
}
