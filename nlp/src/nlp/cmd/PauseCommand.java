package nlp.cmd;

import nlp.NLP;
import nlp.cmd.perm.Permissions;
import nlp.desktop.TaskBar;
import nlp.desktop.TaskBar.AppStatus;
import nlp.desktop.apps.Application;
import nlp.desktop.apps.Applications;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A <code>Command</code> that pauses a specified application.
 */
public class PauseCommand extends Command{
	
	/**
	 * Constructs a new <code>PauseCommand</code>.
	 *
	 * @param cmd The name of the <code>Command</code>
	 * @param maxArgs The maximum amount of arguments the <code>Command</code> can have
	 * @param allRequired Whether or not all of the arguments for a <code>Command</code> are required
	 * @param perm The required permission if any, to run the <code>Command</code>
	 * @param aliases Any aliases the <code>Command</code> possesses
	 *
	 * @see nlp.cmd.Command
	 * @see nlp.cmd.Commands
	 */
	PauseCommand(String cmd, int maxArgs, boolean allRequired, Permissions perm, String... aliases){
		super(cmd, maxArgs, allRequired, perm, aliases);
	}
	
	@Override
	void execute(String[] args) throws Exception{
		if (args.length == getMaxReqArgs()){
			Application app = Applications.byName(args[0]);
			
			if (app != null)
				TaskBar.togglePause(app);
			else System.err.println("Error: Invalid application!");
		}
	}
	
	@Override
	Description getDescription(){
		return new Description(this, "Toggles the pause status of the specified application.");
	}
	
	@Override
	public List<String> onTabComplete(String[] args){
		List<String> apps = new ArrayList<String>();
		
		for (Applications app : Applications.values()){
			AppStatus status = TaskBar.getStatus(app.getApp());
			
			if (!app.isHiddenApp() && status != null && status != AppStatus.HIDDEN) apps.add(app.name().toLowerCase());
		}
		Collections.sort(apps);
		return apps;
	}
}
