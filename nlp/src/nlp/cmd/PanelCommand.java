package nlp.cmd;

import java.util.ArrayList;
import java.util.List;

import nlp.NLP;
import nlp.cmd.Argument.Required;
import nlp.cmd.perm.Permissions;
import nlp.panel.Panels;

/**
 * A <code>Command</code> that switches to a specified panel.
 */
public class PanelCommand extends Command{

	/**
	 * Constructs a new <code>PanelCommand</code>.
	 * 
	 * @param cmd The name of the <code>Command</code>
	 * @param maxArgs The maximum amount of arguments the <code>Command</code> can have
	 * @param allRequired Whether or not all of the arguments for a <code>Command</code> are required
	 * @param perm The required permission if any, to run the <code>Command</code>
	 * @param aliases Any aliases the <code>Command</code> possesses
	 * 
	 * @see nlp.cmd.Command
	 * @see nlp.cmd.Commands
	 */
	PanelCommand(String cmd, int maxArgs, boolean allRequired, Permissions perm, String... aliases) {
		super(cmd, maxArgs, allRequired, perm, aliases);
	}

	@Override
	void execute(String[] args) throws Exception{
		try{
			Panels panel = Panels.valueOf(args[0].toUpperCase());
//			if (NLP.tabs.indexOfTab((args[0].substring(0, 1).toUpperCase() + args[0].toLowerCase().substring(1))) != -1) Panels.setPanel(panel);
//			else throw new IllegalArgumentException();
		}catch (IllegalArgumentException e){
			System.err.println("Error: Invalid panel!");
		}
	}

	@Override
	Description getDescription(){
		List<Option> options = new ArrayList<Option>();
		List<String> panels = onTabComplete(null);
		
		for (String panel : panels) options.add(new Option(panel, "The "+ panel + " panel."));
		return new Description(this, "Switches to the specified panel.", 
				new Argument("panel", "The panel to switch to.", Required.ALWAYS, options.toArray(new Option[options.size()])));
	}

	@Override
	public List<String> onTabComplete(String[] args){
		List<String> panels = new ArrayList<String>();
		
		for (Panels panel : Panels.values()){
//			if (panel != Panels.INVALID && panel != Panels.getCurrent() && NLP.tabs.indexOfTab((panel.name().substring(0, 1) + panel.name().substring(1).toLowerCase())) != -1)
//				panels.add(panel.name().toLowerCase());
		}
		return panels;
	}
}
