package nlp.cmd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nlp.cmd.perm.Permissions;
import nlp.net.Client;
import nlp.panel.ConsolePanel;
import nlp.panel.Panels;

/** 
 * A <code>Command</code> that runs the previously ran command with a yes or no argument. 
 */
public class YesNoCommand extends Command{

	@SuppressWarnings("serial")
	public static Map<String, Boolean> options = new HashMap<String, Boolean>(){{
		put("yes", true);
		put("y", true);
		put("no", false);
		put("n", false);
	}};
	
	/**
	 * Constructs a new <code>YesNoCommand</code>.
	 * 
	 * @param cmd The name of the <code>Command</code>
	 * @param maxArgs The maximum amount of arguments the <code>Command</code> can have
	 * @param allRequired Whether or not all of the arguments for a <code>Command</code> are required
	 * @param perm The required permission if any, to run the <code>Command</code>
	 * @param aliases Any aliases the <code>Command</code> possesses
	 * 
	 * @see nlp.cmd.Command
	 * @see nlp.cmd.Commands
	 */
	YesNoCommand(String cmd, int maxArgs, boolean allRequired, Permissions perm, String... aliases){
		super(cmd, maxArgs, allRequired, perm, aliases);
	}

	@Override
	void execute(String[] args) throws Exception{
		List<String> prevCommands = ((ConsolePanel) Panels.CONSOLE.get()).prevCommands;
		
		if (prevCommands.size() > 0){
			Commands prevCmd = Commands.byAlias(prevCommands.get(prevCommands.size()-1).split(" ")[0]);
			
			if (prevCmd != null){
				switch(prevCmd){
					case LOGIN:
						prevCmd.execute(args[0], Client.userToLogin, Client.userToLoginCode);
						return;
					default:
						break;
				}
			}
		}
		System.err.println("Error: Invalid command!");
	}

	@Override
	Description getDescription(){
		return new Description(this, "Runs the previously run command with a yes or no argument.");
	}

	@Override
	public List<String> onTabComplete(String[] args){
		return new ArrayList<String>(Arrays.asList("yes","no"));
	}
}
