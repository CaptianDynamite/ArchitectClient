package nlp.cmd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nlp.cmd.Argument.Required;
import nlp.cmd.perm.Permissions;
import nlp.cmd.perm.Ranks;
import nlp.net.Client;
import nlp.net.PermissionPacket;

public class PermissionCommand extends Command{

	PermissionCommand(String cmd, int maxArgs, int maxReq, boolean allRequired, long perms, String... aliases){
		super(cmd, maxArgs, maxReq, allRequired, perms, aliases);
	}

	@Override
	void execute(String[] args) throws Exception{
//		System.out.println(args.length);
		Permissions perm = Permissions.byName(args[0].toUpperCase());
		
		switch(perm){
			case PROMOTE: case DEMOTE:
				if (perm == Permissions.PROMOTE && !Client.hasPermission(Permissions.PROMOTE)){
					System.err.println("Error: Invalid command!");
					break;
				}
				
				if (args.length < 2) System.err.println("Error: Too few arguments!");
				else if (args.length > 2) System.err.println("Error: Too many arguments!");
				else{
					Ranks rank = getCurrentRank(), newRank;
					int offset = args[0].toLowerCase().equals("promote") ? 1 : ((rank.getIndex() > 0) ? -1 : 0);
					
					newRank = (Ranks.values()[rank.getIndex()+offset] != Ranks.INVALID) ? Ranks.values()[rank.getIndex()+offset] : rank;
//					System.out.println("newRank: " + newRank.getName());
					if (newRank != rank){
						if (offset > 0) Client.sendPacket(new PermissionPacket(args[1].replace("me", ""), (Client.permissions | newRank.getDefaultPermissions())));
						else Client.sendPacket(new PermissionPacket(args[1].replace("me", ""), (Client.permissions & (~newRank.getDefaultPermissions()))));
					}
				}
				break;
			default:
				if (args.length >= 2){
					switch(args[1].toLowerCase()){
						case "perms":
							if (args.length > 2) System.err.println("Error: Too many arguments!");
							else System.out.println(Permissions.getNames().toString().substring(1).replace("]", ""));
							break;
						case "group":
							if (args.length > 3) System.err.println("Error: Too many arguments!");
							else if (args.length < 3) System.err.println("Error: No group specified!");
							else{
								Ranks group = Ranks.byName(args[2]);
								
								if (group != Ranks.INVALID) System.out.println(group.getDefaultPermissionsList().toString().substring(1).replace("]", ""));
								else System.err.println("Error: Invalid group specified!");
							}
							break;
						case "user":
							switch(args.length){
								case 2:
									System.err.println("Error: No user specified!");
									break;
								case 3:
									/* TODO check for valid user */
									System.err.println("Error: No action specified!");
									break;
								case 4:
									switch(args[3].toLowerCase()){
										case "add": case "a":
										case "del": case "d":
											System.err.println("Error: No value specified!");
											break;
										case "list": case "l":
											/* TODO list the groups and perms of the user */
											break;
										default:
											System.err.println("Error: Invalid action!");
											break;
									}
									break;
								case 5:
									Ranks group = Ranks.byName(args[4]);
									perm = Permissions.byName(args[4]);
									
//									System.out.println("group = " + group.name() + " | perm = " + perm.name());
									switch(args[3].toLowerCase()){
										case "add": case "a":
											if (group != Ranks.INVALID){
												if (!Client.hasPermissibleRank(group)) 
													Client.sendPacket(new PermissionPacket(args[2].replace("me", ""), (Client.permissions | group.getDefaultPermissions())));
											}else if (perm != Permissions.INVALID){
												if (!Client.hasPermission(perm)) Client.sendPacket(new PermissionPacket(args[2].replace("me", ""), (Client.permissions | perm.getRawValue())));
											}else System.err.println("Error: Invalid group/permission!");
											break;
										case "del": case "d":
											if (group != Ranks.INVALID){
												if (Client.hasPermissibleRank(group)) 
													Client.sendPacket(new PermissionPacket(args[2].replace("me", ""), (Client.permissions & (~group.getDefaultPermissions()))));
											}else if (perm != Permissions.INVALID){
												if (Client.hasPermission(perm)) Client.sendPacket(new PermissionPacket(args[2].replace("me", ""), (Client.permissions & (~perm.getRawValue()))));
											}else System.err.println("Error: Invalid group/permission!");
											break;
										default:
											System.err.println("Error: Invalid action!");
											break;
									}
									break;
							}
							break;
						default:
							System.err.println("Error: Invalid target!");
							break;
					}
				}
				break;
		}
	}

	@Override
	Description getDescription(){
		return new Description(this, "Manages the permissions of users and groups.", 
				new Argument("target", "The target of the command.", Required.ALWAYS, 
						new Option("group", "Target a group."), 
						new Option("user", "Target a user."), 
						new Option("perms", "List of all permissions.")), 
				new Argument("user", "The user to affect.", Required.SOMETIMES), 
				new Argument("action", "The action to take place.", Required.SOMETIMES, 
						new Option("add", "Add a permission to a user or group."), 
						new Option("del", "Remove a permission from a user or group."), 
						new Option("list", "List the users info or permissions in a group.")), 
				new Argument("value", "The group or permisison value to be changed.", Required.SOMETIMES));
	}

	@Override
	public List<String> onTabComplete(String[] args){
//		System.out.println("args[0]: '" + args[0] + "' | args.length: " + args.length);
		Permissions perm = Permissions.byName(args[0].toUpperCase());
		
		if (perm == Permissions.DEMOTE || perm == Permissions.PROMOTE){
			if (perm == Permissions.PROMOTE && !Client.hasPermission(Permissions.PROMOTE)) return null;
			
			if (args.length == 2){
				List<String> users = new ArrayList<String>(Arrays.asList("me"));
				
				/* TODO suggest users */
				return users;
			}
		}else{
			switch(args.length){
				case 2:
					return new ArrayList<String>(Arrays.asList("group", "user", "perms"));
				case 3:
					if (args[1].equalsIgnoreCase("group")) return Ranks.getNames();
					else if (args[1].equalsIgnoreCase("user")){
						List<String> users = new ArrayList<String>(Arrays.asList("me"));
						
						return users;
						/* TODO suggest users */
					}
					break;
				case 4:
					if (args[1].equalsIgnoreCase("user")){
						if (!Client.hasPermission(Permissions.PROMOTE)) return new ArrayList<String>(Arrays.asList("del", "list"));
						else return new ArrayList<String>(Arrays.asList("add", "del", "list"));
					}
				case 5:
					if (args[1].equalsIgnoreCase("user")){
						List<String> options = new ArrayList<String>();
						
						options.addAll(Ranks.getNames());
						options.addAll(Permissions.getNames());
						return options;
					}
					break;
				default:
					break;
			}
		}
		return null;
	}
	
	private Ranks getCurrentRank(){
		Ranks lastRank = Ranks.INVALID;
		for (Ranks rank : Ranks.values()){
			if (rank != Ranks.INVALID && Client.hasPermissibleRank(rank)) lastRank = rank;
		}
		return lastRank;
	}
}
