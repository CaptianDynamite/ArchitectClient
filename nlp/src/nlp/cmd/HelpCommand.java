package nlp.cmd;

import java.util.Collections;
import java.util.List;

import nlp.cmd.perm.Permissions;
import nlp.net.Client;
import nlp.panel.comp.MessageBuilder;

/**
 * A <code>Command</code> that displays helpful information about other commands.
 */
public class HelpCommand extends Command{
	
	/**
	 * Constructs a new <code>HelpCommand</code>.
	 * 
	 * @param cmd The name of the <code>Command</code>
	 * @param maxArgs The maximum amount of arguments the <code>Command</code> can have
	 * @param allRequired Whether or not all of the arguments for a <code>Command</code> are required
	 * @param perm The required permission if any, to run the <code>Command</code>
	 * @param aliases Any aliases the <code>Command</code> possesses
	 * 
	 * @see nlp.cmd.Command
	 * @see nlp.cmd.Commands
	 */
	HelpCommand(String cmd, int maxArgs, boolean allRequired, Permissions perm, String... aliases){
		super(cmd, maxArgs, allRequired, perm, aliases);
	}

	@Override
	void execute(String[] args) throws Exception{
		if (args == null) System.out.println(onTabComplete(null).toString().substring(1).replace("]", ""));
		else if (!args[0].isEmpty()){
			Commands cmd = Commands.byAlias(args[0]);
			
			if (cmd == null || !Client.hasPermission(cmd.getPermsRawValue())){
				System.err.println("Error: Invalid command!");
				execute(null);
			}else{
				switch(cmd){
					case HELP:
						System.out.println("Quick! What's the number for 911?!");
						break;
					case PERMISSION:
						Permissions perm = Permissions.byName(args[0].toUpperCase());
						
						if (perm == Permissions.DEMOTE || perm == Permissions.PROMOTE){
							if (perm == Permissions.PROMOTE && !Client.hasPermission(Permissions.PROMOTE)){
								System.err.println("Error: Invalid command!");
								execute(null);
								break;
							}
							
//							System.out.println(MessageBuilder.createNewBuilder().add("Usage: " + args[0].toLowerCase() + " <user>\n     user - The user to " + args[0].toLowerCase() + ".").build());
							System.out.println("Usage: " + args[0].toLowerCase() + " <user>\n     user - The user to " + args[0].toLowerCase() + ".");
							break;
						}
					default:
						if (!Commands.hiddenCmds.contains(cmd.getCommand()) && cmd.get().getDescription() != null) System.out.println(cmd.get().getDescription());
						break;
				}
			}
		}
	}
	
	@Override
	Description getDescription(){
		return null;
	}
	
	@Override
	public List<String> onTabComplete(String[] args){
		List<String> cmds = Commands.getCommands();
		
		cmds.removeAll(Commands.hiddenCmds);
		if (Client.hasPermission(Commands.PERMISSION.getPermsRawValue())){
			cmds.remove(Commands.PERMISSION.get().getCommand());
			cmds.addAll(Commands.PERMISSION.getAliases());
			cmds.remove(Commands.PERMISSION.getAliases().get(1));
			if (!Client.hasPermission(Permissions.PROMOTE)) cmds.remove(Permissions.PROMOTE.getName());
		}
		Collections.sort(cmds);
		return cmds;
	}
}
