package nlp.cmd;

import java.util.List;

import nlp.cmd.perm.Permissions;
import nlp.desktop.apps.Applications;
import nlp.desktop.apps.Console;
import nlp.panel.ConsolePanel;
import nlp.panel.Panels;

/**
 * A <code>Command</code> that clears the console of all text.
 */
public class ClearCommand extends Command{

	/**
	 * Constructs a new <code>ClearCommand</code>.
	 * 
	 * @param cmd The name of the <code>Command</code>
	 * @param maxArgs The maximum amount of arguments the <code>Command</code> can have
	 * @param allRequired Whether or not all of the arguments for a <code>Command</code> are required
	 * @param perm The required permission if any, to run the <code>Command</code>
	 * @param aliases Any aliases the <code>Command</code> possesses
	 * 
	 * @see nlp.cmd.Command
	 * @see nlp.cmd.Commands
	 */
	ClearCommand(String cmd, int maxArgs, boolean allRequired, Permissions perm, String... aliases){
		super(cmd, maxArgs, allRequired, perm, aliases);
	}

	@Override
	void execute(String[] args) throws Exception{
		((Console) Applications.CONSOLE.getApp()).clearConsole();
	}

	@Override
	Description getDescription(){
		return new Description(this, "Clears the console of all text.");
	}

	@Override
	public List<String> onTabComplete(String[] args){
		return null;
	}
}
