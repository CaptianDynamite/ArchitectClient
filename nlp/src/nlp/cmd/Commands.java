package nlp.cmd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nlp.cmd.perm.Permissions;
import nlp.net.Client;

/**
 * A static list of <code>Command</code> objects which handles various aspects of the <code>Command</code>.
 * This includes execution of the <code>Command</code>, and the <code>onTabComplete(String[])</code> method.
 */
public enum Commands{

	/** A <code>Command</code> that clears the console of all text. */
	CLEAR(new ClearCommand("clear", 0, true, Permissions.VIEW_CONSOLE)), 
	/** A <code>Command</code> that displays helpful information about other commands. */
	HELP(new HelpCommand("help", 1, false, Permissions.BASIC_IO, "?")), 
	/** A <code>Command</code> that logs in a specified user. */
	LOGIN(new LoginCommand("login", 3, 2, true, Permissions.INVALID)), 
	/** A <code>Command</code> that pauses a specified application. */
	PAUSE(new PauseCommand("pause", 1, true, Permissions.BASIC_IO)),
	/** A <code>Command</code> that switches to a specified panel. */
	/*PANEL(new PanelCommand("panel", 1, true, Permissions.VIEW_CONSOLE)),*/
	/** A <code>Command</code> that manages the permissions of a specified user. */
	PERMISSION(new PermissionCommand("manage", 5, 2, false, (Permissions.DEMOTE.getRawValue() | Permissions.PROMOTE.getRawValue()), "man", Permissions.DEMOTE.getName(), Permissions.PROMOTE.getName())),
	/** A <code>Command</code> that displays the ping of the <code>Client</code> to the server. */
	PING(new PingCommand("ping", 0, false, Permissions.INVALID)), 
//	/** A <code>Command</code> that promotes a specified user. */
//	PROMOTE(new PermissionCommand("promote", 5, 2, false, Permissions.PROMOTE, PERMISSION.getAliases().toArray(new String[PERMISSION.getAliases().size()]))),
	/** A <code>Command</code> that is used for testing code segments. */
	TEST(new TestCommand("test", -1, false, Permissions.VIEW_CONSOLE)), 
	/** A <code>Command</code> that runs the previously ran command with a yes or no argument. */
	YES_NO(new YesNoCommand(YesNoCommand.options.keySet().toArray(new String[YesNoCommand.options.size()])[0], 1, true, Permissions.INVALID, YesNoCommand.options.keySet().toArray(new String[YesNoCommand.options.size()])));
	
	private Command cmd;
	
	public static List<String> hiddenCmds = new ArrayList<String>(Arrays.asList(YES_NO.getCommand()));
	
	/**
	 * Registers new commands.
	 * 
	 * @param cmd The <code>Command</code> to register
	 * 
	 * @see nlp.cmd.Command
	 */
	Commands(final Command cmd){
		this.cmd = cmd;
	}
	
	/**
	 * Executes a <code>Command</code>.<p>
	 * 
	 * Handles argument validity for the <code>Command</code>.
	 * 
	 * @param args The given arguments for the <code>Command</code>
	 * @throws Exception Any exception that occurs while executing the <code>Command</code>
	 * 
	 * @see nlp.cmd.Command#execute(String[])
	 */
	public void execute(String... args) throws Exception{
		if ((args == null && (cmd.getMaxArgs() == 0 || !cmd.allArgsRequired())) || 
				(args != null && ((cmd.getMaxArgs() == -1) || (args.length >= cmd.getMaxReqArgs() && args.length <= cmd.getMaxArgs())))){
			cmd.execute(args);
		}else{
			if ((args == null && cmd.getMaxArgs() > 0) || (cmd.allArgsRequired() && args.length < cmd.getMaxArgs()) || (!cmd.allArgsRequired() && args.length < cmd.getMaxReqArgs())){
				if (cmd instanceof PermissionCommand && args[0].equalsIgnoreCase(Permissions.PROMOTE.getName()) && !Client.hasPermission(Permissions.PROMOTE)) System.err.println("Error: Invalid command!");
				else System.err.println("Error: Not enough arguments!");
			}else{
				System.err.println("Error: Too many arguments!");
			}
		}
	}
	
	/**
	 * Returns the actual <code>Command</code> object.
	 * 
	 * @return the <code>Command</code> object
	 * 
	 * @see nlp.cmd.Command
	 */
	public Command get(){
		return cmd;
	}
	
	/**
	 * Returns the name of the command.
	 * 
	 * @return the name for the <code>Command</code> object
	 * 
	 * @see nlp.cmd.Command#getCommand()
	 */
	public String getCommand(){
		return cmd.getCommand();
	}
	
	/**
	 * Returns the permission required to run the command.
	 * 
	 * @return the required permission for the <code>Command</code>
	 * 
	 * @see nlp.cmd.Command#getPerm()
	 */
	public Permissions getPerm(){
		return cmd.getPerm();
	}
	
	public long getPermsRawValue(){
		return cmd.getPermsRawValue();
	}
	
	/**
	 * Returns the command's aliases.
	 * 
	 * @return the aliases for the <code>Command</code>
	 * 
	 * @see nlp.cmd.Command#getAliases()
	 */
	public List<String> getAliases(){
		return cmd.getAliases();
	}
	
	/**
	 * Returns a list of argument options based on the <code>Command</code> and given arguments.<p>
	 * 
	 * Handles the amount of arguments being too few compared to the maximum required arguments.
	 * 
	 * @param args The current command arguments in the console input
	 * @return a list of argument options for the <code>Command</code> based on the arguments provided
	 * 
	 * @see nlp.cmd.Command#onTabComplete(String[])
	 */
	public List<String> onTabComplete(String[] args){
		if (args == null || args.length-1 <= cmd.getMaxArgs()) return cmd.onTabComplete(args);
		return null;
	}
	
	/**
	 * Returns a list of all the registered command names.<p>
	 * 
	 * Handles the permissions of the <code>Client</code> when compiling the list of registered command names.
	 * 
	 * @return a list of all registered command names
	 */
	public static List<String> getCommands(){
		List<String> commands = new ArrayList<String>();
		
		for (Commands cmd : values()){
			if (Client.hasPermission(cmd.getPermsRawValue())) commands.add(cmd.getCommand());
		}
		return commands;
	}
	
	/**
	 * Returns a <code>Commands</code> version of the <code>Command</code>.
	 * 
	 * @param alias The name of the <code>Commands</code> object
	 * @return a <code>Commands</code> object based on the given alias
	 */
	public static Commands byAlias(String alias){
		if (alias != null && !alias.isEmpty()){
			for (Commands c : values()){
				if (c.getAliases().contains(alias.toLowerCase())) return c;
			}
		}
		return null;
	}
}
