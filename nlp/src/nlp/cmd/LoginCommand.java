package nlp.cmd;

import java.util.List;

import nlp.cmd.Argument.Required;
import nlp.cmd.perm.Permissions;
import nlp.net.Client;
import nlp.net.LoginPacket;
import nlp.net.LogoutPacket;

/**
 * A <code>Command</code> that logs in a specified user.
 */
public class LoginCommand extends Command{
	
	/**
	 * Constructs a new <code>LoginCommand</code>.
	 * 
	 * @param cmd The name of the <code>Command</code>
	 * @param maxArgs The maximum amount of arguments the <code>Command</code> can have
	 * @param allRequired Whether or not all of the arguments for a <code>Command</code> are required
	 * @param perm The required permission if any, to run the <code>Command</code>
	 * @param aliases Any aliases the <code>Command</code> possesses
	 * 
	 * @see nlp.cmd.Command
	 * @see nlp.cmd.Commands
	 */
	LoginCommand(String cmd, int maxArgs, int maxReq, boolean allRequired, Permissions perm, String... aliases){
		super(cmd, maxArgs, maxReq, allRequired, perm, aliases);
	}

	@Override
	void execute(String[] args) throws Exception{
		if (Client.username != null){
			if (!Client.username.equals(args[0])){
				if (Commands.byAlias(args[0]) != Commands.YES_NO){
					System.out.println("You're already logged in as " + Client.username + "!");	
					System.out.print("Would you like to switch to " + (Client.userToLogin = args[0]) + "? (Y/N): ");
					Client.userToLoginCode = args[1];
				}else if (YesNoCommand.options.get(args[0]).booleanValue()) Client.sendPacket(new LogoutPacket());
			}else System.err.println("Error: You're already logged in as " + Client.username + "!");
		}else Client.sendPacket(new LoginPacket(args[0], args[1]));
	}

	@Override
	Description getDescription(){
		return new Description(this, "Login as a given user with a given code.", 
				new Argument("user", "The user you wish to login as.", Required.ALWAYS),
				new Argument("code", "The code for the given user.", Required.ALWAYS));
	}

	@Override
	public List<String> onTabComplete(String[] args){
		return null;
	}
}
