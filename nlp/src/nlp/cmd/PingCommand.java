package nlp.cmd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nlp.cmd.perm.Permissions;
import nlp.net.Client;
import nlp.net.PingPacket;

/**
 * A <code>Command</code> that displays the ping of the <code>Client</code> to the server.
 */
public class PingCommand extends Command{

	/**
	 * Constructs a new <code>PingCommand</code>.
	 * 
	 * @param cmd The name of the <code>Command</code>
	 * @param maxArgs The maximum amount of arguments the <code>Command</code> can have
	 * @param allRequired Whether or not all of the arguments for a <code>Command</code> are required
	 * @param perm The required permission if any, to run the <code>Command</code>
	 * @param aliases Any aliases the <code>Command</code> possesses
	 * 
	 * @see nlp.cmd.Command
	 * @see nlp.cmd.Commands
	 */
	PingCommand(String cmd, int maxArgs, boolean allRequired, Permissions perm, String... aliases) {
		super(cmd, maxArgs, allRequired, perm, aliases);
	}

	@Override
	void execute(String[] args) throws Exception{
		Client.sendPacket(new PingPacket());
	}

	@Override
	Description getDescription(){
		return new Description(this, "Displays your connection speed to the server.");
	}

	@Override
	public List<String> onTabComplete(String[] args){
		return new ArrayList<String>(Arrays.asList("server"));
	}
}
