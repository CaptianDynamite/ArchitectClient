package nlp.cmd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nlp.cmd.perm.Permissions;

/**
 * Represents a command, which executes various tasks upon user input.
 */
public abstract class Command{
	
	private String cmd;
	private String[] aliases;
	private Permissions perm = null;
	private long perms;
	private int maxArgs;
	private int maxReq;
	private boolean allRequired;
	
	/**
	 * Constructs a new <code>Command</code>.
	 * 
	 * @param cmd The name of the command
	 * @param maxArgs The maximum amount of arguments the command can have
	 * @param allRequired Whether or not all of the arguments for a command are required
	 * @param perm The required permission if any, to run the command
	 * @param aliases Any aliases the command possesses
	 */
	Command(String cmd, int maxArgs, boolean allRequired, Permissions perm, String... aliases){
		this(cmd, maxArgs, maxArgs, allRequired, perm, aliases);
	}
	
	/**
	 * Constructs a new <code>Command</code> with the option of partial argument requirements.
	 * 
	 * @param cmd The name of the command
	 * @param maxArgs The maximum amount of arguments the command can have
	 * @param maxReq The maximum amount of required arguments for the command
	 * @param allRequired Whether or not all of the arguments for a command are required
	 * @param perm The required permission if any, to run the command
	 * @param aliases Any aliases the command possesses
	 */
	Command(String cmd, int maxArgs, int maxReq, boolean allRequired, Permissions perm, String... aliases){
		this(cmd, maxArgs, maxReq, allRequired, perm.getRawValue(), aliases);
		this.perm = perm;
	}
	
	/**
	 * Constructs a new <code>Command</code>.
	 * 
	 * @param cmd The name of the command
	 * @param maxArgs The maximum amount of arguments the command can have
	 * @param allRequired Whether or not all of the arguments for a command are required
	 * @param perms The required permissions if any, to run the command
	 * @param aliases Any aliases the command possesses
	 */
	Command(String cmd, int maxArgs, boolean allRequired, long perms, String... aliases){
		this(cmd, maxArgs, maxArgs, allRequired, perms, aliases);
	}
	
	/**
	 * Constructs a new <code>Command</code> with the option of partial argument requirements.
	 * 
	 * @param cmd The name of the command
	 * @param maxArgs The maximum amount of arguments the command can have
	 * @param maxReq The maximum amount of required arguments for the command
	 * @param allRequired Whether or not all of the arguments for a command are required
	 * @param perms The required permissions if any, to run the command
	 * @param aliases Any aliases the command possesses
	 */
	Command(String cmd, int maxArgs, int maxReq, boolean allRequired, long perms, String... aliases){
		this.cmd = cmd;
		this.maxArgs = maxArgs;
		this.maxReq = maxReq;
		this.allRequired = allRequired;
		this.perms = perms;
		this.aliases = aliases;
	}
	
	/**
	 * Returns the command name.
	 * 
	 * @return the name of the command
	 */
	String getCommand(){
		return cmd;
	}
	
	/**
	 * Returns all the aliases.
	 * 
	 * @return a list of aliases for the command
	 */
	List<String> getAliases(){
		List<String> list = new ArrayList<String>(Arrays.asList(cmd));
		if (aliases != null) list.addAll(new ArrayList<String>(Arrays.asList(aliases)));
		return list;
	}
	
	/**
	 * Returns the command permission.
	 * 
	 * @return the permission required to run the command
	 */
	Permissions getPerm(){
		return perm;
	}
	
	/**
	 * Returns the command permissions.
	 * 
	 * @return the permissions required to run the command
	 */
	long getPermsRawValue(){
		return perms;
	}
	
	/**
	 * Returns the maximum amount of arguments.
	 * 
	 * @return the maximum amount of arguments for the command
	 */
	int getMaxArgs(){
		return maxArgs;
	}
	
	/**
	 * Returns the maximum amount of required arguments.
	 * 
	 * @return the maximum amount of required arguments for the command
	 */
	int getMaxReqArgs(){
		return maxReq;
	}
	
	/**
	 * Returns whether or not all arguments are required.
	 * 
	 * @return whether or not all the arguments for the command are required
	 */
	boolean allArgsRequired(){
		return allRequired;
	}
    
	/**
	 * Executes the command.
	 * 
	 * @param args The given arguments
	 * @throws Exception Any exception that occurs while executing the command
	 */
    abstract void execute(final String[] args) throws Exception;
    
    /**
     * Returns a description of the command.
     * 
     * @return the <code>Description</code> of the <code>Command</code>
     * 
     * @see nlp.cmd.Description
     */
    abstract Description getDescription();
    
    /**
     * Returns a list of argument options based on the given arguments.
     * 
     * @param args The given arguments
     * @return a list of argument options based on the arguments provided
     */
    public abstract List<String> onTabComplete(final String[] args);
}
