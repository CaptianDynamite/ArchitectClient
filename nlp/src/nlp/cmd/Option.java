package nlp.cmd;

public class Option{
	
	private String opt;
	private String desc;
	
	/**
	 * Constructs a new <code>Option</code> for an <code>Argument</code>.
	 * 
	 * @param opt The name of the option
	 * @param desc The description of the option
	 */
	Option(String opt, String desc){
		this.opt = opt;
		this.desc = desc;
	}
	
	/**
	 * Returns the option name.
	 * 
	 * @return the name of the option
	 */
	public String get(){
		return opt;
	}
	
	/**
	 * Returns the option description.
	 * 
	 * @return the description of the option
	 */
	public String getDescription(){
		return desc;
	}
}
