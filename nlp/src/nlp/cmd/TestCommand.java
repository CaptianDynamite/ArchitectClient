package nlp.cmd;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import nlp.NLP;
import nlp.cmd.perm.Permissions;
//import nlp.cmd.perm.Ranks;
import nlp.net.Client;
import nlp.panel.comp.MessageBuilder;
import nlp.util.Images;

/**
 * A <code>Command</code> that is used for testing code segments.
 */
public class TestCommand extends Command{

//	private final String tab = "     ";
	
	/**
	 * Constructs a new <code>TestCommand</code>.
	 * 
	 * @param cmd The name of the <code>Command</code>
	 * @param maxArgs The maximum amount of arguments the <code>Command</code> can have
	 * @param allRequired Whether or not all of the arguments for a <code>Command</code> are required
	 * @param perm The required permission if any, to run the <code>Command</code>
	 * @param aliases Any aliases the <code>Command</code> possesses
	 * 
	 * @see nlp.cmd.Command
	 * @see nlp.cmd.Commands
	 */
	TestCommand(String cmd, int maxArgs, boolean allRequired, Permissions perm, String... aliases) {
		super(cmd, maxArgs, allRequired, perm, aliases);
	}

	@Override
	void execute(String[] args) throws Exception{
//		File perms = new File(FileSystemView.getFileSystemView().getHomeDirectory(), "nlp_group_perms.yml");
//		PrintWriter writer = new PrintWriter(perms);
//		
//		for (Ranks rank : Ranks.values()){
//			if (rank != Ranks.INVALID){
//				writer.println(rank.getName() + ": ");
//				for (Permissions perm : Permissions.getPermissions(rank.getDefaultPermissions())){
//					writer.println(tab + perm.getName());
//				}
//			}
//		}
//		writer.close();
		System.out.println(Client.permissions + " | hasPermission(" + Permissions.DEMOTE.name() + "): " + Client.hasPermission(Permissions.DEMOTE));
//		System.out.println(MessageBuilder.createNewBuilder().add("This is a ").add("test", new JLabel(new ImageIcon(Images.MAIN_ICON.getBool(false)))).add(" message!").build());
//		System.out.println(MessageBuilder.createNewBuilder().add("Testing ").add("123!").build());
//		System.out.println(MessageBuilder.createNewBuilder().add("image!", new JLabel(new ImageIcon(Images.MAIN_ICON.getBool(false)))).build());
//		throw new Exception("test");
		System.out.println("Done testing!");
	}

	@Override
	Description getDescription(){
		return new Description(this, "Testing, testing, 123...");
	}

	@Override
	public List<String> onTabComplete(String[] args){
		return null;
	}
}
