package nlp.cmd.perm;

import java.util.ArrayList;
//import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * A static list of user permissions.<p>
 * 
 * Handles what users can and can't do.
 * 
 * @see nlp.cmd.perm.Ranks
 */
public enum Permissions{
	/* Member permissions */
	/** Allows a user to use text input/output. */
	BASIC_IO(0, Ranks.MEMBER),
	/** Allows a user to use 10,000 nodes max. */
	MAX_10K_NODES(1, Ranks.MEMBER),
	/** Allows a user to promote other users. */
	REPORT(2, Ranks.MEMBER),
	/** Allows a user to ignore other non-staff users. */
	IGNORE(3, Ranks.MEMBER),
	
	/* Trusted permissions */
	/** Allows a user to use 10,000,000 nodes max. */
	MAX_10M_NODES(4, Ranks.TRUSTED),
	/** Allows a user to use audio input/output. */
	AUDIO_IO(5, Ranks.TRUSTED),
	/** Allows a user to use 1,000 test nodes max. */
	TEST_1K_NODES(6, Ranks.TRUSTED),
	
	/* Respected permissions */
	/** Allows a user to use 100,000,000 nodes max. */
	MAX_100M_NODES(7, Ranks.RESPECTED),
	/** Allows a user to mute other users of a lower rank. */
	MUTE(8, Ranks.RESPECTED),
	/** Allows a user to use 100,000 test nodes max. */
	TEST_100K_NODES(9, Ranks.RESPECTED),
	
	/* Staff permissions */
	/** Allows a user to kick and/or ban other users of a lower rank. */
	KICK_BAN(10, Ranks.STAFF),
	/** Allows a user to restart, start, and pause the server. */
	RESTART_START_PAUSE(11, Ranks.STAFF),
	/** Allows a user to shutdown the tests of other users of a lower rank. */
	TEST_SHUTDOWN(12, Ranks.STAFF),
	/** Allows a user to ignore session timeouts. */
	IGNORE_TIMEOUT(13, Ranks.STAFF),
	/** Allows a user to receive error notifications. */
	ERROR_NOTIFICATIONS(14, Ranks.STAFF),
	
	/* Moderator permissions */
	/** Allows a user to use 1,000,000 test nodes max. */
	TEST_1M_NODES(15, Ranks.MODERATOR),
	/** Allows a user to demote other users of an already lower rank. */
	DEMOTE(16, Ranks.MODERATOR),
	/** Allows a user to rollback 10,000 nodes max. */
	ROLLBACK_10K_NODES(17, Ranks.MODERATOR),
	/** Allows a user to delete the messages of other users of a lower rank. */
	DELETE_MESSAGE(18, Ranks.MODERATOR),
	
	/* Developer permissions */
	/** Allows a user to view the server console. */
	VIEW_CONSOLE(19, Ranks.DEVELOPER),
	/** Allows a user to use 500,000 test nodes max. */
	TEST_500K_NODES(20, Ranks.DEVELOPER),
	/** Allows a user to generate main testing threads. */
	GENERATE(21, Ranks.DEVELOPER),
	/** Allows a user to shutdown and resume the activity the server. */
	SHUTDOWN_RESUME(22, Ranks.DEVELOPER),
	/** Allows a user to rollback 10,000,000 nodes max. */
	ROLLBACK_10M_NODES(23, Ranks.DEVELOPER),
	
	/* Administrator permissions */
	/** Allows a user to promote other users. */
	PROMOTE(24, Ranks.ADMINISTRATOR),
	/** Allows a user to create a remote server thread. */
	REMOTE_THREAD_CREATE(25, Ranks.ADMINISTRATOR),
	
	/** Any invalid permission. */
    INVALID(-1, Ranks.INVALID);

	/** Minimum Member raw permissions set */
	public static final long MEMBER_PERMISSIONS = Permissions.getRaw(Ranks.MEMBER);
	
	/** Minimum Trusted raw permissions set */
	public static final long TRUSTED_PERMISSIONS = Permissions.getRaw(Ranks.TRUSTED);
	
	/** Minimum Respected raw permissions set */
	public static final long RESPECTED_PERMISSIONS = Permissions.getRaw(Ranks.RESPECTED);
	
	/** Minimum Moderator raw permissions set */
	public static final long MODERATOR_PERMISSIONS = Permissions.getRaw(Ranks.MODERATOR);
	
	/** Minimum Developer raw permissions set */
	public static final long DEVELOPER_PERMISSIONS = Permissions.getRaw(Ranks.DEVELOPER);
	
    /** Represents a raw set of all permissions */
    public static final long ALL_PERMISSIONS = Permissions.getRaw(values());
    
    private final long raw;
    private final int offset;
    private final Ranks rank;

    /**
     * Registers new permissions.
     * 
     * @param offset The bit offset of the permission
     * @param name The name of the permission
     * @param rank The lowest rank required for the permission
     * 
     * @see nlp.cmd.perm.Ranks
     */
    Permissions(int offset, Ranks rank){
    	this.offset = offset;
    	this.raw = 1 << offset;
        this.rank = rank;
    }

    /**
     * Returns the permission name.
     * 
     * @return the name of the permission
     */
    public String getName(){
        return name().toLowerCase();
    }
    
    /**
     * Returns the lowest rank required for the permisison.
     * 
     * @return the lowest <code>Ranks</code> required for the permission
     * 
     * @see nlp.cmd.perm.Ranks
     */
    public Ranks getRank(){
    	return rank;
    }

    /**
     * Returns the permission bit offset.
     * 
     * @return the bit offset of the permission
     */
    public int getOffset(){
    	return offset;
    }

    /**
     * Returns the permission raw value.
     * 
     * @return the raw value of the permission
     */
    public long getRawValue(){
        return raw;
    }

    /**
     * Returns a <code>Permissions</code> object based on the name.
     * 
     * @param name The permission name
     * @return a <code>Permissions</code> object based on the given name
     */
    public static Permissions byName(String name){
    	if (name != null && !name.isEmpty()){
	    	for (Permissions perm : values()){
	    		if (perm != INVALID && perm.getName().equals(name.toLowerCase())) return perm;
	    	}
    	}
    	return INVALID;
    }
    
    /**
     * Returns a <code>Permissions</code> object based on the bit offset.
     * 
     * @param offset The permission offset
     * @return a <code>Permissions</code> object based on the given bit offset
     */
    public static Permissions byOffset(int offset){
        return (offset >= 0 && offset < values().length) ? values()[offset] : INVALID;
    }
    
    /**
     * Returns a list of permissions based on the raw permission value.
     * 
     * @param permissions The raw permission value
     * @return a list of permissions based on the raw permission value
     */
    public static List<Permissions> getPermissions(long permissions){
        List<Permissions> perms = new ArrayList<Permissions>();
        
        for (Permissions perm : values()){
            if (((permissions >> perm.getOffset()) & 1) == 1) perms.add(perm);
        }
        return perms;
    }
    
    /**
     * TODO nlp.cmd.perm.Permissions#getNames
     * @return
     */
    public static List<String> getNames(){
    	List<String> perms = new ArrayList<String>();
    	
    	for (Permissions perm : values()){
    		if (perm != INVALID) perms.add(perm.getName());
    	}
    	return perms;
    }
    
    /**
     * Returns the raw permissions value of the given permissions.
     * 
     * @param permissions The permissions to iterate through
     * @return the raw value of the given <code>Permissions</code> objects
     */
    public static long getRaw(Permissions... permissions){
        long raw = 0;
        
        for (Permissions perm : permissions){
            if (perm != INVALID) raw |= perm.getRawValue();
        }
        return raw;
    }

    /**
     * Returns the raw permissions value of the given rank.
     * 
     * @param rank The rank from which to getBool the permissions
     * @return the raw value of the permissions based on the given <code>Ranks</code> object
     */
    private static long getRaw(Ranks rank){
    	List<Permissions> perms = new ArrayList<Permissions>();
    	
    	for (Permissions perm : values()){
    		if (perm != INVALID && perm.getRank().getIndex() <= rank.getIndex()) perms.add(perm);
    	}
    	return getRaw(perms);
    }
    
    /**
     * Returns the raw permissions value of the given permissions.
     * 
     * @param permissions The permissions to iterate through
     * @return the raw value of the given permissions
     */
    public static long getRaw(Collection<Permissions> permissions){
        return getRaw(permissions.toArray(new Permissions[permissions.size()]));
    }
    
//    public static long getRaw(Collection<Permissions> permissions, Permissions... additional){
//    	Permissions[] perms = permissions.toArray(new Permissions[permissions.size()+additional.length]);
//    	
//    	for (int i = 0; i < additional.length; i++){
//    		perms[i+permissions.size()] = additional[i];
//    	}
//    	return getRaw(perms);
//    }
}
