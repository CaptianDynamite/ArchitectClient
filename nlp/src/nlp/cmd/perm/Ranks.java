package nlp.cmd.perm;

import java.util.ArrayList;
import java.util.List;

/**
 * A static list of ranks users can have.
 */
public enum Ranks{

	/** A new and slightly trusted user. */
	MEMBER, 
	/** A typically experienced user, who is trustworthy. */
	TRUSTED, 
	/** A seasoned user who is respected among staff and the community. */
	RESPECTED, 
	/** A user who is a part of the Architect staff team. */
	STAFF, 
	/** A user who moderates and oversees other users actions. */
	MODERATOR, 
	/** A developer for the Architect project. */
	DEVELOPER, 
	/** A user who oversees everything about the Architect project. */
	ADMINISTRATOR,
	
	/** Any invalid rank. */
	INVALID;
	
	/**
	 * Returns the rank name.
	 * 
	 * @return a formatted version of the rank name
	 */
	public String getName(){
		return (this != INVALID) ? name().substring(0, 1) + name().substring(1).toLowerCase() : null;
	}
	
	public static List<String> getNames(){
		List<String> names = new ArrayList<String>();
		
		for (Ranks rank : values()){
			if (rank != INVALID) names.add(rank.name().toLowerCase());
		}
		return names;
	}
	
	public static Ranks byName(String name){
		if (name != null && !name.isEmpty()){
			for (Ranks rank : values()){
				if (rank != INVALID && rank.name().toLowerCase().equals(name.toLowerCase())) return rank;
			}
		}
		return INVALID;
	}
	
	/**
	 * Returns the rank index.
	 * 
	 * @return the index of the <code>Ranks</code> object based on its index in the enum
	 */
	public int getIndex(){
		for (int i = 0; i < values().length; i++){
			if (this != INVALID && name().equals(values()[i].name())) return i;
		}
		return -1;
	}
	
	/**
	 * Returns the rank's default permissions.
	 * 
	 * @return the default permissions raw value of the <code>Ranks</code> object
	 * 
	 * @see nlp.cmd.perm.Permissions#getRawValue()
	 */
	public long getDefaultPermissions(){
		if (this == Ranks.ADMINISTRATOR) return Permissions.ALL_PERMISSIONS;
		try{
			return Permissions.class.getDeclaredField(name() + "_PERMISSIONS").getLong(null);
		}catch (Exception e){}
		return -1;
	}
	
	public List<String> getDefaultPermissionsList(){
		List<String> perms = new ArrayList<String>();
		
		for (Permissions perm : Permissions.getPermissions(getDefaultPermissions())){
			perms.add(perm.getName());
		}
		return perms;
	}
}
