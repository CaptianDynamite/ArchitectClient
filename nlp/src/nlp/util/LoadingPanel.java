package nlp.util;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.util.*;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import nlp.NLP;
import nlp.desktop.apps.Applications;
import nlp.desktop.apps.comp.CircularProgressBar;
import nlp.panel.comp.TextComponents;
import nlp.util.Images.IconSizes;

/**
 * A panel that displays information about the program while it is loading.
 */
public class LoadingPanel extends JFrame{

	private static final long serialVersionUID = 1L;
	
	public static final Dimension DEFAULT_SIZE = new Dimension(320, 360);

	private static CircularProgressBar loaded;
//	private static LogoProgressBar loaded;
	private static JLabel loading;
	
	private static int classesLoaded;
//	private static int classesToLoad = (Panels.values().length-1)+3; /* LoadingPanel, Images, Panels, TabComponent */
	private static int classesToLoad = 2+getAppCount(); /* LoadingPanel, Images, Applications */
	private static Map<Class<? extends Loadable>, Integer> classes = new HashMap<Class<? extends Loadable>, Integer>();
	
	/**
	 * Constructs a new <code>LoadingPanel</code>.
	 */
	public LoadingPanel(){
		setUndecorated(true);
		setPreferredSize(new Dimension((int)(NLP.getScreen().width*(1.0/6)), (int)(NLP.getScreen().height*(1.0/3))));
		setTitle(NLP.TITLE);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent event){
	        	NLP.cleanExit();
	        }
		});
		
		JPanel info = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.fill = GridBagConstraints.BOTH;
		
		loaded = new CircularProgressBar((int)(getPreferredSize().width*0.7));
//		loaded = new LogoProgressBar((int) (getPreferredSize().width*0.7), 1000);
//		loaded.setIndeterminate(true);
		c.insets = new Insets((getPreferredSize().width-loaded.getDiameter())/2, (getPreferredSize().width-loaded.getDiameter())/2, 0, (getPreferredSize().width-loaded.getDiameter())/2);
//		c.insets = new Insets((getPreferredSize().width-loaded.getWidth())/2, (getPreferredSize().width-loaded.getWidth())/2, 0, (getPreferredSize().width-loaded.getWidth())/2);
		info.add(loaded, c);
		
		pack();
		
		c.gridy = 1;
		c.insets = new Insets(0, 20, 0, 20);
		loading = new JLabel();
//		loading.setPreferredSize(new Dimension(getPreferredSize().width-40, (int) getGraphics().getFontMetrics(loading.getFont()).getStringBounds("1", getGraphics()).getHeight()));
		loading.setForeground(Color.WHITE);
		nowLoading(null, "TextComponents/"+TextComponents.LOADING_PANEL_LOADING.name().toLowerCase());
		loading.setFont(TextComponents.LOADING_PANEL_LOADING.getFixedFont(loading));
		info.add(loading, c);
		loaded.setFont(loading.getFont());
		
		info.setBackground(new Color(52, 57, 61));
		add(info);
		
		setVisible(true);
//		System.out.println(getSize());
	}
	
	/**
	 * Loads everything that needs to be loaded before the main program is shown.
	 */
	public void load(){
		/* Load Images */
		Images.loadImages();
		
		List<BufferedImage> icons = new ArrayList<BufferedImage>();
		for (IconSizes size : IconSizes.values()){
			icons.add(Images.iconSets.get(Images.MAIN_ICON).get(size));
		}
		NLP.loading.setIconImages(icons);
		
		/* Initialize the main client 
		 * Loads the panels */
		NLP.initialize();
		
		if (loaded.getValue() == 100 || loaded.isIndeterminate()){
			Timer delay = new Timer(100, new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e){
					NLP.start();
					NLP.loading.dispose();
				}
			});
			delay.setRepeats(false);
			delay.start();
		}
	}
	
	/**
	 * Sets the given component as the current component being loaded.
	 * 
	 * @param loadable The class with loadable objects
	 * @param component The component currently being loaded
	 */
	public static void nowLoading(Loadable loadable, String component){
//		NLP.out.println("Loading: " + component);
		loading.setText("Loading: " + component);
		loading.setToolTipText(loading.getText());
		
		Class<? extends Loadable> cls = (loadable != null) ? loadable.getClass() : null;
		if (cls != null){
			try{
				if (!classes.containsKey(cls)) classes.put(cls, 0);
				classes.put(cls, classes.get(cls)+1);
				float clsPercentLoaded = (loadable.thingsToLoad() == 0) ? 1.0F : ((classes.get(cls)*1.0F)/loadable.thingsToLoad());
				loaded.setValue(((((classesLoaded*1.0F)/classesToLoad))+((1.0F/classesToLoad)*clsPercentLoaded))*100.0F);
//				NLP.out.println(cls.getSimpleName()+": " + ((1.0F/classesToLoad)*((classes.getBool(cls)*1.0F)/loadable.thingsToLoad())));
				
				if (classes.get(cls) >= loadable.thingsToLoad()) classesLoaded++;
//				NLP.out.println(cls.getSimpleName()+ "(" + classes.getBool(cls)+ "/" + loadable.thingsToLoad() + "|" + classesLoaded + "/" + classesToLoad + "): " + loaded.getValueF());
			}catch (Exception e){
				e.printStackTrace();
			}
		}else{
			loaded.setValue(((((++classesLoaded))*1.0F)/classesToLoad)*100.0F);
//			NLP.out.println("null(1|1|" + classesLoaded + "|" + classesToLoad + "): " + loaded.getValueF());
		}
//		NLP.out.println("toLoad: "+ classesToLoad + " | loaded: " + classesLoaded);
	}
	
	private static int getAppCount(){
		int count = 0;
		for (Field f : Applications.class.getDeclaredFields()){
			if (f.isEnumConstant()) count++;
		}
		return count;
	}
}
