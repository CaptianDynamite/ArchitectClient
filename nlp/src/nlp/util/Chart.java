package nlp.util;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.swing.*;

import nlp.NLP;
import nlp.desktop.apps.comp.UpdatingToolTip;
import nlp.util.Chart.Types;

public class Chart extends JPanel{

	private static final long serialVersionUID = 1L;
	
	private static final int STROKE = 1;
	private static final JLabel noData = new JLabel("No data available!");
	
	private int max;
	private long total;
	private int[] data;
	private Types type = Types.DEFAULT;
	private Color a, b;
	private String unit;
	private Chart next, prev;
	private UpdatingToolTip toolTip;
	
	private Map<Area, Integer> values;
	
	public Chart(Types type, Color a, Color b, boolean mutable){
		this(type, a, b, mutable, null);
	}
	
	public Chart(Types type, Color a, Color b, boolean mutable, String unit){
		this(type, a, b, -1, mutable, unit);
	}
	
	public Chart(Types type, Color a, Color b, int max, boolean mutable, String unit){
		this.type = type;
		this.a = a;
		this.b = b;
		this.unit = unit;
		if (max > 0) this.max = max;
		toolTip = new UpdatingToolTip(this);
		
		setOpaque(false);
//		if (mutable){
//			GridBagConstraints c = new GridBagConstraints();
//			setLayout(new GridBagLayout());
//
//			c.gridx = 0;
//			c.gridy = 0;
//			c.anchor = GridBagConstraints.NORTHEAST;
//			JMenu charts = new JMenu("☰");
//
//			for (int i = 0; i < Types.values().length; i++){
//				JMenuItem chart = new JMenuItem(Types.values()[i].name());
//				chart.addActionListener(new ActionListener(){
//					public void actionPerformed(ActionEvent e){
//						Chart.this.type = Types.byId(i);
//					}
//				});
//				charts.add(chart);
//			}
//			add(charts, c);
//		}
		addMouseMotionListener(new MouseMotionAdapter(){
			@Override
			public void mouseMoved(MouseEvent e){
				boolean isInArea = false;
				
				if (values != null && values.size() > 0){
					for (Area area : values.keySet()){
						if (area.contains(e.getPoint())){
							toolTip.setToolTipText(String.valueOf(values.get(area)) + unit);
							isInArea = true;
							break;
						}
					}
				}
				if (!isInArea) toolTip.setToolTipText(null);
			}
		});
		noData.setForeground(Color.WHITE.darker());
		add(noData);
	}
	
	public Types getType(){
		return type;
	}
	
	//Chart(byte[] data){
		//this(Types.byId(data[1]);
		//
		//for (int i = 2; i < data.length; i++){
		//updateData()
	//}

	public void updateData(int[] data, boolean override){
		if (override) this.data = data;
		else{
			if (this.data == null) this.data = new int[0];
			
			int[] newData = new int[this.data.length+data.length];
//			System.out.println("this.data.length: " + this.data.length + " | data.length: " + data.length + " | newData.length: " + newData.length);
			
			for (int i = 0; i < newData.length; i++){
				newData[i] = (i < this.data.length) ? this.data[i] : data[i-this.data.length];
			}
			this.data = newData;
		}
		if (type != Types.LINE) Arrays.sort(data);
		if (data.length == 1 && type == Types.PIE) data = new int[]{data[0], max-data[0]};
		
		total = 0;
		for (int i = 0; i < data.length; i++){
			max = (max > data[i]) ? max : data[i];
			total += data[i];
		}
		if (values != null) values.clear();
		else values = new HashMap<Area, Integer>();
//		System.out.println("total(" + data.length + "): " + total);
//		NLP.out.println(Arrays.toString(data));
		
//		boolean toolTipEnabled = ToolTipManager.sharedInstance().isEnabled();
//		if (toolTipEnabled) ToolTipManager.sharedInstance().setEnabled(false);
		repaint();
//		ToolTipManager.sharedInstance().setEnabled(toolTipEnabled);
	}

	@Override
	public void paint(Graphics g){
//		System.out.println("paint");
		if (noData.getWidth() == 0 || noData.getHeight() == 0) noData.setSize(getSize());
		else noData.setVisible(!(data != null && data.length > 0));
		super.paint(g);
		
		if (!noData.isVisible()){
			BufferedImage chart = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics2D g2d = chart.createGraphics();
			
			int barWidth = (int) (((type == Types.BAR) ? getHeight() : ((type == Types.HISTOGRAM) ? getWidth() : 0))/data.length), prevValue = 0;
			double arcLength = 0, prevArc = 0;
			
			g2d.setStroke(new BasicStroke(STROKE));
			double pieRemaining = 360;
			for (int i = 0; i < data.length; i++){
				Color newColor;
				float interpolation = (i+1)*(1.0F/data.length);
				int newRed = (int) Math.min(Math.abs(((b.getRed()*interpolation)+(a.getRed()*(1-interpolation)))), 255),
						newGreen = (int) Math.min(Math.abs(((b.getGreen()*interpolation)+(a.getGreen()*(1-interpolation)))), 255),
						newBlue = (int) Math.min(Math.abs(((b.getBlue()*interpolation)+(a.getBlue()*(1-interpolation)))), 255);
				
				newColor = new Color(newRed, newGreen, newBlue);
				g2d.setColor(newColor);
				
				Area area = null;
				switch(type){
					case BAR: case HISTOGRAM:
						double barLength = Math.max(1, (((data[i]*1.0D)/max)*((type == Types.BAR) ? ((2.0D/3)*getWidth()) : getHeight())));
						Rectangle2D rect = (type == Types.BAR) ? new Rectangle2D.Double(0, i*barWidth, barLength, barWidth) :
								new Rectangle2D.Double(i*barWidth, getHeight()-barLength, barWidth, barLength);
						
						area = new Area(rect);
						if ((i+1) == data.length) drawAxes(g2d, newColor);
						break;
					case LINE:
						if (i == 0) drawAxes(g2d, newColor);
						
						if ((i+1) < data.length){
							Line2D line = new Line2D.Double(((1.0D/(data.length-1))*getWidth())*i, getHeight()-(((data[i]*1.0D)/max)*(0.91D*getHeight())),
									((1.0D/(data.length-1))*getWidth())*(i+1), getHeight()-(((data[i+1]*1.0D)/max)*(0.91D*getHeight())));
							g2d.draw(line);
						}
						break;
					case PIE:
						//System.out.println(
						//		new BigDecimal(String.valueOf(data[i])).divide(total.multiply(
						//		new BigDecimal(String.valueOf(pieRemaining/360))), data.length, RoundingMode.FLOOR).multiply(
						//		new BigDecimal(String.valueOf(pieRemaining))));
						//BigDecimal extent = new BigDecimal(String.valueOf(data[i])).divide(new BigDecimal(String.valueOf(total)).multiply(new BigDecimal(String.valueOf(pieRemaining/360))), data.length, RoundingMode.FLOOR).multiply(new BigDecimal(String.valueOf(pieRemaining)));
						Arc2D slice = new Arc2D.Double(0, 0, getWidth(), getWidth(), arcLength, (((data[i]*1.0D)/(total*(pieRemaining/360)))*pieRemaining), Arc2D.PIE); //NOTE: min visible value = 0.02D
						
						//System.out.println(extent);
						if (prevArc <= slice.getAngleExtent() || prevValue == data[i]){
							//if (prevArc.compareTo(extent) <= 0 || prevValue == data[i]){
							area = new Area(slice);
							area.transform(AffineTransform.getRotateInstance(Math.toRadians(-90), getWidth()/2.0F, getHeight()/2.0F));
							pieRemaining -= slice.getAngleExtent();
							
							AffineTransform flip = AffineTransform.getScaleInstance(-1, 1);
							flip.translate(-getHeight(), 0);
							area.transform(flip);
							//System.out.println("data[" + i + "]: " + data[i] + "/" + total + " | arcLength: " + arcLength + " | slice: " + slice.getAngleExtent());
							arcLength += slice.getAngleExtent();
							//System.out.println(slice.getAngleExtent() + "+");
						}else{
							if (prev == null){
								//System.out.println(prevArc + " > " + extent + " | " +  data[i-1] + " > " + data[i]);
								prev = new Chart(type, a, b, (getComponentCount() > 0));
								prev.next = this;
								prev.data = new int[data.length-i];
								//System.out.println("data: " + data.length + " | prev: " + prev.data.length + " | i: " + i);
							}
							prev.data[prev.data.length-(data.length-i)] = data[i];
						}
						//					prevArc = extent;
						prevArc = slice.getAngleExtent();
						prevValue = data[i];
						break;
					default:
						break;
				}
				if (area != null && prev == null){
					g2d.fill(area);
					values.put(area, data[i]);
					
					Point loc = MouseInfo.getPointerInfo().getLocation();
					SwingUtilities.convertPointFromScreen(loc, this);
					if (area.contains(loc)){
						toolTip.setToolTipText(String.valueOf(data[i]) + unit);
					}else toolTip.setToolTipText(null);
				}
			}
			//System.out.println("Pie remaining: " + pieRemaining);
			//if (type != Types.PIE)
			g.drawImage(chart, 0, 0, null);
			//else g.drawImage(chart, getWidth(), 0, -getWidth(), getHeight(), null);
			if (prev != null) prev.updateData(prev.data, true);
		}
	}
	
	private void drawAxes(Graphics2D g2d, Color newColor){
		g2d.setColor(a);
		g2d.fillRect(0, 0, STROKE, getHeight());
		g2d.fillRect(getWidth()-STROKE, 0, STROKE, getHeight());
		g2d.setColor(b);
		g2d.fillRect(0, getHeight()-STROKE, getWidth(), STROKE);
		
		int intRed = (int) Math.min(Math.abs(((b.getRed()*0.5F)+(a.getRed()*0.5F))), 255), 
				intGreen = (int) Math.min(Math.abs(((b.getGreen()*0.5F)+(a.getGreen()*0.5F))), 255), 
				intBlue = (int) Math.min(Math.abs(((b.getBlue()*0.5F)+(a.getBlue()*0.5F))), 255);
		g2d.setColor(new Color(intRed, intGreen, intBlue));
		g2d.fillRect(0, getHeight()-STROKE, STROKE, STROKE);
		g2d.setColor(newColor);
	}
	
	public void setMax(int max){
		this.max = max;
	}
	
	public void setType(Types type){
		this.type = type;
		repaint();
	}
	
	public enum Types{
		BAR, PIE, HISTOGRAM, 
		/** 
		 * Creates a line chart with the default TimeUnit.DAYS.
		 * 
		 * @see java.util.concurrent.TimeUnit 
		 */
		LINE(TimeUnit.DAYS);
		
		public static final Types DEFAULT = PIE;
		public static final TimeUnit DEFAULT_TIME_UNIT = LINE.getUnit();
		
		private TimeUnit unit;
		/** Default value of 1 second */
		private static long millis = DEFAULT_TIME_UNIT.toMillis(1);
		private Types(){}
		
		private Types(TimeUnit unit){
			this.unit = unit;
		}
		
		public static Types byId(int id){
			return (id >= 0 && id < values().length) ? values()[id] : null;
		}

		/**
		 * Creates a line chart.
		 * 
		 * @param unit The time unit for the chart to go by
		 * @return a line chart with a specific TimeUnit
		 * 
		 * @see nlp.util.Chart.Types#LINE
		 * @see java.util.concurrent.TimeUnit
		 */
		public static Types LINE(TimeUnit unit){
			Types line = LINE;
			line.unit = unit;
			return line;
		}
		
		public static Types LINE(long millis){
			Types line = LINE;
			line.millis = millis;
			return line;
		}
		
		public TimeUnit getUnit(){
			if (this == LINE) return unit;
			return null;
		}
		
		public void setUnit(TimeUnit unit){
			if (this == LINE) this.unit = unit;
		}
		
		public void setUnitMillis(long millis){
			if (this == LINE) Types.millis = millis;
		}
		
		/**
		 * Returns in millis the time span between points on the line chart.<p>
		 * 
		 * Handles non-line based charts and whether or not the line
		 * chart had a TimeUnit specified (the duration of <b>1</b> TimeUnit), 
		 * or the time specified, in milliseconds (including the default value).
		 * 
		 * @return The time span in milliseconds between points on the line chart.
		 */
		public long getMillis(){
			if (this == LINE) return (unit != null) ? unit.toMillis(1L) : millis;
			return 0L;
		}
	}
}