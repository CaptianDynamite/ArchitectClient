package nlp.util;

public interface Loadable{
	/**
	 * Returns the amount of things to load.
	 * 
	 * @return the amount of things to load in the class
	 */
	public int thingsToLoad();
}
