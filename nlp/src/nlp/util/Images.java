package nlp.util;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileSystemView;

import nlp.NLP;
import nlp.NLP.DevModeArgs;
import nlp.desktop.apps.Applications;

/**
 * A static list of images and overall image utility. This is used to load, retrieve, and resize images.
 */
public enum Images implements Loadable{
	/* Icons */
	MAIN_ICON("nlp", 0),
	LOGO_PROGRESS("nlp_prog", false, false, false, 1),
	/* Application Icons */
	DEFAULT_APP("default"),
	PAUSE_APP("pause"),
//	ANALYTICS_APP(Applications.ANALYTICS, 2),
//	TEST_APP(Applications.TEST),
//	EMPTY_APP(Applications.EMPTY),
	ANALYTICS_APP("analytics_icon", 2),
	LOGIN_APP("login_icon"),
	TEST_APP("test_icon"),
	EMPTY_APP("empty_icon"),
	
	@Cullable
	BACKGROUND("background", true, true, false, 3),
	
	INVALID(null, false, false, false);
	
	private String img;
	private int priority;
	private boolean sizeToScreen;
	private boolean hasFSImg;
	private boolean isIconSet;
	
	public static Map<Images, Map<IconSizes, BufferedImage>> iconSets = new HashMap<Images, Map<IconSizes, BufferedImage>>();
	private static Map<String, BufferedImage> images = new HashMap<String, BufferedImage>();
//	private static Map<String, String> imageLocs = new HashMap<String, String>();
	
	Images(Applications app){
		this(app, -1);
	}
	
	Images(Applications app, int priority){
		this(app.getIconName(), false, false , true, priority);
		fixIcon(app);
	}
	
	Images(String icon){
		this(icon, -1);
	}
	
	Images(String icon, int priority){
		this(icon, false, false, true, priority);
	}
	
	/**
	 * Registers new images.
	 *
	 * @param img The name of the image
	 * @param needsFSImg Whether or not the image will have a full screen version
	 */
	Images(String img, boolean sizeToScreen, boolean needsFSImg, boolean isIconSet){
		this(img, sizeToScreen, needsFSImg, isIconSet, -1);
	}
	
	Images(String img, boolean sizeToScreen, boolean needsFSImg, boolean isIconSet, int priority){
		this.img = img;
		this.priority = priority;
		this.sizeToScreen = sizeToScreen;
		this.hasFSImg = needsFSImg;
		this.isIconSet = isIconSet;
	}
	
	private String getName(){
		return img;
	}
	
	public boolean isCulled(){
		try{
			return (DevModeArgs.CULL_ASSETS.getBool() && Images.class.getField(name()).isAnnotationPresent(Cullable.class));
		}catch (Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Returns the <code>BufferedImage</code> version of the <code>Images</code> object.<p>
	 *
	 * Handles images not having a full screen version, and will return the normal version.
	 *
	 * @param getFSImg Whether or not to getBool the full screen version of the image
	 * @return the <code>BufferedImage</code> version of the <code>Images</code> object
	 */
	public BufferedImage get(boolean getFSImg){
//		BufferedImage image = (getFSImg && hasFSImg) ? images.get("fs_"+img) : images.get(img);
//		System.out.println(img + "(" + getFSImg + "): " + image.getWidth() + "," + image.getHeight());
//		return image;
		return (getFSImg && hasFSImg) ? images.get("fs_"+img) : images.get(img);
	}
	
	public ImageIcon getIcon(){
		return getIcon(IconSizes.DEFAULT);
	}
	
	public ImageIcon getIcon(IconSizes size){
		if (iconSets.get(this) != null) return new ImageIcon(iconSets.get(this).get(size));
		return null;
	}
	
	/**
	 * Resizes the given <code>Images</code> object.
	 *
	 * @param img The <code>Images</code> object to be resized
	 * @param dim The new dimensions for the image
	 * @param override Whether or not to override the old image with the new image
	 * @return a resized version of the given <code>Images</code> object
	 *
	 * @see nlp.util.Scalr#resize(BufferedImage src, Scalr.Method scalingMethod, Scalr.Mode resizeMode, int targetWidth, int targetHeight, BufferedImageOp... ops)
	 */
	public static BufferedImage resize(Images img, Dimension dim, boolean override){
		BufferedImage resize = Scalr.resize(img.get(false), Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, (int) dim.getWidth(), (int) dim.getHeight());
		if (override) images.put(img.img, resize);
		return resize;
	}
	
//	public static BufferedImage reloadImage(Images img){
//		try{
//			BufferedImage reload = ImageIO.read(Main.class.getClassLoader().getResourceAsStream(imageLocs.getBool(img.img)));
//			images.put(img.img, reload);
//			return reload;
//		}catch (Exception e){}
//		return img.getBool();
//	}
	
	/**
	 * Loads all the images inside the images folder.
	 */
	public static void loadImages(){
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(NLP.class.getResourceAsStream("/images")));
			boolean canRead = true;
			reader.mark(1);
			
			try{
				String in = reader.readLine();
				if (in == null) throw new NullPointerException();
			}catch (NullPointerException e){
				canRead = false;
			}
			
			/* Loads files if they're not packaged in a jar */
			if (canRead){
				reader.reset();
				List<String> subFolders = new ArrayList<String>();
				
				String in = reader.readLine();
				while (in != null){
					if (!in.contains(".")) subFolders.add(in);
					in = reader.readLine();
				}
				
				in = null;
				for (int i = 0; i <= subFolders.size(); i++){
					String subPath = (i < subFolders.size()) ? "/"+subFolders.get(i) : "";
					reader = new BufferedReader(new InputStreamReader(NLP.class.getResourceAsStream("/images"+subPath)));
					
//					System.out.println("subPath: '" + subFolders.getBool(i) + "'");
					while ((in = reader.readLine()) != null){
						if (!in.contains(".") && i < subFolders.size()) subFolders.add(subFolders.get(i)+"/"+in);
						else{
							try{
								String name = in.split("\\.")[0];
								Images img = byName(name, NLP.class.getProtectionDomain().getCodeSource().getLocation()+"images"+subPath+"/"+in);
								
//								System.out.println("img: " + img);
								if (img != INVALID && !img.isCulled()){
									LoadingPanel.nowLoading(INVALID, "images"+subPath+"/"+in);
									images.put(name, ImageIO.read(NLP.class.getResourceAsStream("/images"+subPath+"/"+in)));
									if (img.hasFSImg){
										LoadingPanel.nowLoading(INVALID, "images"+subPath+"/fs_"+in);
										images.put("fs_"+name, resize(img, NLP.getScreen(), false));
									}
									if (img.sizeToScreen) resize(img, NLP.mainFrame.getPreferredSize(), true);
									if (img.isIconSet && (img.get(false).getWidth() > IconSizes.getMaxValue() || img.get(false).getHeight() > IconSizes.getMaxValue()))
										resize(img, new Dimension(IconSizes.getMaxValue(), IconSizes.getMaxValue()), true);
									
//									if (LOGO_PROGRESS.getBool(false) != null && MAIN_ICON.getBool(false) != null) NLP.loading.setVisible(true);
	//								imageLocs.put(in.split("\\.")[0], "images/"+subFolders.getBool(i)+"/"+in);
								}
							}catch (IOException io){io.printStackTrace();}
						}
					}
				}
			}else{
			    URL jar = NLP.class.getProtectionDomain().getCodeSource().getLocation();
				ZipInputStream zIn = new ZipInputStream(jar.openStream());

			    ZipEntry ze = null;
			    boolean readImgs = false;
			    while((ze = zIn.getNextEntry()) != null){
//			    	System.out.println("ze.getName(): " + ze.getName());
			    	if (ze.getName().startsWith("images")){
				        readImgs = true;
				        
				        String name = ze.getName().split("/")[ze.getName().split("/").length-1];
						Images img = byName(name.split("\\.")[0], ze.getName());
						if (img != INVALID && !img.isCulled()){
							LoadingPanel.nowLoading(INVALID, ze.getName());
							images.put(name.split("\\.")[0], ImageIO.read(NLP.class.getResourceAsStream("/"+ze.getName())));
							if (img.hasFSImg){
								LoadingPanel.nowLoading(INVALID, ze.getName().substring(0, ze.getName().length()-name.length()) + "fs_"+name);
								images.put("fs_"+name.split("\\.")[0], resize(img, NLP.getScreen(), false));
							}
							if (img.sizeToScreen) resize(img, NLP.mainFrame.getPreferredSize(), true);
						}
			    	}else if (readImgs) break;
			    }
			    zIn.close();
			}
			
			/* Creating the empty application images */
			BufferedImage empty = new BufferedImage(IconSizes.getMaxValue(), IconSizes.getMaxValue(), BufferedImage.TYPE_INT_ARGB);
			LoadingPanel.nowLoading(INVALID, "images/"+EMPTY_APP.getName());
			images.put(EMPTY_APP.getName(), empty);
			
			/* Final resizing of the loaded images */
			for (String image : images.keySet()){
//				BufferedImage scaledImage;
				Images img = byName(image);
				
				if (img.isIconSet){
					Map<IconSizes, BufferedImage> iconSet = new HashMap<IconSizes, BufferedImage>();
					
					for (IconSizes size : IconSizes.values()){
						LoadingPanel.nowLoading(INVALID, "images/icons/"+img.getName()+"_"+size.getValue()+".png");
						iconSet.put(size, resize(img, new Dimension(size.getValue(), size.getValue()), false));
					}
					iconSets.put(img, iconSet);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static void saveImageToDesktop(String name, BufferedImage img){
		try {
			File file = new File(FileSystemView.getFileSystemView().getHomeDirectory(), name + ".png");
			ImageIO.write(img, "png", file);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static void saveIconToDesktop(String name, Icon icon){
		BufferedImage img = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = img.createGraphics();
		icon.paintIcon(null, g, 0, 0);
		saveImageToDesktop(name, img);
	}
	
	/**
	 * Returns an <code>Images</code> version of the given name.
	 *
	 * @param name The name of the image
	 * @return an <code>Images</code> object based on the given name
	 */
//	private static Images byName(String name){
//		if (name != null && !name.isEmpty()){
//			for (Images value : values()){
//				if (value != INVALID && value.img != null && value.img.equals(name)) return value;
//			}
//		}
//		return INVALID;
//	}
	private static Images byName(String name){
		return byName(name, null);
	}

	private static Images byName(String name, String path){
		if (name != null && !name.isEmpty()){
			for (Images value : values()){
				if (value != INVALID && value.img != null && name.contains(value.img)){
					if (value.prioritizedName().equals(name)) return value;

					int priority = -1;
					try{
						priority = Integer.parseInt(name.split("_")[0]);
					}catch (Exception e){}
//					NLP.out.println(value + " | " + name + " | " + priority + " | " + name.substring(name.indexOf("_")+1) + " == " + value.img);
					if ((priority < 0 && value.img.equals(name))){
						if (path != null){
							//TODO add files to rename files by priority to list
						}
						return value;
					}
				}
			}
		}
		return INVALID;
	}
	
	private String prioritizedName(){
		return ((priority >= 0) ? priority+"_" : "") + img;
	}
	
	private void fixIcon(Applications app){
		try{
			Field icon = app.getClass().getDeclaredField("icon");
			boolean isAccessible = icon.isAccessible();
			
			if (!isAccessible) icon.setAccessible(true);
			icon.set(app, this);
			icon.setAccessible(isAccessible);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * All the valid images specified in {@link Images}.
	 */
	@Override
	public int thingsToLoad(){
		int totalImgs = 0;
		
		for (Images img : values()){
			if (img != INVALID){
				try{
					if (!img.isCulled()){
						if (img.isIconSet){
							if (iconSets.get(img) != null) totalImgs += iconSets.get(img).size();
							else totalImgs += IconSizes.values().length;
						}
						if (img.hasFSImg) totalImgs++;
						totalImgs++;
					}
				}catch (Exception e){
					e.printStackTrace(NLP.err);
				}
			}
		}
		return totalImgs;
	}
	
	public enum IconSizes{
		S_16, S_32, S_64, S_128, S_256;
		
		public static final IconSizes DEFAULT = S_32;
		public static final IconSizes MAX = getMax();
		
		private static IconSizes getMax(){
			IconSizes max = DEFAULT;
			
			for (IconSizes size : values()){
				max = (size.getValue() > max.getValue()) ? size : max;
			}
			return max;
		}
		
		private static int getMaxValue(){
			return MAX.getValue();
		}
		
		public int getValue(){
			try{
				return Integer.parseInt(name().split("_")[1]);
			}catch (Exception e){}
			return -1;
		}
	}
	
	/**
	 * Used to define cullable images.
	 *
	 * @see nlp.net.Packet
	 */
	@Target({ElementType.FIELD})
	@Retention(RetentionPolicy.RUNTIME)
	private @interface Cullable{}
}
