package nlp.desktop;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import nlp.NLP;
import nlp.NLP.DevModeArgs;
import nlp.desktop.TaskBar.AppStatus;
import nlp.desktop.apps.Application;
import nlp.desktop.apps.Applications;
import nlp.desktop.apps.comp.MovableComponentsPanel;
import nlp.desktop.apps.comp.PreviewFrame;
import nlp.net.Client;
import nlp.util.LoadingPanel;

public class Desktop extends JDesktopPane{
	
	public static final int iconGap = 10;
	public static final Color background = new Color(52, 57, 61);
	
//	private static GridBagConstraints c;
	private static Desktop desktop;
	private static JPanel container;
	private static TaskBar tasks;
	private static MovableComponentsPanel apps;
	
	private Desktop(){
		setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
		setDesktopManager(new DesktopManager());
		container = new JPanel(){
			@Override
			public void paint(Graphics g){
				super.paint(g);
				
				if (apps != null){
					Rectangle drag = apps.getDragRect();
					
					if (drag != null){
						g.setColor(Color.RED);
						g.drawRect(drag.x, drag.y, drag.width, drag.height);
					}
				}
			}
			
			@Override
			public Dimension getPreferredSize(){
				return Desktop.this.getPreferredSize();
			}
			
			@Override
			public Dimension getMaximumSize(){
				return getPreferredSize();
			}
			
			@Override
			public Dimension getSize(){
				return getPreferredSize();
			}
		};
		container.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 1.0;
		
		c.anchor = GridBagConstraints.PAGE_END;
		tasks = TaskBar.getTaskBar();
		tasks.setCrossDragging(true);
		container.add(tasks, c);

		apps = new MovableComponentsPanel(){
			@Deprecated
			@Override
			public Component add(Component comp){
				if (comp instanceof Application) return super.add(comp);
				return null;
			}

			@Override
			public Dimension getPreferredSize(){
				return desktop.getPreferredSize();
			}

			@Override
			public Dimension getMaximumSize(){
				return getPreferredSize();
			}
			
			@Override
			public Dimension getSize(){
				return getPreferredSize();
			}

			@Override
			public void paintComponent(Graphics g){
				g.setColor(background);
				g.fillRect(0, 0, getWidth(), getHeight());
				if (DevModeArgs.ALLOW_FORCED.getBool() && !Client.hasLoggedIn() && !Applications.LOGIN.getApp().getAppFrame().isVisible()){
					Applications.LOGIN.getApp().getAppFrame().setVisible(true);
					NLP.mainFrame.setResizable(false);
				}
			}
		};
		apps.setLayout(new FlowLayout(FlowLayout.LEADING, iconGap, iconGap));
		apps.setFocusable(false);

		c.gridy--;
		c.weighty = 1.0;
		c.gridheight = 2;
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(0, 0, tasks.getPreferredSize().height, 0);
		container.add(apps, c);
		add(container);
		addComponentListener(new ComponentAdapter(){
			@Override
			public void componentResized(ComponentEvent e){
				container.setSize(container.getPreferredSize());
				tasks.setSize(tasks.getPreferredSize());
				if (NLP.mainFrame.isVisible()) NLP.mainFrame.setVisible(true);
			}
		});
//		container.add(apps);
	}
	
	public static void loadApplications(){
		if (desktop == null){
			desktop = new Desktop();
			
			for (Applications apps : Applications.values()){
				Application app = apps.getApp();
				
				LoadingPanel.nowLoading(app, app.getLoadingText());
				if (!apps.isHiddenApp()) desktop.apps.add(app);
				else app.setVisible(false);
				desktop.add(app.getAppFrame());
			}
//			System.out.println(apps.getSize());
		}
	}
	
	public static Desktop getDesktop(){
		return desktop;
	}
	
	@Override
	public Component add(Component comp){
		for (Component c : getComponents()){
			if (c == comp) return comp;
		}
//		System.out.println("add: " + comp.getClass());
		if (comp instanceof Application) return apps.add(comp);
		return super.add(comp);
	}
	
	@Override
	public Component getComponentAt(Point p){
		return getComponentAt(p.x, p.y);
	}
	
	@Override
	public Component getComponentAt(int x, int y){
		return container.getComponentAt(x, y);
	}
	
	@Override
	public Dimension getPreferredSize(){
		Dimension size = NLP.mainFrame.getSize();
		Insets ins = NLP.mainFrame.getInsets();
		
		return new Dimension(size.width-(ins.left+ins.right), size.height-(ins.top+ins.bottom));
	}
	
	@Override
	public Dimension getSize(){
		return getPreferredSize();
	}
	
	@Override
	public int getWidth(){
		return getSize().width;
	}
	
	@Override
	public int getHeight(){
		return getSize().height;
	}
	
	private class DesktopManager extends DefaultDesktopManager{
		@Override
		public void dragFrame(JComponent frame, int newX, int newY){
			if (!(frame instanceof PreviewFrame)) super.dragFrame(frame, newX, newY);
		}
		
		@Override
		public void activateFrame(JInternalFrame frame){
			super.activateFrame(frame);
			
//			NLP.out.println("'"+ frame.getTitle() + "' activated");
			if (!(frame instanceof PreviewFrame)) TaskBar.setStatus(Applications.byName(frame.getTitle()), AppStatus.FOCUSED);
		}
		
		@Override
		public void endDraggingFrame(JComponent frame){
			super.endDraggingFrame(frame);
			
			Point p = MouseInfo.getPointerInfo().getLocation();
			SwingUtilities.convertPointFromScreen(p, desktop);
			
			if (!NLP.mainFrame.contains(p)){
				if (p.x < 0 || p.x > desktop.getWidth()){
					frame.setSize((int) (desktop.getWidth()/2.0F), desktop.getHeight()-TaskBar.getTaskBar().getHeight());
					frame.setLocation((p.x < 0) ? 0 : (int) (desktop.getWidth()/2.0F), 0);
				}else{
					try{
						frame.setLocation(frame.getX(), 0);
						((JInternalFrame) frame).setMaximum(true);
					}catch (Exception e){}
				}
			}else if (TaskBar.getTaskBar().getBounds().intersects(frame.getBounds()))
				frame.setLocation(frame.getX(), TaskBar.getTaskBar().getY()-frame.getHeight());
		}
		
		@Override
		public void deactivateFrame(JInternalFrame frame){
			super.deactivateFrame(frame);
			
//			NLP.out.println("deactivated");
			if (!(frame instanceof PreviewFrame)){
//				NLP.out.println(frame.getTitle() + " | " + Applications.EMPTY.name());
				Application app = Applications.byName(frame.getTitle());
				AppStatus status = TaskBar.getStatus(app);
				
				if (status != null && status != AppStatus.HIDDEN) TaskBar.setStatus(app, AppStatus.RUNNING);
			}
			else{
				SwingUtilities.invokeLater(new Runnable(){
					@Override
					public void run(){
						selectFrame(true);
					}
				});
			}
		}
	}
}
