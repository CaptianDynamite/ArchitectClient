package nlp.desktop.apps.comp;

import nlp.NLP;
import nlp.util.Images;
import nlp.util.Images.IconSizes;
import nlp.util.Scalr;

import javax.swing.*;
import javax.swing.plaf.basic.BasicProgressBarUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;

public class LogoProgressBar extends CustomProgressBar{

	private static final int MIN_OPACITY = 10;
	
	private int opacity = 255;
	private boolean hasCreatedImgs;
	
	private long fadeTime;
	private Color fill;
	private BufferedImage logo, progress, realProgress;
	
	public LogoProgressBar(int sideSize, long fadeTime){
		this(sideSize, fadeTime, null);
	}
	
	public LogoProgressBar(int sideSize, long fadeTime, Font font){
		this(sideSize, fadeTime, font, DEFAULT_FILL);
	}
	
	public LogoProgressBar(int sideSize, long fadeTime, Font font, Color fill){
		this(sideSize, fadeTime, font, fill, DEFAULT_TEXT);
	}
	
	public LogoProgressBar(int sideSize, long fadeTime, Font font, Color fill, Color text){
		super(sideSize, font, fill, text);
		this.fadeTime = fadeTime;
		this.fill = fill;
		
		setOpaque(false);
	}
	
	@Override
	void paintProgress(Graphics g, JComponent comp){
		if (hasCreatedImgs){
			if (isIndeterminate()){
				opacity = (int) (Math.floor(fadeTime - Math.abs((getElapsedTime() % (fadeTime * 2)) - fadeTime)) * (255.0D / fadeTime));
				if (opacity < MIN_OPACITY) opacity = MIN_OPACITY;
			}
			
			int width = (getPercentComplete() > 0) ? (int) (progress.getWidth()*getPercentComplete()) : ((!isIndeterminate()) ? 1 : progress.getWidth());
			realProgress = new BufferedImage(width, progress.getHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics2D g2d = realProgress.createGraphics();
			
			g2d.setColor(new Color(fill.getRed(), fill.getGreen(), fill.getBlue(), opacity));
			for (int y = 0; y < realProgress.getHeight(); y++){
				for (int x = 0; x < realProgress.getWidth(); x++){
					if (new Color(progress.getRGB(x, y), true).getAlpha() >= MIN_OPACITY) g2d.fillRect(x, y, 1, 1);
				}
			}
			g2d.dispose();
			
			g2d = (Graphics2D) g.create();
			g2d.drawImage(realProgress, 0, 0, null);
			g2d.drawImage(logo, 0, 0, null);
			g2d.dispose();
		}else createLoadingImages();
	}
	
	@Override
	public void setSize(int width, int height){}
	
	@Override
	public Dimension getSize(){
		return getPreferredSize();
	}
	
	@Override
	public void setIndeterminate(boolean isIndeterminate){
		super.setIndeterminate(isIndeterminate);
		if (!isIndeterminate) createLoadingImages();
	}
	
	private void createLoadingImages(){
		try{
			if (getParent() != null){
				logo = Images.MAIN_ICON.get(false);
				progress = Images.LOGO_PROGRESS.get(false);
				
				BufferedImage fixedLogo = new BufferedImage(logo.getWidth(), logo.getHeight(), BufferedImage.TYPE_INT_ARGB);
				BufferedImage fixedProgress = new BufferedImage(progress.getWidth(), progress.getHeight(), BufferedImage.TYPE_INT_ARGB);
				
				for (int i = 0; i < 2; i++){
					Graphics2D g = (i > 0) ? fixedProgress.createGraphics() : fixedLogo.createGraphics();
					
					g.setColor(((i > 0) ? fill : getParent().getBackground().darker().darker()));
					for (int y = 0; y < IconSizes.MAX.getValue(); y++){
						for (int x = 0; x < IconSizes.MAX.getValue(); x++){
							if (new Color(((i > 0) ? progress.getRGB(x, y) : logo.getRGB(x, y)), true).getAlpha() > 0)
								g.fillRect(x, y, 1, 1);
						}
					}
				}
				logo = Scalr.resize(fixedLogo, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, getSide(), getSide());
				progress = Scalr.resize(fixedProgress, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, getSide(), getSide());
				hasCreatedImgs = true;
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the timer used to update the indeterminate progress bar.
	 *
	 * @return the timer that updates the values used to calculate the position and speed of the indeterminate progress bar
	 */
	@Override
	Timer getPaintTimer(){
		Timer paintTimer = new Timer(1, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				BufferedImage fixedProgress = new BufferedImage(progress.getWidth(), progress.getHeight(), BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = fixedProgress.createGraphics();
				
				g.setColor(new Color(fill.getRed(), fill.getGreen(), fill.getBlue(), opacity));
				for (int y = 0; y < fixedProgress.getHeight(); y++){
					for (int x = 0; x < fixedProgress.getWidth(); x++){
						if (new Color(progress.getRGB(x, y), true).getAlpha() >= MIN_OPACITY) g.fillRect(x, y, 1, 1);
					}
				}
				g.dispose();
				progress = fixedProgress;
			}
		});
//		NLP.out.println("delay: " + paintTimer.getDelay());
//		NLP.out.println("fadeValue: " + fadeValue + " | " + paintTimer.getDelay());
		return paintTimer;
	}
}
