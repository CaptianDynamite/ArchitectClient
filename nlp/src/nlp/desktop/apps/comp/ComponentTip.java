package nlp.desktop.apps.comp;

import javax.swing.*;
import javax.swing.plaf.ToolTipUI;
import java.awt.*;

public class ComponentTip extends JToolTip{
	
	private static final long serialVersionUID = 1L;
	
	public ComponentTip(JComponent comp){
		if (comp != null){
			setPreferredSize(comp.getPreferredSize());
			setLayout(new BorderLayout());
			setOpaque(false);
			
			comp.setOpaque(false);
			add(comp);
			
			setUI(new ToolTipUI(){
				@Override
				public Dimension getMinimumSize(JComponent c){
					return c.getLayout().minimumLayoutSize(c);
				}
				@Override
				public Dimension getPreferredSize(JComponent c){
					return c.getLayout().preferredLayoutSize(c);
				}
				@Override
				public Dimension getMaximumSize(JComponent c){
					return getPreferredSize(c);
				}
			});
		}
	}
	
	@Override
	protected boolean isPaintingOrigin(){
		return true;
	}
}
