package nlp.desktop.apps.comp;

import nlp.NLP;
import nlp.desktop.Desktop;
import nlp.desktop.apps.Application;
import nlp.desktop.apps.Applications;

import javax.swing.*;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MovableComponentsPanel extends JPanel{
	
	private static final int POINTS_PER_SIDE = 1;
	
	private final MouseAdapter mouseListener;
	private final MouseMotionAdapter mouseMotionListener;
	
	private Component picked;
	private Component prevClosest;
	private MovableComponentsPanel crossed;
	
	private Point initLoc;
	private boolean isDragging;
	private boolean crossDragging;
	
	public MovableComponentsPanel(){
		mouseListener = getMouseListener();
		mouseMotionListener = getMouseMotionListener();
		
		addMouseListener(new MouseAdapter(){
			@Override
			public void mouseExited(MouseEvent e){
				super.mouseExited(e);
				
				Point p = e.getLocationOnScreen();
				SwingUtilities.convertPointFromScreen(p, getContainer());
				Component comp = getContainer().getComponentAt(p);
				
				if (comp != null && !comp.equals(MovableComponentsPanel.this) && comp instanceof MovableComponentsPanel)
					crossed = (MovableComponentsPanel) comp;
			}
			
			@Override
			public void mouseEntered(MouseEvent e){
				super.mouseEntered(e);
				
				if (crossed != null){
					crossed.isDragging = false;
					crossed = null;
				}
			}
		});
	}
	
	protected void onCrossDrag(Component dragged, MouseEvent e){
		MovableComponentsPanel cross = ((MovableComponentsPanel) dragged.getParent());
		
		if (!crossDragging) crossed = cross;
		initLoc = cross.initLoc;
		picked = dragged;
		isDragging = true;
	}
	protected void onCrossDrop(Component dropped, MouseEvent e){
		initLoc = null;
		picked = null;
		isDragging = false;
	}
	
	@Override
	public Component add(Component comp){
		return add(comp, -1);
	}
	
	@Override
	public Component add(Component comp, int index){
		comp.addMouseListener(mouseListener);
		comp.addMouseMotionListener(mouseMotionListener);
		return super.add(comp, index);
	}
	
	@Override
	public void paint(Graphics g){
		super.paint(g);
		
		if (isDragging){
			Component closest = (Component) getClosestComp(MouseInfo.getPointerInfo().getLocation())[0];
			
			if (closest != null && crossed == null){
				g.setColor(Color.WHITE);
				g.drawRect(closest.getX(), closest.getY(), closest.getWidth(), closest.getHeight());
				
				prevClosest = closest;
			}
		}
	}
	
	public void setCrossDragging(boolean canCrossDrag){
		this.crossDragging = canCrossDrag;
	}
	
	public Rectangle getDragRect(){
		if (isDragging && picked != null){
			Point p = MouseInfo.getPointerInfo().getLocation();
			SwingUtilities.convertPointFromScreen(p, this);
			return new Rectangle(p.x-initLoc.x, p.y-initLoc.y, picked.getWidth(), picked.getHeight());
		}
		return null;
	}
	
	protected Object[] getClosestComp(Point screenLoc){
		Component comp = null;
		int index = -1;
		
		double shortest = getWidth()*getHeight();
		boolean appsChecked = false;
		for (int i = 0; i < getComponentCount(); i++){
			Component curr = getComponent(i);
			
			if (!appsChecked){
				Point rel = new Point(screenLoc.x, screenLoc.y);
				
				SwingUtilities.convertPointFromScreen(rel, curr);
				if (curr.contains(rel)){
					comp = curr;
					index = i;
					return new Object[]{comp, index};
				}
				if (i == getComponentCount()-1){
					appsChecked = true;
					SwingUtilities.convertPointFromScreen(screenLoc, this);
					i = -1;
				}
			}else{
				for (int y = (curr.getY()+curr.getHeight()); y >= curr.getY(); y -= ((POINTS_PER_SIDE <= 1) ? curr.getHeight() : (curr.getHeight()/POINTS_PER_SIDE))){
					for (int x = (curr.getX()+curr.getWidth()); x >= curr.getX(); x -= ((POINTS_PER_SIDE <= 1) ? curr.getWidth() : (curr.getWidth()/POINTS_PER_SIDE))){
//						NLP.out.println("x: " + (x-comp.getX()) + " == " + comp.getWidth() + " || y: " + (y-comp.getY()) + " == " + comp.getHeight()); //DEBUG checked points
//						NLP.out.println("x: " + x + "/" + comp.getX() + " | y: " + y + "/" + comp.getY()); //DEBUG checked points
						if (((x == curr.getX() || x == curr.getX()+1) || x-curr.getX() == curr.getWidth()) || ((y == curr.getY() || y == curr.getY()+1) || y-curr.getY() == curr.getHeight())){
							double distance = Math.sqrt(Math.pow((screenLoc.x - x), 2) + Math.pow((screenLoc.y - y), 2));
							
							//DEBUG Checked points
//							Graphics g = getGraphics();
//							Color prevColor = g.getColor();
//
//							g.setColor(Color.RED);
//							g.drawRect(x, y, 1, 1);
//							g.setColor(prevColor);
							
							if (distance < shortest){
								shortest = distance;
								comp = curr;
								index = i;
							}
						}
					}
				}
//				NLP.out.println("---------------------------------------"); //DEBUG checked points
			}
		}
		return new Object[]{comp, index};
	}
	
	private void moveComp(Component comp, int index){
		List<Component> comps = new ArrayList<Component>(Arrays.asList(getComponents()));
		
		removeAll();
		comps.remove(comp);
		
		for (int i = 0; i < comps.size()+(index+1); i++){
			if (i != index) add(comps.get(i));
			else{
				add(comp);
				index = -1;
				i--;
			}
		}
	}
	
	private MouseAdapter getMouseListener(){
		return new MouseAdapter(){
			@Override
			public void mousePressed(MouseEvent e){
				super.mousePressed(e);
				
				SwingUtilities.invokeLater(new Runnable(){
					@Override
					public void run(){
						if (e.getButton() == MouseEvent.BUTTON1){
							prevClosest = e.getComponent();
							picked = prevClosest;
							initLoc = e.getPoint();
						}
					}
				});
			}
			
			@Override
			public void mouseReleased(MouseEvent e){
				super.mouseReleased(e);
				
				isDragging = false;
				SwingUtilities.invokeLater(new Runnable(){
					@Override
					public void run(){
						if (prevClosest != null){
							if (crossed != null){
								crossed.onCrossDrop(picked, e);
								crossed = null;
							}else{
								Object[] closest = getClosestComp(e.getLocationOnScreen());
								//NLP.out.println(Arrays.toString(closest));
								moveComp(picked, (int) closest[1]);
							}
							prevClosest = null;
							picked = null;
							getContainer().repaint();
							NLP.mainFrame.setVisible(true);
						}
					}
				});
			}
		};
	}
	
	private MouseMotionAdapter getMouseMotionListener(){
		return new MouseMotionAdapter(){
			@Override
			public void mouseDragged(MouseEvent e){
				super.mouseDragged(e);
				
				if (SwingUtilities.isLeftMouseButton(e)){
					isDragging = true;
					getContainer().repaint();
					if (crossed != null) crossed.onCrossDrag(picked, e);
				}
			}
		};
	}
	
	/**
	 * The highest level component containing the MoveableComponentsPanel
	 * that requires repainting.
	 *
	 * @return the highest containing component
	 */
	private Component getContainer(){
		return Desktop.getDesktop();
	}
}
