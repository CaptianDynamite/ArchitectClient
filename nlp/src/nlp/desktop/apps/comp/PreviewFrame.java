package nlp.desktop.apps.comp;

import nlp.NLP;
import nlp.desktop.Desktop;
import nlp.desktop.TaskBar;
import nlp.desktop.TaskBar.AppStatus;
import nlp.desktop.apps.Application;
import nlp.desktop.apps.Applications;
import nlp.util.Images;
import nlp.util.Scalr;

import javax.swing.*;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;

public class PreviewFrame extends JInternalFrame{
	
	private static final float PREVIEW_RATIO = 1.0F/6;
	
	public static final int PREVIEW_DELAY = 200;
	
	private JLabel preview;
	
	public PreviewFrame(JButton appPin){
		super(appPin.getName(), false, true, false, false);
		setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		setSize(getPreferredSize());
		
		addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				Application app = Applications.byName(getTitle());
				
				dispose();
				if (app != null){
					SwingUtilities.invokeLater(new Runnable(){
						@Override
						public void run(){
							AppStatus status = TaskBar.getStatus(app);
							
							if (!app.getAppFrame().isVisible()) app.getAppFrame().setVisible(true);
							else if (status == AppStatus.RUNNING){
								try{
									app.getAppFrame().setSelected(true);
								}catch (Exception ex){}
							}
						}
					});
				}
			}
			
			@Override
			public void mouseExited(MouseEvent e){
				Point loc = e.getLocationOnScreen();
				for (Component c : getComponents()){
					if (c instanceof BasicInternalFrameTitlePane && c.getParent() == PreviewFrame.this){
						SwingUtilities.convertPointFromScreen(loc, c);
						if (!c.contains(loc)) dispose();
					}
				}
			}
		});
		addInternalFrameListener(new InternalFrameAdapter(){
			@Override
			public void internalFrameClosing(InternalFrameEvent e){
				try{
					Applications.byName(getTitle()).getAppFrame().doDefaultCloseAction();
				}catch (Exception ex){}
			}
		});
		
		Insets ins = getInsets();
		Point loc = appPin.getLocationOnScreen();
		SwingUtilities.convertPointFromScreen(loc, nlp.desktop.Desktop.getDesktop());
		loc.x -= Desktop.iconGap;
		loc.y -= getHeight();
		loc.x -= (loc.x >= (int) ((getWidth()+ins.left+ins.right)/4.0F)) ? (int) (((getWidth()+ins.left+ins.right)/4.0F)-(Desktop.iconGap/2.0F)) : 0;
		if ((loc.x+ins.left+getWidth()+ins.right) > Desktop.getDesktop().getWidth()) loc.x = (Desktop.getDesktop().getWidth()-(ins.left+getWidth()+ins.right))+Desktop.iconGap;
		setLocation(loc.x, loc.y);
		setUI(new PreviewUI());
		
		preview = new JLabel();
		add(preview);
	}
	
	public void paintPreview(BufferedImage img){
		preview.setIcon(new ImageIcon(Scalr.resize(img, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, getWidth(), getHeight()-((BasicInternalFrameUI) getUI()).getNorthPane().getHeight())));
	}
	
	@Override
	public Dimension getPreferredSize(){
		return new Dimension((int) (NLP.mainFrame.getWidth()*PREVIEW_RATIO), (int) (NLP.mainFrame.getHeight()*PREVIEW_RATIO));
	}
	
	private class PreviewTitlePane extends BasicInternalFrameTitlePane{
		
		private PreviewTitlePane(){
			super(PreviewFrame.this);
		}
		
		@Override
		protected void addSystemMenuItems(JMenu systemMenu){}
		
		@Override
		protected JMenuBar createSystemMenuBar(){
			SystemMenuBar menuBar = new SystemMenuBar(){
				@Override
				public void paint(Graphics g){
					Applications.byName(PreviewFrame.this.getTitle()).getIcon(Images.IconSizes.S_16).paintIcon(this, g, 0, 0);
				}
			};
			menuBar.setBorderPainted(false);
			return menuBar;
		}
	}
	
	private class PreviewUI extends BasicInternalFrameUI{
		
		private PreviewUI(){
			super(PreviewFrame.this);
		}
		
		@Override
		protected JComponent createNorthPane(JInternalFrame w){
			return new PreviewTitlePane();
		}
	}
}
