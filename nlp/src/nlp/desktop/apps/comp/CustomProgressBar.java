package nlp.desktop.apps.comp;

import nlp.NLP;
import nlp.NLP.DevModeArgs;
import nlp.util.LoadingPanel;

import javax.swing.*;
import javax.swing.plaf.basic.BasicProgressBarUI;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.Format;

public abstract class CustomProgressBar extends JProgressBar{

	private static final int DEFAULT_MIN = 0;
	private static final int DEFAULT_MAX = 100;
	protected static final Format time = new DecimalFormat("#,###.#");
	protected static final Color DEFAULT_TEXT = Color.WHITE;
	protected static final Color DEFAULT_FILL = new Color(85, 85, 255, 255);
	
	private static final int TIMER_UPDATE_SPEED = 100;
	public final Timer timer;
	private final Timer paintTimer;
	private UpdatingToolTip toolTip;
	
	private float value;
	private long startTime;
	
	private int sideSize;
	private Color fill;
	private Color text;
	
	/**
	 * Constructs a new <code>CircularProgressBar</code>.<br>
	 * Uses the default font.
	 *
	 * @param sideSize The side size of the progress bar
	 */
	public CustomProgressBar(int sideSize){
		this(sideSize, null);
	}
	
	/**
	 * Constructs a new <code>CircularProgressBar</code>.<br>
	 * Uses a custom blue default color.
	 *
	 * @param sideSize The side size of the progress bar
	 * @param font The font of the text used in the progress bar
	 */
	public CustomProgressBar(int sideSize, Font font){
		this(sideSize, font, DEFAULT_FILL);
	}
	
	/**
	 * Constructs a new <code>CircularProgressBar</code>.<br>
	 * Uses white for the default text color.
	 *
	 * @param sideSize The side size of the progress bar
	 * @param font The font of the text used in the progress bar
	 * @param fill The color of the progress bar
	 */
	public CustomProgressBar(int sideSize, Font font, Color fill){
		this(sideSize, font, fill, DEFAULT_TEXT);
	}
	
	/**
	 * Constructs a new <code>CircularProgressBar</code>.
	 *
	 * @param sideSize The side size of the progress bar
	 * @param font The font of the text used in the progress bar
	 * @param fill The color of the progress bar
	 * @param text The color of the text used in the progress bar
	 */
	public CustomProgressBar(int sideSize, Font font, Color fill, Color text){
		this.sideSize = sideSize;
		this.fill = fill;
		this.text = text;
		this.timer = getElapsedTimer();
		this.paintTimer = getPaintTimer();
		
		if (font != null) setFont(font);
		setBackground(new Color(0,0,0,0));
		setForeground(fill);
		setBorder(BorderFactory.createEmptyBorder());
		setStringPainted(true);
		setUI(new CustomProgressUI());
		toolTip = new UpdatingToolTip(this);
	}
	
	abstract void paintProgress(Graphics g, JComponent comp);
	
	/**
	 * Returns the timer used to update the progress bar.
	 *
	 * @return the timer that updates the values used to calculate the position and speed of the indeterminate progress bar
	 */
	abstract Timer getPaintTimer();
	
	public int getSide(){
		return sideSize;
	}
	
	@Override
	public int getMinimum(){
		return (super.getMinimum() > 0) ? super.getMinimum() : DEFAULT_MIN;
	}
	
	@Override
	public int getMaximum(){
		return (super.getMaximum() > 0) ? super.getMaximum() : DEFAULT_MAX;
	}
	
	@Override
	public int getValue(){
		return (int) value;
	}
	
	/**
	 * Returns the value of the progress bar.
	 *
	 * @return the float value of the progress bar
	 */
	public float getValueF(){
		return value;
	}
	
	/**
	 * Returns the elapsed time in milliseconds.
	 *
	 * @return how long the timer has been running
	 */
	public float getElapsedTime(){
		return (0 < startTime) ? (float) (System.currentTimeMillis()-startTime) : 0;
	}
	
	@Override
	public double getPercentComplete(){
		return (value-getMinimum())/(getMaximum()-getMinimum());
	}
	
	@Override
	public boolean isVisible(){
		boolean isVisible = true;
		
		Component parent = getParent();
		while ((parent = parent.getParent()) != null){
			isVisible = parent.isVisible();
			if (!isVisible) break;
		}
		return isVisible;
	}
	
	@Override
	public Dimension getPreferredSize(){
		return getUI().getPreferredSize(null);
	}
	
	@Override
	public void setValue(int value){
		if (!isIndeterminate()) setValue((float) value);
	}
	
	/**
	 * Sets the value of the progress bar.
	 *
	 * @see #setValue(int)
	 */
	public void setValue(float value){
		if (!isIndeterminate()) this.value = value;
	}
	
	public Color getFill(){
		return fill;
	}
	
	public Color getText(){
		return text;
	}
	
	/**
	 * Returns the timer used to show how much time has elapsed.
	 *
	 * @return the timer that updates the tool tip to the current length of time the progress bar has been running
	 */
	private Timer getElapsedTimer(){
		return new Timer(TIMER_UPDATE_SPEED, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				toolTip.setToolTipText(time.format(getElapsedTime()/1000) + "s");
				if (DevModeArgs.DEV_MODE.getBool() && NLP.loading != null && NLP.loading.isVisible()) NLP.loading.setTitle("(" + getToolTipText() + ") " + NLP.TITLE);
				if (!CustomProgressBar.this.isVisible()) timer.stop();
			}
		});
	}
	
	protected class CustomProgressUI extends BasicProgressBarUI{
		
		/**
		 * Returns a <code>Dimension</code> equal to the side size.
		 */
		@Override
		public Dimension getPreferredSize(JComponent comp){
			return new Dimension(sideSize, sideSize);
		}
		
		/**
		 * Paints the progress bar.
		 *
		 * Handles the font, and progress bar size being too big or small.
		 *
		 * @see javax.swing.plaf.basic.BasicProgressBarUI#paint(Graphics, JComponent)
		 */
		@Override
		public void paint(Graphics g, JComponent comp){
			if (startTime <= 0) startTime = System.currentTimeMillis();
			if (isIndeterminate() && !paintTimer.isRunning()) paintTimer.start();
			if (!timer.isRunning()) timer.start();
			
			paintProgress(g, comp);
			
			if (!isIndeterminate() && isStringPainted()){
				String progress = new DecimalFormat("#,###.###").format(getPercentComplete()*100).replace(",", ".") + "%";
				Rectangle2D bounds = g.getFontMetrics().getStringBounds(progress, g);
				
				g.setColor(text);
				g.drawString(progress, (int) ((comp.getWidth()-bounds.getWidth())/2), (int) (((comp.getHeight()-bounds.getHeight())/2)+Math.sqrt(bounds.getHeight()*8.5)));
			}
			getParent().repaint();
			if (!CustomProgressBar.this.isVisible()) paintTimer.stop();
		}
	}
}
