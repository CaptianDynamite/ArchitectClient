package nlp.desktop.apps.comp;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;

public class UpdatingToolTip extends JComponent{
	
	public UpdatingToolTip(JComponent parent){
		ToolTipManager.sharedInstance().registerComponent(parent);
		parent.add(this);
		setVisible(true);
	}
	
	@Override
	public void setToolTipText(String text){
		super.setToolTipText(text);
		((JComponent) getParent()).setToolTipText(getToolTipText());
		
		Point loc = MouseInfo.getPointerInfo().getLocation();
		SwingUtilities.convertPointFromScreen(loc, getParent());
		if (getParent().contains(loc))
			ToolTipManager.sharedInstance().mouseMoved(new MouseEvent(getParent(), 0, System.currentTimeMillis(), 0, loc.x, loc.y, 0, false, 0));
	}
}
