package nlp.desktop.apps.comp;

import nlp.NLP;
import nlp.desktop.apps.comp.CustomProgressBar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Arc2D;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JProgressBar;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.plaf.basic.BasicProgressBarUI;

/**
 * A <code>JProgressBar</code> in the shape of a circle, used to display the progress of a task.
 */
public class CircularProgressBar extends CustomProgressBar{
	
	private static final long serialVersionUID = 1L;
	
	private final float barWidth = (1.0F/3F);
	
	private final float indetermDegree = 37.5F;
	private final float fallingDegree = 56.25F;
	private final float termVel = indetermDegree*(1.0F/5);
	private final float initVelocity = 4.0F;
	private float velocity = initVelocity;
	private float cycleValue;
	
	private boolean fixedFont;

	/**
	 * Constructs a new <code>CircularProgressBar</code>.<br>
	 * Uses the default font.
	 * 
	 * @param diameter The diameter of the progress bar
	 */
	public CircularProgressBar(int diameter){
		this(diameter, null);
	}
	
	/**
	 * Constructs a new <code>CircularProgressBar</code>.<br>
	 * Uses a custom blue default color.
	 * 
	 * @param diameter The diameter of the progress bar
	 * @param font The font of the text used in the progress bar
	 */
	public CircularProgressBar(int diameter, Font font){
		this(diameter, font, new Color(85, 85, 255));
	}
	
	/**
	 * Constructs a new <code>CircularProgressBar</code>.<br>
	 * Uses white for the default text color.
	 * 
	 * @param diameter The diameter of the progress bar
	 * @param font The font of the text used in the progress bar
	 * @param bar The color of the progress bar
	 */
	public CircularProgressBar(int diameter, Font font, Color bar){
		this(diameter, font, bar, Color.WHITE);
	}
	
	/**
	 * Constructs a new <code>CircularProgressBar</code>.
	 * 
	 * @param diameter The diameter of the progress bar
	 * @param font The font of the text used in the progress bar
	 * @param bar The color of the progress bar
	 * @param text The color of the text used in the progress bar
	 */
	public CircularProgressBar(int diameter, Font font, Color bar, Color text){
		super(diameter, font, bar, text);
		
		if (font != null) super.setFont(font.deriveFont((diameter*barWidth)/2));
	}
	
	@Override
	void paintProgress(Graphics g, JComponent comp){
		int diameter = getWidth();
		Graphics2D g2d = (Graphics2D) g.create();
		
		if (!fixedFont) setFont(getFixedFont(getFont()));
		
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setPaint(getFill());
		
		double degree = (!isIndeterminate()) ? (360 * (getValueF()/getMaximum())) : indetermDegree;
		double id = diameter-(diameter*barWidth);
		Ellipse2D inner = new Ellipse2D.Double(((diameter*1.0F)/2)-(id/2), ((diameter*1.0F)/2)-(id/2), id, id);
		Arc2D outer = new Arc2D.Double(0, 0, diameter, diameter, 90 - degree - ((!isIndeterminate()) ? 0 : cycleValue), degree, Arc2D.PIE);
		Area area = new Area(outer);
		area.subtract(new Area(inner));
		g2d.fill(area);
		g2d.dispose();
//		if ((isIndeterminate() && cycleValue >= 360) || (!isIndeterminate() && getValue() >= getMaximum())) System.out.println("Loaded in " + getElapsedTime()/1000 + "s");
	}
	
	/**
	 * Returns a fixed version of the font.
	 * 
	 * @param font The font to be fixed
	 * @return the font properly fixed
	 */
	private Font getFixedFont(Font font){
		double id = getDiameter()-(getDiameter()*barWidth);
		Ellipse2D inner = new Ellipse2D.Double(((getDiameter()*1.0F)/2)-(id/2), ((getDiameter()*1.0F)/2)-(id/2), id, id);
		
		font = font.deriveFont((float) id);
		
		float size = font.getSize();
		Rectangle2D bounds = getGraphics().getFontMetrics(font).getStringBounds("100.00%", getGraphics());
		while (bounds.getWidth() > inner.getWidth()){
			font = font.deriveFont(--size);
			bounds = getGraphics().getFontMetrics(font).getStringBounds("100.00%", getGraphics());
		}
		fixedFont = true;
		return font.deriveFont((size-2));
	}
	
	/**
	 * Returns the number of frames for the indeterminate progress bar.
	 * 
	 * @return the number of frames needed to complete one pass around indeterminate version of the progress bar
	 */
	private int getFramesToComplete(){
		float velocity = initVelocity;
		
		int frames = 0;
		for (float value = 0; value < 360; frames++){
			velocity += (value >= (fallingDegree+180)) ? -(initVelocity*1.5F) :
					((value >= fallingDegree) ? ((velocity < termVel) ? (initVelocity*1.5F) : 0) : 0);
			velocity = (velocity >= initVelocity) ? velocity : initVelocity;
			value += (value < 360) ? velocity : -value;
		}
//		System.out.println("frames needed: " + frames + " | 1000.0F/"+frames+" = "+(int)(1000.0F/frames));
		return frames;
	}
	
	/**
	 * Returns the timer used to update the indeterminate progress bar.
	 * 
	 * @return the timer that updates the values used to calculate the position and speed of the indeterminate progress bar
	 */
	@Override
	Timer getPaintTimer(){
		Timer paintTimer = new Timer((int) (1000.0F/getFramesToComplete()), new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				velocity += (cycleValue >= (fallingDegree+180)) ? -(initVelocity*1.5F) :
						((cycleValue >= fallingDegree) ? ((velocity < termVel) ? (initVelocity*1.5F) : 0) : 0);
				velocity = (velocity >= initVelocity) ? velocity : initVelocity;
				cycleValue += (cycleValue < 360) ? velocity : -cycleValue;
			}
		});
		return paintTimer;
	}
	
	/**
	 * Returns the diameter of the progress bar.
	 * 
	 * @return the diameter of the progress bar in pixels
	 */
	public int getDiameter(){
		return getSide();
	}
}
