package nlp.desktop.apps;

import nlp.NLP;
import nlp.net.Client;
import nlp.net.LoginPacket;
import nlp.panel.comp.ResizingComponent;
import nlp.panel.comp.TextComponents;
import nlp.util.Images;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.RoundRectangle2D;

@Hidden
public class Login extends ForcedApplication{
	
	private static final Color background = new Color(52, 57, 61, 245);
	private static final Color textColor = background.brighter().brighter();
	private static final Insets backIns = new Insets(50, 20, 50, 20);
	
	@Override
	AppPanel createAppWindow(){
		GridBagConstraints c = new GridBagConstraints();
		
		JButton login = new JButton("Login"){
			@Override
			public void setEnabled(boolean isEnabled){
				super.setEnabled(isEnabled);
				setText((isEnabled) ? "Login" : "Logging in...");
			}
		};
		JLabel userLabel = new JLabel("Username: ");
		JTextField user = new JTextField(NLP.USER_LENGTH);
		JPasswordField code = new JPasswordField(NLP.USER_LENGTH);
		AppPanel window = new AppPanel(){
			/**
			 * Paints the rounded rectangle background and all other components.
			 */
			@Override
			public void paintComponent(Graphics g){
				super.paintComponent(g);
				
				Graphics2D g2d = (Graphics2D) g;
				
				if (!Images.BACKGROUND.isCulled()) g2d.drawImage(Images.BACKGROUND.get(NLP.mainFrame.getExtendedState() == JFrame.MAXIMIZED_BOTH), null, 0, 0);
				else{
					g2d.setColor(background.darker());
					g2d.fillRect(0, 0, getWidth(), getHeight());
				}
				
				RoundRectangle2D panel = new RoundRectangle2D.Double(userLabel.getX()-backIns.left, user.getY()-backIns.top,
						login.getWidth()+(backIns.left+backIns.right), (login.getY()-user.getY())+user.getHeight()+(backIns.top+backIns.bottom), 20, 20);
				g2d.setColor(background);
				g2d.fill(panel);
			}
			
			/**
			 * Sets the visibility of the login panel.<br>
			 * Requests the focus in the window be given to the user text field.
			 */
			@Override
			public void setVisible(boolean isVisible){
				super.setVisible(isVisible);
				if (isVisible){
					user.requestFocusInWindow();
				}
			}
		};
		
		window.setLayout(new GridBagLayout());
		window.setPreferredSize(getPreferredSize());
		window.setOpaque(false);
		
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.LINE_END;
		
		userLabel.setFont(TextComponents.LOGIN_PANEL_LABEL.getFixedFont(this, userLabel));
		userLabel.setForeground(textColor);
		c.insets = new Insets(userLabel.getMinimumSize().height, 5, 5, 5);
		window.add(userLabel, c);
		c.gridy++;
		
		JLabel passLabel = new JLabel("Password: ");
		passLabel.setFont(TextComponents.LOGIN_PANEL_LABEL.getFixedFont(this, passLabel));
		passLabel.setForeground(textColor);
		window.add(passLabel, c);
		
		c.anchor = GridBagConstraints.LINE_START;
		c.gridx++;
		c.gridy = 0;
		
		
		user.setFont(TextComponents.LOGIN_PANEL_LABEL.getFixedFont(this, user));
		user.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, getBackground().brighter(), getBackground().darker()));
		user.setBackground(background);
		user.setForeground(textColor);
		user.setFocusTraversalKeysEnabled(false);
		user.addKeyListener(new KeyListener(){
			public void keyPressed(KeyEvent e){
				if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_TAB) code.requestFocusInWindow();
			}
			public void keyReleased(KeyEvent e){}
			public void keyTyped(KeyEvent e){}
		});
		window.add(user, c);
		code.setFont(TextComponents.LOGIN_PANEL_LABEL.getFixedFont(this, code));
		code.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, getBackground().brighter(), getBackground().darker()));
		code.setBackground(background);
		code.setForeground(textColor);
		code.setFocusTraversalKeysEnabled(false);
		code.addKeyListener(new KeyListener(){
			public void keyPressed(KeyEvent e){
				if (e.getKeyCode() == KeyEvent.VK_TAB) login.requestFocusInWindow();
				else if (e.getKeyCode() == KeyEvent.VK_ENTER) login.doClick();
			}
			public void keyReleased(KeyEvent e){}
			public void keyTyped(KeyEvent e){}
		});
		c.gridy++;
		window.add(code, c);
		
		c.gridx = 0;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		login.setFont(TextComponents.LOGIN_PANEL_LOGIN.getFixedFont(this, login));
		login.setFocusPainted(false);
		login.setBackground(new Color(85, 85, 255));
		login.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (user.getText().length() > 0 && !user.getText().isEmpty() && code.getPassword().length > 0 && !String.valueOf(code.getPassword()).isEmpty()){
					Client.sendPacket(new LoginPacket(user.getText(), String.valueOf(code.getPassword())));
					
					login.setEnabled(false);
					Timer timeout = new Timer(Client.TIMEOUT, new ActionListener(){
						@Override
						public void actionPerformed(ActionEvent e){
							login.setEnabled(true);
						}
					});
					timeout.setRepeats(false);
					timeout.start();
				}
			}
		});
		c.insets = new Insets(login.getMinimumSize().height, 5, 5, 5);
		c.gridy++;
		window.add(login, c);
		return window;
	}
	
	@Override
	public int thingsToLoad(){
		return 6;
	}
}
