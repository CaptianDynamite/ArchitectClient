package nlp.desktop.apps;

import nlp.NLP;
import nlp.NLP.DevModeArgs;
import nlp.cmd.Commands;
import nlp.cmd.PermissionCommand;
import nlp.net.Client;
import nlp.panel.comp.TextComponents;

import javax.swing.*;
import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;
import java.awt.*;
import java.awt.event.*;
import java.io.PrintStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Console extends ForcedApplication{
	
	private static final String AUTHOR_MSG = "Console written by regice202";
	
	private static final Color foreground = Color.WHITE;
	private static final Color background = new Color(52, 57, 61);
	private static final Color userInput = Color.GREEN;
	private static final Color error = new Color(235, 20, 80);
	
	private boolean isCustomStreams;
	
	private int tabIndex;
	private boolean isFirstTab = true;
	private List<String> prevTabOptions;
	
	private JTextPane console;
	private static JScrollPane scroll;
	private static HTMLDocument doc;
	private static Element paragraph;
	
	Console(){
		setForced(false);
		prevTabOptions = new ArrayList<String>();
	}
	
	@Override
	AppPanel createAppWindow(){
		AppPanel window = new AppPanel();
		GridBagConstraints c = new GridBagConstraints();
		Font font = null;
		
		try{
			font = new Font("Consolas", Font.PLAIN, 12);
		}catch (Exception e){}
		font = TextComponents.CONSOLE_PANEL_CONSOLE.getFixedFont(this, ((font != null) ? font : window.getFont()));
		window.setFont(font);
		
		window.setLayout(new GridBagLayout());
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.PAGE_END;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.fill = GridBagConstraints.HORIZONTAL;
		
		JTextField input = new JTextField();
		input.setBackground(background);
		input.setForeground(foreground);
		input.setCaretColor(userInput);
		input.setFocusTraversalKeysEnabled(false);
		input.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String text = input.getText();
				
				while (text.startsWith(" ")) text = text.substring(1);
				while (text.endsWith(" ")) text = text.substring(0, text.length()-1);
				
				if (!text.isEmpty()){
					println(text, userInput, false);
					runCommand(text);
				}
				input.setText(null);
			}
		});
		input.addKeyListener(new KeyAdapter(){
			@Override
			public void keyPressed(KeyEvent e){
				if (e.getKeyCode() == KeyEvent.VK_TAB){
					String text = input.getText();
					String[] split = text.split(" ");
					
					Commands command = Commands.byAlias(split[0]);
					if (text.endsWith(" ")){
						String[] fixed = new String[split.length+1];
						for (int i = 0; i < split.length; i++) fixed[i] = split[i];
						fixed[split.length] = "";
						split = fixed;
					}
					
					if (isFirstTab) prevTabOptions = (text != null && command != null) ?
							getMatching(command.onTabComplete(split), split[split.length-1]) : getMatching(Commands.HELP.onTabComplete(null), split[0]);
					
					if (prevTabOptions != null && !prevTabOptions.isEmpty()){
						if (isFirstTab && prevTabOptions.size() > 1){
							println(prevTabOptions.toString().substring(1).replace("]", ""), null, false);
							isFirstTab = false;
							input.setText(appendTabText(split, prevTabOptions.get(0)));
							tabIndex = 1;
						}else if (prevTabOptions.size() < 2) input.setText(appendTabText(split, prevTabOptions.get(0)));
						else{
							input.setText(appendTabText(split, prevTabOptions.get(tabIndex)));
							tabIndex += ((tabIndex+1) < prevTabOptions.size()) ? 1 : -tabIndex;
						}
					}
				}else if (!isFirstTab) isFirstTab = true;
			}
		});
		window.add(input, c);
		
		c.gridy--;
		c.gridheight = 2;
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(0, 0, input.getMinimumSize().height, 0);
		
		console = new JTextPane();
		console.setFont(font);
		console.setEditable(false);
		console.setFocusable(false);
		console.setContentType("text/html");
		console.setBackground(background);
		console.setForeground(foreground);
		console.addMouseListener(new MouseAdapter(){
			@Override
			public void mousePressed(MouseEvent e){
				console.setFocusable(true);
				console.requestFocusInWindow();
			}
			
			@Override
			public void mouseReleased(MouseEvent e){
				console.setFocusable(false);
				input.requestFocusInWindow();
			}
		});
		
		doc = (HTMLDocument) console.getStyledDocument();
		try{
			doc.insertAfterStart(doc.getDefaultRootElement().getElement(0), "<p id=\"messages\" font=\"#DEFAULT\" color=\"rgb("+foreground.getRed()+","+foreground.getGreen()+","+foreground.getBlue()+")\""
					+" face=\""+window.getFont().getName()+"\" size-px=\""+window.getFont().getSize()+"\" style=\"margin-top: 0\">"
					+"<font color=\"rgb("+userInput.getRed()+","+userInput.getGreen()+","+userInput.getBlue()+")\">"+AUTHOR_MSG+"</font><br></p>");
			paragraph = doc.getElement("messages");
		}catch (Exception e){}
		
		scroll = new JScrollPane(console);
		scroll.getVerticalScrollBar().setUnitIncrement(getFontMetrics(console.getFont()).getHeight()*2);
		window.add(scroll, c);
		
		if (!DevModeArgs.ALLOW_FORCED.getBool() && NLP.useCustomPrintStreams && !NLP.debugCustomPrintStreams) redirectStreams();
		return window;
	}
	
	/**
	 * Runs commands.<p>
	 *
	 * Handles whether or not the command is valid, prints why they're invalid if they are;
	 * checks the permissions of the <code>Client</code> to see if the <code>Client</code> can run it.
	 *
	 * @param input The input from the console
	 */
	private void runCommand(String input){
		Commands cmd = Commands.byAlias(input.split(" ")[0]);
		String[] args = (input.split(" ").length > 1) ? input.substring(input.split(" ")[0].length()+1).split(" ") : null;
		
		if (args != null){
			List<String> newArgs = new ArrayList<String>();
			for (String arg : args){
				if (!arg.isEmpty()) newArgs.add(arg);
			}
			args = newArgs.toArray(new String[newArgs.size()]);
		}
		
		if (cmd != null && Client.hasPermission(cmd.getPermsRawValue())){
			try{
				if (!(cmd.get() instanceof PermissionCommand) && cmd != Commands.YES_NO) cmd.execute(args);
				else cmd.execute(input.split(" "));
			}catch (Exception e){
				e.printStackTrace();
			}
		}else if (Client.hasPermission(Commands.HELP.getPerm())){
			System.err.println("Error: Invalid command!");
			runCommand(Commands.HELP.getCommand());
		}else System.out.println("How did i getBool here?...");
	}
	
	/**
	 * Returns a list of matching options based on the given argument.<br>
	 * Matching is based on if the option starts with the given arguemnt.
	 *
	 * @param options The options to choose from
	 * @param arg The argument to match the options to
	 * @return a list of matching options based on the given argument
	 */
	private List<String> getMatching(List<String> options, String arg){
		List<String> matching = new ArrayList<String>();
		
		if (options != null && !options.isEmpty()){
			if (!arg.equals("")){
				for (String option : options){
					if (!option.toLowerCase().equals(arg.toLowerCase()) && option.toLowerCase().startsWith(arg.toLowerCase())){
						matching.add(option);
					}
				}
			}else matching = options;
		}
		return matching;
	}
	
	/**
	 * Returns the appended input text line with the matching option.
	 *
	 * @param in The input text field text split by a single space
	 * @param opt The option to append to the input text field
	 * @return the new input text field text
	 *
	 * @see #getMatching(List, String)
	 */
	private String appendTabText(String[] in, String opt){
		String append = "";
		
		for (int i = 0; i < in.length-1; i++) append += in[i] + " ";
		return append + opt;
	}
	
	private static void println(String text, Color color, boolean error){
		print(text+"<br>", color, error);
	}
	
	private static void print(String text, Color color, boolean error){
		String timestamp = (NLP.useTimestamps) ? "["+new SimpleDateFormat("HH:mm:ss.ms").format(new Timestamp(new Date().getTime()))+"] " : "";
		color = (!error) ? ((color != null) ? color : foreground) : Console.error;
		
//		if (error) NLP.out.println("text(pre): '" + text.replaceAll("\\n", "<br>") + "'");
		if (text.replaceAll("\\n", "").replaceAll("\\r", "").length() > 0)
			text = timestamp+"<font color=\"rgb("+color.getRed()+","+color.getGreen()+","+color.getBlue()+")\">"+text;
		text = text.replaceAll("\\n", "<br>").replaceAll("\\r", "");
		
//		if (error) NLP.out.println("text(post): '" + text + "'");
		try{
			doc.insertBeforeEnd(paragraph, text);
		}catch (Exception e){
			e.printStackTrace(NLP.err);
		}
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run(){
				scroll.getVerticalScrollBar().setValue(scroll.getVerticalScrollBar().getMaximum());
			}
		});
//		NLP.out.println(console.getText());
	}
	
	public void redirectStreams(){
		if (!isCustomStreams){
			System.setOut(new PrintStream(System.out){
				@Override
				public void println(Object obj){
					byte[] data = (obj.toString() + "\n").getBytes();
					write(data, 0, data.length);
				}
				
				@Override
				public void print(Object obj){
					byte[] data = obj.toString().getBytes();
					write(data, 0, data.length);
				}
				
				@Override
				public void write(byte[] buf, int off, int len){
					String out = new String(buf, off, len);
					
					if (!NLP.useCustomPrintStreams || NLP.debugCustomPrintStreams) NLP.out.write(buf, off, len);
					else{
						SwingUtilities.invokeLater(new Runnable(){
							@Override
							public void run(){
								Console.print(out.replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("  ", "&nbsp;&nbsp;"), foreground, false);
							}
						});
					}
				}
			});
			System.setErr(new PrintStream(System.err){
				@Override
				public void println(Object obj){
					if (obj instanceof Exception) obj = new PrintableException((Exception) obj);
					byte[] data = (obj.toString() + "<br>").getBytes();
					write(data, 0, data.length);
				}
				
				@Override
				public void print(Object obj){
					if (obj instanceof Exception) obj = new PrintableException((Exception) obj);
					byte[] data = obj.toString().getBytes();
					write(data, 0, data.length);
				}
				
				@Override
				public void write(byte[] buf, int off, int len){
					String out = new String(buf, off, len);
					
					if (!NLP.useCustomPrintStreams || NLP.debugCustomPrintStreams) NLP.err.write(buf, off, len);
					else{
						SwingUtilities.invokeLater(new Runnable(){
							@Override
							public void run(){
								Console.print(out.replaceAll("\t", "&nbsp&nbsp&nbsp&nbsp&nbsp "), error, true);
							}
						});
					}
				}
			});
		}else{
			System.setOut(NLP.out);
			System.setErr(NLP.err);
		}
		isCustomStreams = !isCustomStreams;
	}
	
	/**
	 * Clears the console of all text.
	 */
	public void clearConsole(){
		try{
			doc.remove(0, doc.getLength());
			paragraph = doc.getElement("messages");
		}catch (Exception e){
			e.printStackTrace(NLP.err);
		}
	}
	
	private static class PrintableException extends Exception{
		
		private static final long serialVersionUID = 1L;
		
		private Exception e;
		
		PrintableException(Exception e){
			super(e);
			this.e = e;
			setStackTrace(e.getStackTrace());
		}

//		@Override
//		public StackTraceElement[] getStackTrace(){
//			List<StackTraceElement> elements = new ArrayList<StackTraceElement>();
//			StackTraceElement[] stackTrace = super.getStackTrace();
//
//			for (int i = 0; i < stackTrace.length; i++){
////				if (i > 0) elements.add(stackTrace[i]);
//				elements.add(stackTrace[i]);
//			}
//			return elements.toArray(new StackTraceElement[elements.size()]);
//		}
		
		@Override
		public String toString(){
			return e.toString();
		}
	}
	
	@Override
	public int thingsToLoad(){
		return 2;
	}
}
