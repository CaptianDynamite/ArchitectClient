package nlp.desktop.apps;

import nlp.NLP;
import nlp.util.Chart;
import nlp.util.Chart.Types;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.management.ManagementFactory;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Analytics extends Application implements Trackable{
	
	static final int DEFAULT_UPDATE_DELAY = 1000;
	static final boolean DEFAULT_OVERRIDE_DATA = true;
	public static final Timer updateTimer = getUpdateTimer();
	
	private static final Map<Trackable, Long> tracked = new HashMap<Trackable, Long>();
	private static final Map<Trackable, Chart> charts = new HashMap<Trackable, Chart>();
	
	private static GridBagConstraints c = new GridBagConstraints();
	private static JPanel graphs = new JPanel();
	
	@Override
	AppPanel createAppWindow(){
		AppPanel window = new AppPanel();
		GridBagConstraints c = new GridBagConstraints();
		
		graphs.setBackground(new Color(52, 57, 61));
		graphs.setLayout(new GridBagLayout());
		
		window.setLayout(new GridBagLayout());
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1.0D;
		c.weighty = 1.0D;
		c.fill = GridBagConstraints.BOTH;
		window.add(graphs, c);
		window.setBackground(graphs.getBackground());
		return window;
	}
	
	@Override
	public int thingsToLoad(){
		return 0;
	}
	
	private static int getUpdateDelay(Trackable track){
		int delay = DEFAULT_UPDATE_DELAY;
		
		try{
			delay = track.getClass().getDeclaredMethod("getValues").getAnnotation(Track.class).updateDelay();
		}catch (Exception e){}
		return delay;
	}
	
	private static boolean getOverrideData(Trackable track){
		boolean override = DEFAULT_OVERRIDE_DATA;
		
		try{
			override = track.getClass().getDeclaredMethod("getValues").getAnnotation(Track.class).overrideData();
		}catch (Exception e){}
		return override;
	}
	
	private static Types getChartType(Trackable track){
		Types type = Types.DEFAULT;
		try{
			type = track.getClass().getDeclaredMethod("getValues").getAnnotation(Track.class).chartType();
		}catch (Exception e){}
		return type;
	}
	
	private static String getUnit(Trackable track){
		String unit = null;
		try{
			unit = track.getClass().getDeclaredMethod("getValues").getAnnotation(Track.class).unit();
		}catch (Exception e){}
		return unit;
	}
	
	static void registerTrackable(Trackable track){
		tracked.put(track, -1L);
		
		if (charts.size() == 0){
			c.gridx = 0;
			c.gridy = 0;
			c.weightx = 1.0D;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.insets = new Insets(0, 20, 0, 20);
		}else c.gridy++;
		
		Chart chart = new Chart(getChartType(track), Color.BLUE, Color.GREEN, false, getUnit(track)){
			@Override
			public Dimension getPreferredSize(){
				if (getType() != Types.PIE) return new Dimension(1, 227);
				else return new Dimension(227, 227);
			}
		};
		
		if (chart.getType() == Types.LINE){
			if (track.getClass().getSimpleName().equals(Analytics.class.getSimpleName())) chart.getType().setUnit(TimeUnit.SECONDS);
		}
		
		charts.put(track, chart);
		graphs.add(charts.get(track), c);
	}
	
	private static Timer getUpdateTimer(){
		Timer update = new Timer(1, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				long currTime = System.currentTimeMillis();
				
				for (Trackable track : tracked.keySet()){
					if (currTime-tracked.get(track) >= getUpdateDelay(track)){
						int[] values = track.getValues();
						
						if (values.length > 0){
							Chart chart = charts.get(track);
							int max = (values[0] == 1) ? values[1] : -1;
							int[] vals = new int[((max < 0) ? values.length-1 : values.length-2)];
							
							for (int i = 0; i < vals.length; i++)
								vals[i] = values[(values.length-vals.length)+i];
//							chart.setMax(max);
							chart.updateData(vals, getOverrideData(track));
							charts.put(track, chart);
							tracked.put(track, currTime);
							Applications.ANALYTICS.getApp().getAppFrame().repaint();
//							NLP.out.println("Max: " + max + " | " + Arrays.toString(vals));
						}
					}
				}
			}
		});
		return update;
	}
	
	@Track(updateDelay = 1, chartType = Types.BAR, overrideData = true, unit = "Mb")
	@Override
	public int[] getValues(){
		Runtime runtime = Runtime.getRuntime();
		float mb = 1024*1024;
		
		return new int[]{1, (int) (((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize()/mb), (int) ((runtime.totalMemory()-runtime.freeMemory())/mb)};
	}
}
