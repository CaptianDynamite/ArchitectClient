package nlp.desktop.apps;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.ThreadFactory;

import nlp.NLP;
import nlp.util.Images;
import nlp.util.Images.IconSizes;

import javax.swing.*;

public enum Applications{
	
	CONSOLE(Console.class), LOGIN(Login.class, Images.LOGIN_APP), TEST(Test.class, Images.TEST_APP), ANALYTICS(Analytics.class, Images.ANALYTICS_APP), EMPTY(EmptyApplication.class, Images.EMPTY_APP);
	
	private static final int POINTS_PER_SIDE = 1;
	
	private Images icon;
	private Class<? extends Application> appCls;
	
	private Application app;
	private boolean hasTriedLoad;
	
	Applications(Class<? extends Application> appCls){
		this.appCls = appCls;
		this.icon = Images.DEFAULT_APP;
	}
	
	Applications(Class<? extends Application> appCls, Images icon){
		this.appCls = appCls;
		this.icon = icon;
	}
	
	public Application getApp(){
		if (!hasTriedLoad){
			try{
				boolean isInner = appCls.getEnclosingClass() != null;
				Constructor construct = (!isInner) ? appCls.getDeclaredConstructor() : appCls.getDeclaredConstructor(appCls.getEnclosingClass());
				boolean hasAccess = construct.isAccessible();
				
				if (!hasAccess) construct.setAccessible(true);
				app = (Application) ((!isInner) ? construct.newInstance() : construct.newInstance(this));
				construct.setAccessible(hasAccess);
				
				if (app instanceof Trackable) Analytics.registerTrackable((Trackable) app);
			}catch (Exception e){
				e.printStackTrace();
			}
			hasTriedLoad = true;
		}
		return app;
	}
	
	public Icon getIcon(){
		return icon.getIcon();
	}
	
	public Icon getIcon(IconSizes size){
		return icon.getIcon(size);
	}
	
	public String getIconName(){
		return name().toLowerCase() + "_icon";
	}
	
	public boolean isHiddenApp(){
		return (app.getClass().isAnnotationPresent(Hidden.class) && NLP.hideApps);
	}
	
//	private Application createClone(){
//		try{
//			return appCls.getConstructor(Icon.class).newInstance(icon);
//		}catch (Exception e){
//			e.printStackTrace();
//		}
//		return app;
//	}
	
	static Applications byClass(Application app){
		for (Applications value : values()){
			if (value.appCls == app.getClass()) return value;
		}
		return null;
	}
	
	public static Application byName(String name){
		for (Applications app : values()){
			if (app.name().equalsIgnoreCase(name)) return app.getApp();
		}
		return null;
	}

	public static Dimension getDefaultAppIconSize(){
	    return new Dimension(IconSizes.DEFAULT.getValue(), IconSizes.DEFAULT.getValue());
    }
    
    @Hidden
    private class EmptyApplication extends Application{
		
		@Override
		AppPanel createAppWindow(){
			return new AppPanel();
		}
	
		@Override
		public String getName(){
			return EMPTY.name().substring(0, 1) + EMPTY.name().substring(1).toLowerCase();
		}
	
		@Override
		public int thingsToLoad(){
			return 0;
		}
	}
}
