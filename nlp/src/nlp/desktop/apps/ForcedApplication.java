package nlp.desktop.apps;

import javax.swing.*;
import javax.swing.plaf.InternalFrameUI;
import javax.swing.plaf.basic.BasicInternalFrameUI;

public abstract class ForcedApplication extends Application{
	
	private boolean forced = true;
	private InternalFrameUI defaultUI;
	
	public ForcedApplication(){
		this(1, 1);
		maximize();
	}
	
	public ForcedApplication(int width, int height){
		defaultUI = getAppFrame().getUI();
		getAppFrame().setSize(width, height);
		setForced(forced);
	}
	
	public boolean isForced(){
		return forced;
	}
	
	public void maximize(){
		try{
			getAppFrame().setMaximum(true);
		}catch (Exception e){}
	}
	
	public void setForced(boolean forced){
		this.forced = forced;
		JInternalFrame appFrame = getAppFrame();
		
		appFrame.setUI((!forced) ? defaultUI : new ForcedUI(appFrame));
		appFrame.setResizable(!forced);
		appFrame.setClosable(!forced);
		try{
			if (appFrame.isMaximum()) appFrame.setMaximum(forced);
		}catch (Exception e){}
		if (!forced) appFrame.setSize(appFrame.getPreferredSize());
	}
	
	@Override
	abstract AppPanel createAppWindow();
	
	@Override
	public abstract int thingsToLoad();
	
	private class ForcedUI extends BasicInternalFrameUI{
		
		public ForcedUI(JInternalFrame frame){
			super(frame);
		}
		
		@Override
		protected JComponent createNorthPane(JInternalFrame frame) {
			return null;
		}
	}
}
