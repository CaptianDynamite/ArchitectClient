package nlp.desktop.apps;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import javax.swing.plaf.basic.BasicInternalFrameUI;

import nlp.NLP;
import nlp.desktop.Desktop;
import nlp.desktop.TaskBar;
import nlp.desktop.TaskBar.AppStatus;
import nlp.desktop.apps.comp.PreviewFrame;
import nlp.util.Images;
import nlp.util.Images.IconSizes;
import nlp.util.Loadable;
import nlp.util.Scalr;

public abstract class Application extends JButton implements Loadable{
	
	public static final String PAUSE = "Pause";
	public static final String RESUME = "Resume";
	
	
	private AppPanel appWindow;
	/**
	 * The JInternalFrame rendered to the desktop.<br><br>
	 *
	 * <u><b>IMPORTANT NOTES</u><b></b><br>
	 * - ONLY USE {@link JComponent#setVisible(boolean)} TO SHOW AND HIDE THE FRAME!!!<br>
	 * - ONLY RUN {@link TaskBar#toggleApp(Application)} AFTER SHOWING/HIDING THE FRAME!!!
	 */
	private JInternalFrame appFrame;
	private Dimension restoreSize;
	private boolean hasRestored;
	private boolean isPinned;
	
	public Map<Component, Boolean> pausedComps = new HashMap<Component, Boolean>();
	public JMenuItem pauseToggle;
	
	/**
	 * Constructs a new <code>Application</code>.
	 * 
	 * @see java.awt.GridBagLayout
	 * @see java.awt.GridBagConstraints
	 */
	public Application(){
		setText(getName());
		setForeground(Color.WHITE.darker());
		setBorder(BorderFactory.createEmptyBorder());
		setContentAreaFilled(false);
		setVerticalTextPosition(JButton.BOTTOM);
		setHorizontalTextPosition(JButton.CENTER);
		setMargin(new Insets(0, 0, 0, 0));
		setIcon(Applications.byClass(this).getIcon());
		hasRestored = true;
		
		appFrame = new JInternalFrame(Application.this.getName(), true, true, true, true){
			@Override
			public Dimension getPreferredSize(){
				if (!forced()) return new Dimension((int) (Desktop.getDesktop().getWidth()*NLP.ratio), (int) (Desktop.getDesktop().getHeight()*NLP.ratio));
				else return getSize();
			}
			
			@Override
			public Dimension getMaximumSize(){
				if (!forced()) return new Dimension(Desktop.getDesktop().getWidth(), (Desktop.getDesktop().getHeight()-((isMaximum()) ? TaskBar.getTaskBar().getHeight() : 0)));
				else return getSize();
			}
			
			@Override
			public void setVisible(boolean isVisible){
				super.setVisible(isVisible);
				if (isVisible) TaskBar.toggleApp(Application.this);
				
				//KEEP? Possibly temp to stop timer from running constantly.
				if (Application.this instanceof Analytics){
					if (isVisible) Analytics.updateTimer.start();
					else Analytics.updateTimer.stop();
				}
				//TODO Send trackable data to server?
//				if (Application.this instanceof Trackable){
//					if (isVisible){
//						if (!Analytics.updateTimer.isRunning()) Analytics.updateTimer.start();
//					}else{
//						boolean doStop = true;
//
//						for (Applications app : Applications.values()){
//							if (!doStop) break;
//							else if (app.getApp() != Application.this && app.getApp() instanceof Trackable && app.getApp().getAppFrame().isVisible()) doStop = false;
//						}
//						if (doStop) Analytics.updateTimer.stop();
//					}
//				}
			}
		};
		appFrame.addComponentListener(new ComponentAdapter(){
			@Override
			public void componentResized(ComponentEvent e){
//				System.out.println("isMaximum: " + appFrame.isMaximum());
//				System.out.println("size: " + appFrame.getSize());
				if (appFrame.isMaximum()){
					super.componentResized(e);
					appFrame.setSize(appFrame.getMaximumSize());
					hasRestored = false;
				}else{
					super.componentResized(e);
					
					if (!hasRestored){
						if (restoreSize != null) appFrame.setSize(restoreSize);
						hasRestored = true;
					}
					restoreSize = appFrame.getSize();
				}
				appFrame.repaint();
			}
		});
		appFrame.addInternalFrameListener(new InternalFrameAdapter(){
			@Override
			public void internalFrameClosing(InternalFrameEvent e){
				appFrame.setVisible(false);
				TaskBar.setStatus(Application.this, AppStatus.HIDDEN);
				TaskBar.toggleApp(Application.this);
				Desktop.getDesktop().selectFrame(true);
			}
		});
		appFrame.add(getAppWindow());
		appFrame.setSize(appFrame.getPreferredSize());
		appFrame.setLocation((int) ((Desktop.getDesktop().getWidth()/2.0F)-(appFrame.getWidth()/2.0F)), (int) ((Desktop.getDesktop().getHeight()/2.0F)-(appFrame.getHeight()/2.0F)));
		appFrame.setDefaultCloseOperation(JInternalFrame.DO_NOTHING_ON_CLOSE);
		appFrame.setUI(new AppUI());
		appFrame.setDoubleBuffered(true);
		appFrame.setFocusable(true);
		appFrame.setVisible(false);
		addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				if (SwingUtilities.isRightMouseButton(e)) TaskBar.getPinMenu(Application.this).show(e.getComponent(), e.getX(), e.getY());
				else if (SwingUtilities.isLeftMouseButton(e) && (!TaskBar.getTaskBar().isPinned(Application.this) || (!appFrame.isVisible() && isPinned)) && e.getClickCount() >= 2){
					appFrame.setVisible(true);
					TaskBar.toggleApp(Application.this);
					TaskBar.setStatus(Application.this, AppStatus.FOCUSED);
				}
			}
		});
	}
	
	abstract AppPanel createAppWindow();
	
	public JPanel getAppWindow(){
		return (appWindow != null) ? appWindow : (appWindow = createAppWindow());
	}
	
	public JInternalFrame getAppFrame(){
		return appFrame;
	}
	
	public Icon getIcon(IconSizes size){
		return Applications.byClass(this).getIcon(size);
	}
	
	public String getLoadingText(){
		return "Apps/" + getName();
	}
	
	public boolean isPinned(){
		return isPinned;
	}
	
	public void togglePin(){
		isPinned = !isPinned;
	}
	
	@Override
	public String getName(){
		return getClass().getSimpleName();
	}
	
	@Override
	public String getToolTipText(){
		return getName();
	}
	
	@Override
	public Dimension getMinimumSize(){
		return getPreferredSize();
	}
	
	@Override
	public Dimension getMaximumSize(){
		return getPreferredSize();
	}
	
	@Override
	public Dimension getSize(){
		return getPreferredSize();
	}
	
	private boolean forced(){
		return (this instanceof ForcedApplication && ((ForcedApplication) this).isForced());
	}
	
	private class AppTitlePane extends BasicInternalFrameTitlePane{
		
		private AppTitlePane(){
			super(appFrame);
		}
		
		@Override
		protected void addSystemMenuItems(JMenu systemMenu){
			super.addSystemMenuItems(systemMenu);
			
			systemMenu.removeAll();
			iconifyAction = new AbstractAction(){
				@Override
				public void actionPerformed(ActionEvent e){
					appFrame.setVisible(false);
					TaskBar.setStatus(Application.this, AppStatus.RUNNING);
					Desktop.getDesktop().selectFrame(true);
				}
			};
			pauseToggle = new JMenuItem(new AbstractAction(){
				@Override
				public void actionPerformed(ActionEvent e){
					TaskBar.togglePause(Application.this);
				}
			});
			
			systemMenu.add(restoreAction).setEnabled(false);
			systemMenu.add(maximizeAction).setText("Maximize");
			systemMenu.add(iconifyAction).setText("Minimize");
			systemMenu.add(pauseToggle).setText((TaskBar.getStatus(Application.this) != AppStatus.PAUSED) ? PAUSE : RESUME);
			systemMenu.addSeparator();
			systemMenu.add(closeAction).setText("Close");
		}
		
		@Override
		protected JMenuBar createSystemMenuBar(){
			SystemMenuBar menuBar = new SystemMenuBar(){
				@Override
				public void paint(Graphics g){
					Applications.byClass(Application.this).getIcon(Images.IconSizes.S_16).paintIcon(this, g, 0, 0);
				}
			};
			menuBar.setBorderPainted(false);
			return menuBar;
		}
	}
	
	private class AppUI extends BasicInternalFrameUI{
		
		private AppUI(){
			super(appFrame);
		}
		
		@Override
		protected JComponent createNorthPane(JInternalFrame frame){
			return new AppTitlePane();
		}
	}
	
	class AppPanel extends JPanel{
		
		private BufferedImage prevFrame;
		
		AppPanel(){
			addComponentListener(new ComponentAdapter(){
				@Override
				public void componentResized(ComponentEvent e){
					super.componentResized(e);
					if (TaskBar.getStatus(Application.this) == AppStatus.PAUSED)
						prevFrame = Scalr.resize(prevFrame, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, getWidth(), getHeight());
				}
			});
		}
		
		@Override
		public Component add(Component c){
			if (TaskBar.getStatus(Application.this) == AppStatus.PAUSED){
				pausedComps.put(c, c.isVisible());
				c.setVisible(false);
			}
			return super.add(c);
		}
		
		@Override
		public void paint(Graphics g){
			//KEEP? Used for live previews
//			for (JInternalFrame frame : Desktop.getDesktop().getAllFrames()){
//				if (frame instanceof PreviewFrame && frame.getTitle().equals(Application.this.getName())){
//					BufferedImage preview = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
//					Graphics2D g2d = preview.createGraphics();
//
//					super.paint(g2d);
//					((PreviewFrame) frame).paintPreview(preview);
//					break;
//				}
//			}
			if (TaskBar.getStatus(Application.this) != AppStatus.PAUSED){
				prevFrame = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
				super.paint(prevFrame.createGraphics());
				super.paint(g);
			}else{
				g.clearRect(0, 0, getWidth(), getHeight());
				g.drawImage(prevFrame, 0, 0, null);
			}
		}
	}
}
