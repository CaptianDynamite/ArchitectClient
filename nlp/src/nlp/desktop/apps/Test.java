package nlp.desktop.apps;

import nlp.desktop.apps.comp.CircularProgressBar;
import nlp.desktop.apps.comp.LogoProgressBar;
import nlp.util.Chart;
import nlp.util.Chart.Types;

import javax.swing.*;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

public class Test extends Application{

	private static final long serialVersionUID = 1L;
	
	Timer outer, inner;
	SecureRandom rand;
	static Demo demo = Demo.PROGRESS;
	int chart, font;
	
	@Override
	AppPanel createAppWindow(){
		rand = new SecureRandom();
		
		getAppFrame().addInternalFrameListener(new InternalFrameAdapter(){
			@Override
			public void internalFrameOpened(InternalFrameEvent e){
				super.internalFrameOpened(e);
				
				System.out.println("demo");
				switch(demo){
					case CHART:
						chartDemo();
						break;
					case PROGRESS:
						progressBarDemo();
						break;
				}
			}
			
			@Override
			public void internalFrameClosing(InternalFrameEvent e){
				super.internalFrameClosing(e);
				
				System.out.println("close demo");
				if (outer != null) outer.stop();
				if (inner != null) inner.stop();
				removeAll();
			}
		});
		AppPanel window = new AppPanel();
		window.setLayout(new GridBagLayout());
		window.setBackground(Color.RED);
		return window;
	}
	
	private void chartDemo(){
//		Chart chart = new Chart(Types.LINE(TimeUnit.HOURS), Color.BLUE, Color.GREEN, false);
		Chart chart = new Chart(Types.PIE, Color.BLUE, Color.GREEN, false);
		chart.setPreferredSize(new Dimension(500, 500));
		chart.setSize(chart.getPreferredSize());
//		chart.updateData(new int[]{20, 25, 35, 140, 50, 33, 78, 62, 32, 100}, true);
		chart.updateData(new int[]{50, 40}, true);
		getAppWindow().add(chart);
		
//		inner = new Timer(350, e ->{
//			int[] values = new int[chart.getWidth()/2];
////			int[] values = new int[100000]; /* Pie Chart */
//			for (int i = 0; i < values.length; i++) values[i] = rand.nextInt(values.length*10)+(values.length*2);
//			chart.updateData(values, true);
//		});
//		inner.restart();
//		inner.start();

//		this.chart = 0;
//		outer = new Timer(2000, e ->{
//			chart.setType(Types.byId(this.chart));
//			this.chart += ((this.chart+1) < Types.values().length) ? 1 : -this.chart;
//		});
//		outer.restart();
//		outer.start();
	}
	
	/**
	 * Shows a demo of the <code>CircularProgressBar</code>.
	 */
	private void progressBarDemo(){
		String[] fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		font = rand.nextInt(fonts.length);
//		System.out.println("fonts["+font+"]: " + fonts[font] + " | fonts.length: " + fonts.length);
//		CircularProgressBar bar = new CircularProgressBar(100, new Font(fonts[font], Font.PLAIN, 12));
		LogoProgressBar bar = new LogoProgressBar(256, 3000, new Font(fonts[font], Font.PLAIN, 12));
//		bar.setIndeterminate(true);
		getAppWindow().add(bar);
		
		outer = new Timer(100, e -> {
			if (bar.getValue() < 100) bar.setValue(bar.getValue()+1);
			else{
				font = rand.nextInt(fonts.length);
				bar.setFont(new Font(fonts[font], Font.PLAIN, 12));
				bar.setValue(0);
			}
//			NLP.out.println("value: " + bar.getValue());
		});
		
		bar.setValue(0);
		outer.restart();
		outer.start();
	}
	
	private enum Demo{
		CHART, PROGRESS;
	}
	
	@Override
	public String getLoadingText(){
		return super.getLoadingText() + "/Demo/" + demo.name().substring(0, 1) + demo.name().toLowerCase().substring(1, demo.name().length());
	}
	
	@Override
	public int thingsToLoad(){
		return 0;
	}
}
