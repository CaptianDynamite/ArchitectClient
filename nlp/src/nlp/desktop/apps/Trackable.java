package nlp.desktop.apps;

import nlp.util.Chart.Types;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public interface Trackable{
	/** An array of values, lead by a boolean which can predetermine a max value;
	 * in which case the following value is the max value, then followed by the data set. **/
	@Track
	int[] getValues();
	
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface Track{
		/** The type of chart being displayed.
		 * @see nlp.util.Chart.Types#DEFAULT **/
		Types chartType() default Types.PIE;
		
		/** Whether or not to override or append new data to the chart.
		 * @see nlp.desktop.apps.Analytics#DEFAULT_OVERRIDE_DATA **/
		boolean overrideData() default Analytics.DEFAULT_OVERRIDE_DATA;
		
		/** The delay in milliseconds to update the data.
		 * @see nlp.desktop.apps.Analytics#DEFAULT_UPDATE_DELAY **/
		int updateDelay() default Analytics.DEFAULT_UPDATE_DELAY;
		
		String unit() default "";
	}
}
