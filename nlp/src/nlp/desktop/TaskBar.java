package nlp.desktop;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import javax.swing.*;

import nlp.NLP;
import nlp.desktop.apps.Application;
import nlp.desktop.apps.Applications;
import nlp.desktop.apps.comp.MovableComponentsPanel;
import nlp.desktop.apps.comp.PreviewFrame;
import nlp.util.Images;

public class TaskBar extends MovableComponentsPanel{
	
	private static TaskBar tasks;
	
	private Color background;
	
	private Rectangle dragRect;
	private int prevClosest = -1;
	
	private TaskBar(){
		background = new Color(0, 0, 0, 220);
		setLayout(new FlowLayout(FlowLayout.LEADING, Desktop.iconGap, Desktop.iconGap));
	}
	
	public static TaskBar getTaskBar(){
		if (tasks == null) tasks = new TaskBar();
		return tasks;
	}
	
	public static void toggleApp(Application app){
		toggleApp(app, -1);
	}
	
	private static void toggleApp(Application app, int index){
		Desktop desktop = Desktop.getDesktop();
		AppPin pinned = getPinnedApp(app);

//		NLP.out.println("toggle");
		if (pinned != null){
			if (!app.getAppFrame().isVisible() && !app.isPinned()) tasks.remove(pinned);
		}else{
			if (index < 0 || index >= tasks.getComponentCount()) tasks.add(app);
			else tasks.add(app, index);
		}
		
		tasks.repaint();
		desktop.repaint();
		if (NLP.mainFrame.isVisible()) NLP.mainFrame.setVisible(true);
	}
	
	public static void togglePause(Application app){
		AppPin appPin = getPinnedApp(app);
		
		if (appPin != null){
			AppStatus prev = appPin.prevStatus;
			
//			NLP.out.println("prevStatus(P): " + appPin.prevStatus);
			appPin.prevStatus = appPin.status;
			appPin.status = (appPin.status == AppStatus.PAUSED) ? prev : AppStatus.PAUSED;
//			NLP.out.println(appPin.getName() + "(P): " + appPin.status + " | " + appPin.prevStatus);
			
			for (Component c : app.getAppWindow().getComponents()){
				if (appPin.status == AppStatus.PAUSED){
					app.pausedComps.put(c, c.isVisible());
					c.setVisible(false);
				}else{
					try{
						c.setVisible(app.pausedComps.get(c));
						app.pausedComps.remove(c);
					}catch (Exception e){}
				}
			}
			
			app.pauseToggle.setText((appPin.status == AppStatus.PAUSED) ? Application.RESUME : Application.PAUSE);
			tasks.repaint();
			Desktop.getDesktop().repaint();
		}else System.err.println("Error: '" + app.getName() + "' is not running!");
	}
	
	public static void setStatus(Application app, AppStatus status){
		AppPin appPin = getPinnedApp(app);
		
		if (appPin != null){
			if (status == AppStatus.PAUSED) togglePause(app);
			else if (appPin.status != AppStatus.PAUSED || status == AppStatus.HIDDEN){
//				NLP.out.println("prevStatus: " + appPin.prevStatus);
				appPin.prevStatus = appPin.status;
				appPin.status = status;
//				NLP.out.println(appPin.getName() + ": " + appPin.status + " | " + appPin.prevStatus);
				tasks.repaint();
			}
			if (status == AppStatus.FOCUSED && appPin.prevStatus == AppStatus.HIDDEN && app.pauseToggle.getText().equals(Application.RESUME)) togglePause(app);
		}
	}
	
	public static AppStatus getStatus(Application app){
		AppPin appPin = getPinnedApp(app);
		
		if (appPin != null) return appPin.status;
		return null;
	}
	
	public boolean isPinned(Application app){
		return getPinnedApp(app) != null;
	}
	
	public static JPopupMenu getPinMenu(Application app){
		JPopupMenu menu = new JPopupMenu(){
			private static final long serialVersionUID = 1L;
			
			@Override
			public void paintComponent(Graphics g){
				super.paintComponent(g);
				g.setColor(Desktop.background.darker());
				g.fillRect(0, 0, getWidth(), getHeight());
			}
			
			@Override
			public void addSeparator(){
				add(new JSeparator());
			}
		};
//					menu.setBorder(BorderFactory.createSoftBevelBorder(BevelBorder.RAISED, console.getBackground().darker(), console.getBackground().darker().darker()));
		menu.setBorder(BorderFactory.createEmptyBorder());
		menu.setOpaque(false);
		menu.setFont(Desktop.getDesktop().getFont());
		
		AppStatus status = getStatus(app);
		if (status != null && status != AppStatus.HIDDEN){
			JMenuItem pause = new JMenuItem(new AbstractAction(){
				@Override
				public void actionPerformed(ActionEvent e){
					togglePause(app);
				}
			});
			pause.setText((status != AppStatus.PAUSED) ? Application.PAUSE : Application.RESUME);
			menu.add(pause);
			menu.addSeparator();
		}
		
		JMenuItem pin = new JMenuItem(new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e){
				app.togglePin();
				toggleApp(app);
			}
		});
		pin.setText(((app.isPinned()) ? "Unpin from" : "Pin to") + " task bar");
		menu.add(pin);
		return menu;
	}
	
	private static AppPin getPinnedApp(Application app){
		for (Component c : tasks.getComponents()){
			if (c.getName().equals(app.getName()) && c instanceof AppPin) return (AppPin) c;
		}
		return null;
	}
	
	@Override
	protected void onCrossDrag(Component dragged, MouseEvent e){
		super.onCrossDrag(dragged, e);
		
		Application app = (Application) dragged;
		if (!isPinned(app)){
			Object[] closest = getClosestComp(e.getLocationOnScreen());
			int index = (int) closest[1];

			dragRect = getDragRect();
			if (tasks.getComponentCount() > 0 && dragRect.x > tasks.getComponent(tasks.getComponentCount()-1).getX()) index = -1;
			if (index != prevClosest){
				AppPin empty = getPinnedApp(Applications.EMPTY.getApp());
				
				if (empty != null) tasks.remove(empty);
				tasks.add(Applications.EMPTY.getApp(), index);
				NLP.mainFrame.setVisible(true);
				prevClosest = index;
			}
		}
	}
	
	@Override
	protected void onCrossDrop(Component dropped, MouseEvent e){
		super.onCrossDrop(dropped, e);
		
		Application app = (Application) dropped;
		AppPin empty = getPinnedApp(Applications.EMPTY.getApp());
		if (!isPinned(app)){
			int index = -1;
			
			if (empty != null){
				for (int i = 0; i < tasks.getComponentCount(); i++)
					if (tasks.getComponent(i).equals(empty)) index = i;
				tasks.remove(index);
			}
			app.togglePin();
			toggleApp(app, index);
		}
	}
	
	@Override
	public Component add(Component c){
		return add(c, -1);
	}

	@Override
	public Component add(Component comp, int index){
		if (comp instanceof Application) return super.add(new AppPin(((Application) comp)), index);
		else if (comp instanceof AppPin) return super.add(comp, index);

		return null;
	}
	
	@Override
	public void paintComponent(Graphics g){
		g.setColor(background);
		g.fillRect(0, 0, getWidth(), getHeight());
	}
	
	@Override
	public Dimension getPreferredSize(){
		Dimension size = NLP.mainFrame.getSize();
		Insets ins = NLP.mainFrame.getInsets();
		
		return new Dimension(size.width-(ins.left+ins.right), Applications.getDefaultAppIconSize().height+(Desktop.iconGap*2));
	}
	
	@Override
	public Dimension getMinimumSize(){
		return getPreferredSize();
	}
	
	private class AppPin extends JButton{
		
		private AppStatus status = AppStatus.HIDDEN;
		private AppStatus prevStatus = AppStatus.HIDDEN;
		
		AppPin(Application app){
			super(app.getIcon());
			
			setName(app.getName());
			setToolTipText(getName());
			setDisabledIcon(getIcon());
			setEnabled(false);
			setBorderPainted(false);
			setContentAreaFilled(false);
			setMargin(new Insets(0, 0, 0, 0));
			addMouseListener(new MouseAdapter(){
				@Override
				public void mouseClicked(MouseEvent e){
					if (SwingUtilities.isLeftMouseButton(e)){
						JInternalFrame appFrame = app.getAppFrame();
						
						if (!appFrame.isVisible()){
							setStatus(app, AppStatus.FOCUSED);
							appFrame.setVisible(true);
						}else{
							if (status == AppStatus.RUNNING){
								try{
									appFrame.setSelected(true);
								}catch (Exception ex){}
							}else{
								setStatus(app, AppStatus.RUNNING);
								appFrame.setVisible(false);
								Desktop.getDesktop().selectFrame(true);
							}
						}
						for (JInternalFrame frame : Desktop.getDesktop().getAllFrames()){
							if (frame instanceof PreviewFrame && frame.getTitle().equals(appFrame.getTitle())) frame.dispose();
						}
					}else getPinMenu(app).show(e.getComponent(), e.getX(), e.getY());
				}
				
				@Override
				public void mouseDragged(MouseEvent e){
					super.mouseDragged(e);
				}
				
				@Override
				public void mouseExited(MouseEvent e){
					super.mouseExited(e);
					
					for (JInternalFrame frame : Desktop.getDesktop().getAllFrames()){
						if (frame instanceof PreviewFrame){
							Point loc = e.getLocationOnScreen();
							
							SwingUtilities.convertPointFromScreen(loc, Desktop.getDesktop());
//							System.out.println("bounds: " + frame.getBounds() + " | loc: " + loc);
							if (!frame.getBounds().contains(loc)){
								frame.dispose();
							}
						}
					}
				}
			});
		}
		
		@Override
		public JToolTip createToolTip(){
			JToolTip tip = super.createToolTip();
			
			if (status != null && status == AppStatus.RUNNING){
				PreviewFrame preview = new PreviewFrame(this);
				Timer previewDelay = new Timer(PreviewFrame.PREVIEW_DELAY, new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e){
						JPanel appWindow = Applications.byName(tip.getTipText()).getAppWindow();
						BufferedImage img = new BufferedImage(appWindow.getWidth(), appWindow.getHeight(), BufferedImage.TYPE_INT_ARGB);
						Graphics2D g2d = img.createGraphics();
						
						appWindow.paint(g2d);
						preview.paintPreview(img);
						preview.setVisible(true);
					}
				});
				previewDelay.setRepeats(false);
				
				Desktop.getDesktop().add(preview);
				previewDelay.start();
			}
			return tip;
		}
		
		@Override
		public void paint(Graphics g){
			super.paint(g);
			
			if (status != AppStatus.PAUSED){
				int height = (int) (getHeight()*status.getRatio());
				if (height > 0 && !getName().equals(Applications.EMPTY.getApp().getName())){
					g.setColor(new Color(85, 85, 255));
					g.fillRect(0, getHeight()-height, getWidth(), height);
				}
			}else{
				BufferedImage paused = (BufferedImage) Images.PAUSE_APP.getIcon().getImage();
				g.drawImage(paused, (int) ((getWidth()/2.0F)-(paused.getWidth()/2.0F)), (int) ((getHeight()/2.0F)-(paused.getHeight()/2.0F)), null);
			}
		}
	}
	
	public enum AppStatus{
		HIDDEN, PAUSED(0.0F), RUNNING, FOCUSED;
		
		AppStatus(){}
		
		AppStatus(float ratio){
			this.ratio = ratio;
		}
		
		private float ratio = -1;
		private final float MAX = 0.1F;
		
		private float getRatio(){
			if (ratio < 0){
				for (int i = 0; i < values().length; i++){
					if (AppStatus.this == values()[i])
						ratio = ((i * 1.0F) / (values().length - 1)) * MAX;
				}
			}
			return ratio;
		}
	}
}
