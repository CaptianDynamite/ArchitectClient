package nlp.panel;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

import nlp.util.Loadable;

/**
 * Represents a panel, which is a graphical user interface that the user can interact with.
 */
public abstract class Panel extends JPanel implements Loadable{
	
	private static final long serialVersionUID = 1L;
	
	public GridBagConstraints c;
	
	/**
	 * Constructs a new <code>Panel</code>.
	 * 
	 * @see javax.swing.JPanel
	 * @see java.awt.GridBagLayout
	 * @see java.awt.GridBagConstraints
	 */
	Panel(){
		setLayout(new GridBagLayout());
		c = new GridBagConstraints();
	}
	
	/**
	 * Adds the given component to the panel with the panel's constraints.
	 */
	@Override
	public Component add(Component comp){
		super.add(comp, c);
		return comp;
	}
	
	/**
	 * Returns the preferred size of the panel.
	 */
	@Override
	public abstract Dimension getPreferredSize();
}
