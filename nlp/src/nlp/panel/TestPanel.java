package nlp.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.swing.Timer;










import nlp.NLP;
import nlp.desktop.apps.comp.CircularProgressBar;
import nlp.util.Chart;
import nlp.util.LoadingPanel;
import nlp.util.Chart.Types;

/**
 * A <code>Panel</code> that is used for testing displayable components.
 */
public class TestPanel extends Panel{

	private static final long serialVersionUID = 1L;

	Timer outer, inner;
	SecureRandom rand;
	Demo demo = Demo.CHART;
	int chart, font;
	
	/**
	 * Constructs a new <code>TestPanel</code>.
	 * 
	 * @see nlp.panel.Panel
	 */
	public TestPanel(){
		setBackground(Color.RED);
		LoadingPanel.nowLoading(this, "Panels/"+Panels.TEST.name().toLowerCase()+"/Demo/"+demo.name().toLowerCase());
	}
	
	private void chartDemo(){
		Chart chart = new Chart(Types.LINE(TimeUnit.HOURS), Color.BLUE, Color.GREEN, false);
		chart.setPreferredSize(new Dimension(getHeight(), getHeight()));
		chart.updateData(new int[]{20, 25, 35, 140, 50, 33, 78, 62, 32, 100}, true);
//		chart.updateData(new int[]{50, 40, 50}, true);
		add(chart);
		
		inner = new Timer(5000, e ->{
			int[] values = new int[chart.getWidth()/2];
//			int[] values = new int[100000]; /* Pie Chart */
			for (int i = 0; i < values.length; i++) values[i] = rand.nextInt(values.length*10)+(values.length*2);
			chart.updateData(values, true);
		});
		inner.setRepeats(true);
		inner.restart();
		inner.start();
//		
//		this.chart = 0;
//		outer = new Timer(2000, e ->{
//			chart.setType(Types.byId(this.chart));
//			this.chart += ((this.chart+1) < Types.values().length) ? 1 : -this.chart;
//		});
//		outer.setRepeats(true);
//		outer.restart();
//		outer.start();
	}
	
	/**
	 * Shows a demo of the <code>CircularProgressBar</code>.
	 */
	private void progressBarDemo(){
		String[] fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		font = new Random().nextInt(fonts.length);
//		System.out.println("fonts["+font+"]: " + fonts[font] + " | fonts.length: " + fonts.length);
		CircularProgressBar bar = new CircularProgressBar(100, new Font(fonts[font], Font.PLAIN, 12));	
		add(bar);
		
		outer = new Timer(10, e -> {
		      if (bar.getValue() < 100) bar.setValue(bar.getValue()+1);
		      else{
		    	  font = new Random().nextInt(fonts.length);
		    	  bar.setFont(new Font(fonts[font], Font.PLAIN, 12));
		    	  bar.setValue(0);
		      }
		});
		outer.setRepeats(true);
		
		bar.setValue(0);
		outer.restart();
		outer.start();
	}
	
	@Override
	public Dimension getPreferredSize(){
		System.out.println("Test: " + new Dimension((int)(NLP.mainFrame.getWidth()*(1.0F/2F)), (int)(NLP.mainFrame.getHeight()*(1.0F/2F))) + " | mainFrame: " + NLP.mainFrame.getSize());
		return new Dimension((int)(NLP.mainFrame.getWidth()*(1.0F/2F)), (int)(NLP.mainFrame.getHeight()*(1.0F/2F)));
	}
	
	/**
	 * Sets the visibility of the <code>TestPanel</code>.<br>
	 * Starts/stops the progress bar demo.
	 */
	@Override
	public void setVisible(boolean isVisible){
		super.setVisible(isVisible);
		
		if (isVisible){
			rand = new SecureRandom();
			switch(demo){
				case CHART:
					chartDemo();
					break;
				case PROGRESS:
					progressBarDemo();
					break;
			}
		}else{
			if (outer != null) outer.stop();
			if (inner != null) inner.stop();
			removeAll();
		}
	}
	
	private enum Demo{
		CHART, PROGRESS;
	}

	@Override
	public int thingsToLoad(){
		return 0;
	}
}
