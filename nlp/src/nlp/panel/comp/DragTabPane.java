package nlp.panel.comp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JTabbedPane;
import javax.swing.plaf.metal.MetalTabbedPaneUI;

import nlp.NLP;
import nlp.panel.Panels;

/**
 * A <code>JTabbedPane</code> that allows for the moving of the tabs.
 */
public class DragTabPane extends JTabbedPane{

	private static final long serialVersionUID = 1L;

//	private static final Color tabBackground = new Color(85, 85, 255);
//	private static final Color tabHighLight = tabBackground.brighter().brighter();
//	private static final Color tabLightHighLight = tabHighLight.brighter().brighter();
//	private static final Color tabShadow = tabBackground.darker().darker();
//	private static final Color tabDarkShadow = tabShadow.darker().darker();
	
	private boolean isMovingTabs;
	private int origTabIndex;
	
	/**
	 * Constructs a new <code>DragTabPane</code>.<br>
	 * Uses the top tab placement and scroll tab layout by default.
	 */
	public DragTabPane(){
		setTabPlacement(JTabbedPane.TOP);
		setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		
//		setBackground(tabBackground);
		setUI(new MetalTabbedPaneUI(){
			@Override
			protected void installDefaults(){
				super.installDefaults();
//				tabAreaBackground = tabBackground;
//				highlight = tabHighLight;
//				lightHighlight = tabLightHighLight;
//				shadow = tabShadow;
//				darkShadow = tabDarkShadow;
//				focus = tabAreaBackground;
//				selectColor = tabAreaBackground;
//				selectHighlight = highlight;
			}
			
			@Override
			protected void paintTabArea(Graphics g, int tabPlacement, int selectedIndex){
				g.setColor(new Color(52, 57, 61));
				g.fillRect(0, 0, NLP.mainFrame.getWidth(), maxTabHeight+getContentBorderInsets(tabPlacement).top);
				super.paintTabArea(g, tabPlacement, selectedIndex);
			}
		});
		
		addMouseWheelListener(new MouseWheelListener(){
			@Override
			public void mouseWheelMoved(MouseWheelEvent e){
				int newIndex = (e.getPreciseWheelRotation() < 0) ? getSelectedIndex()-1 : getSelectedIndex()+1;
				setSelectedIndex((newIndex < getTabCount()) ? ((newIndex >= 0) ? newIndex : 0) : getTabCount()-1);
			}
		});
		
		addMouseListener(new MouseListener(){
			@Override
			public void mouseReleased(MouseEvent e){
				origTabIndex = -1;
				isMovingTabs = false;
			}
			@Override
			public void mousePressed(MouseEvent e){
				origTabIndex = indexAtLocation(e.getXOnScreen()-getLocationOnScreen().x, e.getYOnScreen()-getLocationOnScreen().y);
			}
			@Override
			public void mouseClicked(MouseEvent e){}
			@Override
			public void mouseExited(MouseEvent e){}
			@Override
			public void mouseEntered(MouseEvent e){}
		});
		
		addMouseMotionListener(new MouseMotionListener(){
			@Override
			public void mouseDragged(MouseEvent e){
				if (isVisible() && !isMovingTabs){
					int tabIndex = indexAtLocation(e.getXOnScreen()-getLocationOnScreen().x, e.getYOnScreen()-getLocationOnScreen().y);
					
					if (tabIndex != -1 && origTabIndex != -1 && origTabIndex != tabIndex){
						String title = getTitleAt(origTabIndex), otherTitle = getTitleAt(tabIndex);
						
						isMovingTabs = true;
						if (tabIndex > origTabIndex){
							insertTab(otherTitle, null, Panels.valueOf(otherTitle.toUpperCase()).get(), null, origTabIndex);
							setTabComponentAt(origTabIndex, new TabComponent(getTabPlacement(), otherTitle));
							
							removeTabAt(origTabIndex+1);
							insertTab(title, null, Panels.valueOf(title.toUpperCase()).get(), null, tabIndex);
							setTabComponentAt(tabIndex, new TabComponent(getTabPlacement(), title));
						}else{
							insertTab(title, null, Panels.valueOf(title.toUpperCase()).get(), null, tabIndex);
							setTabComponentAt(tabIndex, new TabComponent(getTabPlacement(), title));
							
							removeTabAt(tabIndex+1);
							insertTab(otherTitle, null, Panels.valueOf(otherTitle.toUpperCase()).get(), null, origTabIndex);
							setTabComponentAt(origTabIndex, new TabComponent(getTabPlacement(), otherTitle));
						}
						setSelectedIndex(tabIndex);
						origTabIndex = tabIndex;
						isMovingTabs = false;
					}
				}
			}
			@Override
			public void mouseMoved(MouseEvent e){}
		});
	}
	
	public void paint(Graphics g){
		if (!isMovingTabs) super.paint(g);
	}
	
	/**
	 * Returns whether or not the tabs are being moved.
	 * 
	 * @return whether or not the user is currently moving the tabs.
	 */
	public boolean isMovingTabs(){
		return isMovingTabs;
	}
}
