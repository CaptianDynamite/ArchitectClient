package nlp.panel.comp;

import java.util.Map;
import java.util.HashMap;

public class MessageHistory{
	
	static Long prevMessage;
	static Map<Long, Message> messages = new HashMap<Long, Message>();
	
	public static Message getMessage(long id){
		return messages.get(id);
	}
	
	public static Message getLastMessage(){
		return getMessage(prevMessage);
	}
	
	public static void addMessage(Message msg){
		prevMessage = msg.getId();
		messages.put(prevMessage, msg);
	}
	
	public static void removeMessage(long id){
		messages.remove(id);
		prevMessage = (messages.size() > 0) ? messages.keySet().toArray(new Long[messages.size()])[messages.size()-1] : -1;
	}
}
