package nlp.panel.comp;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

import nlp.NLP;
import nlp.util.Loadable;
import nlp.util.LoadingPanel;

public enum TextComponents{
	
	LOADING_PANEL_LOADING(13.0F),
	CONSOLE_PANEL_CONSOLE(11.0F),
	LOGIN_PANEL_LABEL(13.0F), LOGIN_PANEL_LOGIN(13.0F),
	
	TAB_COMPONENT_LABEL(11.0F);
	
//	private static final Dimension MINIMUM_SUPPORTED_SIZE = new Dimension(3840, 2160); /* 4K */
	private static final Dimension MINIMUM_SUPPORTED_SIZE = new Dimension(1920, 1080);
//	private static final Dimension MINIMUM_SUPPORTED_SIZE = new Dimension(1280, 800); /* Laptop */
	
	private float defaultFontSize;
	
	private TextComponents(float defaultFontSize){
		this.defaultFontSize = defaultFontSize;
	}
	
	public Font getFixedFont(JComponent comp){
		return getFixedFont(null, comp);
	}
	
	public Font getFixedFont(Loadable load, JComponent comp){
		return getFixedFont(load, comp.getFont().deriveFont(defaultFontSize));
	}
	
	public Font getFixedFont(Font font){
		return getFixedFont(null, font);
	}
	
	public Font getFixedFont(Loadable load, Font font){
		if (load != null && NLP.loading.isVisible()) LoadingPanel.nowLoading(load, "TextComponents/"+name().toLowerCase());
		
		Graphics2D g = new BufferedImage(NLP.getScreen().width, NLP.getScreen().height, BufferedImage.TYPE_INT_ARGB).createGraphics();
		int defaultHeight = g.getFontMetrics(font).getHeight(), heightToReach = ((NLP.getScreen().width*NLP.getScreen().height) > (MINIMUM_SUPPORTED_SIZE.width*MINIMUM_SUPPORTED_SIZE.height)) ?
				(int) (((NLP.getScreen().height*NLP.ratio)/(MINIMUM_SUPPORTED_SIZE.height*NLP.ratio))*defaultHeight) : (int) (((MINIMUM_SUPPORTED_SIZE.height*NLP.ratio)/(NLP.getScreen().height*NLP.ratio))*defaultHeight);
				
		if (heightToReach < defaultHeight){
			defaultHeight = heightToReach;
			heightToReach = g.getFontMetrics(font).getHeight();
		}
//		System.out.println(name() + ": " + defaultHeight + " | " + heightToReach);
		
		while (defaultHeight < heightToReach){
			font = font.deriveFont((float) font.getSize()+1);
			defaultHeight = g.getFontMetrics(font).getHeight();
		}
		return font;
	}
}
