package nlp.panel.comp;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import nlp.NLP;
import nlp.panel.Panels;
import nlp.util.Loadable;

public class TabComponent extends JPanel implements Loadable{

	private static final long serialVersionUID = 1L;

	private static final boolean HAS_BUTTON_DEFAULT = true;
	
	public TabComponent(int tabPlacement, String title){
		this(tabPlacement, title, null, HAS_BUTTON_DEFAULT);
	}
	
	public TabComponent(int tabPlacement, String title, Icon icon){
		this(tabPlacement, title, icon, HAS_BUTTON_DEFAULT);
	}
	
	public TabComponent(int tabPlacement, String title, Icon icon, boolean hasButton){
		setOpaque(false);
//		setBackground(NLP.tabs.getBackground());
//		setFocusable(false);
		
		JLabel label = new JLabel(title, icon, JLabel.TRAILING);
		label.setFont(TextComponents.TAB_COMPONENT_LABEL.getFixedFont(this, label));
		add(label);
		
		if (hasButton){
			JButton close = new JButton("X");
			close.setFont(label.getFont());
			close.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e){
//					if (NLP.tabs.getTabCount() > 1 && !NLP.tabs.isMovingTabs()) NLP.tabs.remove(NLP.tabs.indexOfTab(title));
//					if (NLP.tabs.getTabCount() == 1 && close.isVisible()){
//						for (Component c : ((JPanel) NLP.tabs.getTabComponentAt(0)).getComponents()){
//							if (c instanceof JButton && ((JButton) c).getText().equals("X")) c.setVisible(false);
//						}
//					}
				}
			});
			close.setContentAreaFilled(false);
            close.setFocusable(false);
            close.setBorder(BorderFactory.createEmptyBorder());
            close.setRolloverEnabled(true);
			add(close);
		}
	}
	
	@Override
	public void paintComponent(Graphics g){
//		super.paintComponent(g);
		
		Graphics2D g2d = (Graphics2D) g;
		
//		g2d.rotate(Math.toRadians((tabPlacement == JTabbedPane.LEFT) ? 270 : ((tabPlacement == JTabbedPane.RIGHT) ? 90 : 0)));
	}
	
	@Override
	public void paint(Graphics g){
		super.paint(g);
//		try{
//			BufferedImage img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
//			Graphics2D gImg = img.createGraphics();
//			super.paint(gImg);
//			ImageIO.write(img, "png", new File(FileSystemView.getFileSystemView().getHomeDirectory(), "test.png"));
//		}catch (Exception e){
//			e.printStackTrace();
//		}
	}

	/**
	 * Panel label text components.
	 * 
	 * @see nlp.panel.comp.TextComponents#TAB_COMPONENT_LABEL
	 */
	@Override
	public int thingsToLoad(){
		return Panels.values().length-1;
	}
}
