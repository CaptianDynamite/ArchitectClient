package nlp.panel.comp;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

import nlp.panel.ConsolePanel;

public class MessageBuilder{

	private static MessageBuilder builder;
	
	private String text;
	private Message msg;
	
	private MessageBuilder(boolean isEditable){
//		messages = new ArrayList<Message>();
		msg = new Message(isEditable);
		text = "";
//		NLP.out.println("Main msg: " + msg);
	}
	
	public static MessageBuilder createNewBuilder(){
		return createNewBuilder(false);
	}
	
	public static MessageBuilder createNewBuilder(boolean isEditable){
		return (builder = new MessageBuilder(isEditable));
	}
	
	private MessageBuilder addPiece(Message piece){
//		NLP.out.println("text(piece): '" + text + "' | " + piece);
		if (msg.prevPiece != null || msg.length > 0){
			piece.prevPiece = msg;
			msg = piece;
			msg.offset = msg.prevPiece.offset+msg.prevPiece.length;
		}else{
			msg.length = piece.length;
			msg.popup = piece.popup;
		}
//		NLP.out.println("text(msg): '" + text + "' | " + msg);
		return builder;
	}
	
	public MessageBuilder add(String msg){
		text += msg;
		return addPiece(new Message(msg));
	}
	
//	public MessageBuilder add(Icon icon){
//		this.msg.prevPiece = new Message(icon);
//		this.msg = this.msg.prevPiece;
//		return builder;
//	}
	
	/**
	 * Returns the builder object with an added JComponent.<p>
	 * 
	 * Handles <code>Message</code> objects, adding them directly instead of into another <code>Message</code>.
	 * 
	 * @param comp The component to be displayed.
	 * @return the builder object  with an added JComponent
	 */
//	public MessageBuilder add(JComponent comp){
//		return builder;
//	}
	
	public MessageBuilder add(String msg, JComponent popup){
		text += msg;
		return addPiece(new Message(msg, popup));
	}
	
	public MessageBuilder add(Message msg){
		return addPiece(msg);
	}
	
	public Object[] build(){
		return new Object[]{msg, text};
	}
	
	public static class UnbuiltMessageException extends Exception{

		private static final long serialVersionUID = 1L;
		
		public UnbuiltMessageException(){
			super();
			setStackTrace(getStackTrace());
		}
		
		@Override
		public String getMessage(){
			return "Error: Message not built!";
		}
		
		@Override
		public StackTraceElement[] getStackTrace(){
			List<StackTraceElement> elements = new ArrayList<StackTraceElement>();
			
			for (StackTraceElement element : super.getStackTrace()){
				if (!element.getClassName().startsWith(ConsolePanel.class.getName()) && element.getClassName().startsWith("nlp")) elements.add(element);
			}
			return elements.toArray(new StackTraceElement[elements.size()]);
		}
	}
}
