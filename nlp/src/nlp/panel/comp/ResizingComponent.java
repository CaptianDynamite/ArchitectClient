package nlp.panel.comp;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

import nlp.NLP;

/**
 * A <code>JPanel</code> that contains a <code>JComponent</code> used to properly resize the component.
 */
public class ResizingComponent extends JPanel{

	private static final long serialVersionUID = 1L;
	private JComponent component;

	/**
	 * Constructs a new <code>ResizingComponent</code>.
	 * 
	 * @param component The component that will be resized
	 */
	public ResizingComponent(JComponent component){
		this.component = component;
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.PAGE_START;
		
		setOpaque(false);
		setLayout(new GridBagLayout());
		setMinimumSize(component.getMinimumSize());
		setPreferredSize(component.getPreferredSize());
		add(component, c);
	}

	/**
	 * Returns a <code>Dimension</code> based on whether or not the window is maximized.<br>
	 * Multiples the component dimensions by the window ratio to getBool the proper size when the window is maximized.
	 */
	@Override
	public Dimension getPreferredSize(){
		return (NLP.mainFrame.getExtendedState() != JFrame.MAXIMIZED_BOTH) ? component.getPreferredSize() : 
				new Dimension((int) (component.getPreferredSize().width*(NLP.getScreen().width/(NLP.getScreen().width*(NLP.ratio)))),
				(int) (component.getPreferredSize().height*(NLP.getScreen().width/(NLP.getScreen().width*(NLP.ratio)))));
	}
	
	public JComponent getOriginalComponent(){
		return component;
	}
	
	/**
	 * Sets the font of this component and the actual component.
	 */
	@Override
	public void setFont(Font font){
		if (component != null) component.setFont(font);
		super.setFont(font);
	}
}
