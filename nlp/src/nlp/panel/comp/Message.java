package nlp.panel.comp;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.UUID;

import javax.swing.JComponent;
import javax.swing.JToolTip;
import javax.swing.plaf.ToolTipUI;
import javax.swing.text.Element;

import nlp.NLP;
import nlp.panel.ConsolePanel;


public class Message{
	
//	public final String POPUP_TEXT = "heyo! ;P";

	private final long id;
	public final boolean isMainMsg;
	public final boolean isEditable;
	
	int length;
	int offset;
	Message prevPiece;
	Tip popup;
	
	/**
	 * Creates an empty message.
	 */
	Message(boolean isEditable){
		isMainMsg = true;
		this.isEditable = isEditable;
		id = create();
	}
	
	/**
	 * Creates a message with text.
	 * 
	 * @param msg The message to be displayed
	 */
	Message(String msg){
		isMainMsg = false;
		isEditable = getMainMessage().isEditable;
		length = msg.length();
		id = create();
	}
	
//	/**
//	 * Creates a message with an image.
//	 * 
//	 * @param icon The image to be displayed
//	 * 
//	 * @see javax.swing.ImageIcon#ImageIcon(java.awt.Image)
//	 */
//	Message(Icon icon){
//		isMainMsg = false;
//		id = create();
//	}
	
	/**
	 * Creates a message with a popup.
	 * 
	 * @param msg The message to be displayed
	 * @param popup The component to be shown upon hovering over the displayed message
	 */
	Message(String msg, JComponent popup){
		isMainMsg = false;
		isEditable = getMainMessage().isEditable;
		length = msg.length();
		this.popup = new Tip(popup);
		id = create();
	}
	
	public String getText(ConsolePanel console){
		String text = null;
		
		try{
			Element msg = getElement(console);
//			NLP.out.println("msg: " + msg.getName() + " | " + msg.getStartOffset() + " | " + msg.getEndOffset());
//			NLP.out.println("console.doc.getLength(): " + console.doc.getLength());
			text = console.doc.getText(msg.getStartOffset(), (msg.getEndOffset()-msg.getStartOffset()));
		}catch (Exception e){
			e.printStackTrace(NLP.err);
		}
		return text;
	}
	
	private Message getMainMessage(){
		Message prev = prevPiece;
		
		if (prev != null){
			while (prev.prevPiece != null) prev = prev.prevPiece;
			return prev;
		}else return this;
	}
	
	public Message getPrevious(){
		return prevPiece;
	}
	
	public JToolTip getPopup(){
		return popup;
	}
	
	public Element getElement(ConsolePanel console){
		try{
			return console.doc.getElement(String.valueOf(getId()));
		}catch (Exception e){
			e.printStackTrace(NLP.err);
		}
		return null;
	}
	
	public int getOffset(){
		return offset;
	}
	
	public int length(){
		return length;
	}
	
	private long create(){
		return (isMainMsg) ? (UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE) : -1;
	}
	
	public long getId(){
		return getMainMessage().id;
	}
	
	private static class Tip extends JToolTip{
		
		private static final long serialVersionUID = 1L;

		Tip(JComponent comp){
			if (comp != null){
				setPreferredSize(comp.getPreferredSize());
				setLayout(new BorderLayout());
				setOpaque(false);
				
				comp.setOpaque(false);
				add(comp);
				
				setUI(new ToolTipUI(){
					@Override
					public Dimension getMinimumSize(JComponent c){
						return c.getLayout().minimumLayoutSize(c);
					}
					@Override
					public Dimension getPreferredSize(JComponent c){
						return c.getLayout().preferredLayoutSize(c);
					}
					@Override
					public Dimension getMaximumSize(JComponent c){
						return getPreferredSize(c);
					}
				});
			}
		}
	}
	
	@Override
	public String toString(){
		return "{Message: {"+id+": "+isEditable+","+isMainMsg+","+length+","+offset+","+prevPiece+","+popup+"}}";
	}
}
