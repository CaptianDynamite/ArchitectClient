package nlp.panel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import nlp.NLP;

/**
 * A static list of <code>Panel</code> objects which handles various aspects of the panels.
 */
public enum Panels{

	/** A <code>Panel</code> that displays information about the program during runtime, and has the ability to execute commands. */
	CONSOLE(ConsolePanel.class), 
	/** A <code>Panel</code> that displays components that are useful to a user trying to login to the server. */
	LOGIN(LoginPanel.class),
	/** A <code>Panel</code> that is used for testing displayable components. */
	TEST(TestPanel.class),
	
	/** Any invalid panel. */
	INVALID(null);
	
	private static Map<String, JPanel> panels = new HashMap<String, JPanel>();
	
	private Class<? extends Panel> panel;
	
	/**
	 * Registers new panels.
	 * 
	 * @param panel The <code>Panel</code> class
	 * 
	 * @see nlp.panel.Panel
	 */
	Panels(Class<? extends Panel> panel){
		this.panel = panel;
	}
	
	/**
	 * Returns a <code>JPanel</code> version of the <code>Panels</code> object.
	 * 
	 * @return a <code>JPanel</code> object
	 */
	public JPanel get(){
		return panels.get(panel.getSimpleName());
	}
	
	public Class<? extends Panel> getPanelClass(){
		return panel;
	}
	
	/**
	 * Returns the name of the <code>Panels</code> object.
	 * 
	 * @return the class simple name of the <code>Panels</code> object
	 * @see java.lang.Class#getSimpleName()
	 */
	private String getName(){
		return panel.getSimpleName();
	}
	
	/**
	 * Returns the panel shown on startup.
	 * 
	 * @return the initial <code>Panels</code> object shown on startup
	 */
	public static Panels getInitial(){
		return CONSOLE;
	}
	
	/**
	 * Return the panel currently showing.
	 * 
	 * @return the <code>Panels</code> object currently being shown
	 */
	public static Panels getCurrent(){
//		return valueOf(NLP.tabs.getTitleAt(NLP.tabs.getSelectedIndex()).toUpperCase());
		return null;
	}
	
	/**
	 * Returns all the panels as <code>JPanel</code> objects.
	 * 
	 * @return all of the <code>Panels</code> values as <code>JPanel</code> objects
	 */
	public static List<JPanel> getPanels(){
		return new ArrayList<JPanel>(Arrays.asList(panels.values().toArray(new JPanel[panels.size()])));
	}
	
	/**
	 * Returns a <code>Panels</code> version of the given <code>Panel</code> class.
	 * 
	 * @param cls The <code>Panel</code> class
	 * @return a <code>Panels</code> object based on the given <code>Panel</code> class
	 */
	public static Panels byClass(Class<? extends Panel> cls){
		if (cls != null){
			for (Panels value : values()){
	//			System.out.println("Panel: '" + value.getName() + "'.equals('" + cls.getSimpleName() + "')");
				if (value != INVALID && value.getName().equals(cls.getSimpleName())) return value;
			}
		}
		return INVALID;
	}
	
	/**
	 * Sets the currently shown panel.
	 * 
	 * @param panel The <code>Panels</code> object to be displayed
	 * @throws IllegalArgumentException Thrown if the given <code>Panels</code> object is invalid
	 */
	public static void setPanel(Panels panel) throws IllegalArgumentException{
		if (panel != INVALID && panel != getCurrent()){
			for (int i = 0; i < values().length; i++){
//				if (values()[i] == panel) NLP.tabs.setSelectedIndex(NLP.tabs.indexOfTab(panel.name().substring(0, 1)+panel.name().substring(1).toLowerCase()));
			}
		}else throw new IllegalArgumentException("Invalid panel");
	}
	
	/**
	 * Adds a panel to the list of panels.
	 * 
	 * @param panel The <code>Panel</code> to be added
	 */
	public static void add(Panel panel){
		panels.put(panel.getClass().getSimpleName(), panel);
		Panels p = byClass(panel.getClass());
		
		if (p == getInitial()) setPanel(p);
	}
}
