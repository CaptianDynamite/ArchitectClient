package nlp.panel;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolTip;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.plaf.basic.DefaultMenuLayout;
import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;

import nlp.NLP;
import nlp.cmd.Commands;
import nlp.cmd.Description;
import nlp.cmd.PermissionCommand;
import nlp.net.Client;
import nlp.panel.comp.Message;
import nlp.panel.comp.MessageBuilder;
import nlp.panel.comp.TextComponents;
import nlp.panel.comp.MessageBuilder.UnbuiltMessageException;
import nlp.panel.comp.MessageHistory;

/**
 * A <code>Panel</code> that displays information about the program during runtime, and has the ability to execute commands.
 */
public class ConsolePanel extends Panel{
	
	private static final long serialVersionUID = 1L;
	
	private static final String AUTHOR_MSG = "Console written by regice202";
	
	public List<String> prevCommands = new ArrayList<String>();
	
	private List<String> prevTabOptions = new ArrayList<String>();	
	private List<String> prevInputs = new ArrayList<String>();
	
	private int cmdIndex = 0, inputIndex = 0, tabIndex = 0;
	private boolean cmdsEnabled, acceptInput = true, firstTab = true, isEditing;
	
	public HTMLDocument doc;
	private Element paragraph;
	
	private JTextPane console;
	private JTextField input;
	private JScrollPane scroll;
	
	private Dimension size;
	private final Color background = new Color(52, 57, 61), foreground = Color.WHITE, userInput = Color.GREEN, error = new Color(235, 20, 80);
	
	/**
	 * Constructs a new <code>ConsolePanel</code> with commands enabled.
	 * 
	 * @see nlp.panel.Panel
	 */
	public ConsolePanel(){
		this(true);
	}
	
	/**
	 * Constructs a new <code>ConsolePanel</code>.
	 * 
	 * @param cmdsEnabled Whether or not commands are enabled for this console
	 * 
	 * @see nlp.panel.Panel
	 */
	public ConsolePanel(boolean cmdsEnabled){
		this(null, cmdsEnabled);
	}
	
	/**
	 * Constructs a new <code>ConsolePanel</code>.
	 * 
	 * @param size The size of the console
	 * @param cmdsEnabled Whether or not commands are enabled for this console
	 * 
	 * @see nlp.panel.Panel
	 */
	public ConsolePanel(Dimension size, boolean cmdsEnabled){
		this(size, cmdsEnabled, NLP.maxSavedCmds, NLP.maxSavedInputs, NLP.maxSavedLines);
	}
	
	/**
	 * Constructs a new <code>ConsolePanel</code>.
	 * 
	 * @param size The size of the console
	 * @param cmdsEnabled Whether or not commands are enabled for this console
	 * @param maxSavedCmds The maximum amount of saved commands
	 * @param maxSavedInputs The maximum amount of saved changes to the input text field
	 * 
	 * @see nlp.panel.Panel
	 */
	public ConsolePanel(Dimension size, boolean cmdsEnabled, int maxSavedCmds, int maxSavedInputs, int maxSavedLines){
		try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			Font font = new Font("Consolas", Font.PLAIN, 12);
			setFont(font);
		}catch (Exception e){
			e.printStackTrace();
		}
		setFont(TextComponents.CONSOLE_PANEL_CONSOLE.getFixedFont(this, getFont()));
		this.cmdsEnabled = cmdsEnabled;
		NLP.maxSavedCmds = maxSavedCmds;
		NLP.maxSavedInputs = maxSavedInputs;
		NLP.maxSavedLines = maxSavedLines;
		
		c.gridx = 0;
		c.gridy = 1;
		
		if (cmdsEnabled){
			c.anchor = GridBagConstraints.PAGE_END;
			c.weightx = 1.0;
			c.weighty = 1.0;
			c.fill = GridBagConstraints.HORIZONTAL;
			
			input = new JTextField();
			input.setBackground(background);
			input.setForeground(foreground);
			input.setCaretColor(foreground);
			input.setFocusTraversalKeysEnabled(false);
			input.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					String text = input.getText();
	
					while (text.startsWith(" ")) text = text.substring(1);
					while (text.endsWith(" ")) text = text.substring(0, text.length()-1);
					
					if (!text.isEmpty()){
						println(text, false, false, Color.GREEN);
						input.setText(null);
						scroll.getVerticalScrollBar().setValue(scroll.getVerticalScrollBar().getMaximum());
						runCommand(text);
						prevCommands.add(text);
						if (prevCommands.size() > NLP.maxSavedCmds) prevCommands.remove(0);
						prevInputs.clear();
					}
					input.setText(null);
					cmdIndex = prevCommands.size()-1;
					inputIndex = 0;
				}
			});
			input.addKeyListener(new KeyListener(){
				public void keyPressed(KeyEvent e){
					if (prevCommands.size() > 0){
						if (e.getKeyCode() == KeyEvent.VK_UP){
							if (cmdIndex > 0 && input.getText() != null && !input.getText().isEmpty()) input.setText(prevCommands.get(--cmdIndex));
							else input.setText(prevCommands.get(cmdIndex));
						}else if (e.getKeyCode() == KeyEvent.VK_DOWN){
							if (cmdIndex < prevCommands.size()-1) input.setText(prevCommands.get(++cmdIndex));
							else input.setText(prevCommands.get(cmdIndex));
						}
					}
					
					if (e.getKeyCode() == KeyEvent.VK_TAB){
//						List<String> options = new ArrayList<String>();
						String text = input.getText();
						String[] split = text.split(" ");
						
						Commands command = Commands.byAlias(split[0]);
						if (text.endsWith(" ")){
							String[] fixed = new String[split.length+1];
							for (int i = 0; i < split.length; i++) fixed[i] = split[i];
							fixed[split.length] = "";
							split = fixed;
						}
						
//						for (String s : split){
//							debug("Split: '" + s + "'");
//						}
						
						if (firstTab) prevTabOptions = (text != null && command != null) ? 
								getMatching(command.onTabComplete(split), split[split.length-1]) : getMatching(Commands.HELP.onTabComplete(null), split[0]);
						
						if (prevTabOptions != null && !prevTabOptions.isEmpty()){
							if (firstTab && prevTabOptions.size() > 1){
								println(prevTabOptions.toString().substring(1).replace("]", ""), false, false);
								firstTab = false;
								input.setText(appendTabText(split, prevTabOptions.get(0)));
								tabIndex = 1;
							}else if (prevTabOptions.size() < 2) input.setText(appendTabText(split, prevTabOptions.get(0)));
							else{
								input.setText(appendTabText(split, prevTabOptions.get(tabIndex)));
								tabIndex += ((tabIndex+1) < prevTabOptions.size()) ? 1 : -tabIndex;
							}
						}
					}else if (!firstTab) firstTab = true;
					
					if (e.isControlDown()){
						if (prevInputs.size() > 0){
							if (e.getKeyCode() == KeyEvent.VK_Z){
								acceptInput = false;
								if (inputIndex > 0 && input.getText() != null && !input.getText().isEmpty()) input.setText(prevInputs.get(--inputIndex));
								else input.setText(prevInputs.get(inputIndex));
							}else if (e.getKeyCode() == KeyEvent.VK_Y){
								acceptInput = false;
								if (inputIndex < prevInputs.size()-1) input.setText(prevInputs.get(++inputIndex));
								else input.setText(prevInputs.get(inputIndex));
							}
						}
					}
					acceptInput = true;
				}
				public void keyTyped(KeyEvent e){}
				public void keyReleased(KeyEvent e){}
			});
			
			add(input);
		}
		
		c.gridy--;
		c.gridheight = 2;
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(0, 0, input.getMinimumSize().height, 0);
		
		console = new JTextPane(){
			private static final long serialVersionUID = 1L;
			
//			@Override
//			public JToolTip createToolTip(){
//				return test;
//			}
//			
//			@Override
//			public String getToolTipText(){
//				return (popup != null) ? "Heyo! ;P" : super.getToolTipText();
//			}
		};
		ToolTipManager.sharedInstance().registerComponent(console);
		console.setEditable(false);
		console.setContentType("text/html");
//		console.setCaretPosition(6); /* "<html>".length() */
		console.setMaximumSize(getPreferredSize());
		console.setBackground(background);
		console.setForeground(foreground);
		doc = (HTMLDocument) console.getStyledDocument();
		try{
			doc.insertAfterStart(doc.getDefaultRootElement().getElement(0), "<p id=\"messages\" font=\"#DEFAULT\" face=\""+getFont().getName()+"\" size-px=\""+getFont().getSize()
					+"\" style=\"margin-top: 0\"><font color=\"rgb("+userInput.getRed()+","+userInput.getGreen()+","+userInput.getBlue()+")\">"+AUTHOR_MSG+"</font><br></p>");
			paragraph = doc.getElement("messages");
			
			doc.addDocumentListener(new DocumentListener(){
				@Override
				public void removeUpdate(DocumentEvent e){}
				@Override
				public void insertUpdate(DocumentEvent e){}
				@Override
				public void changedUpdate(DocumentEvent e){}
			});
		}catch (Exception e){
			e.printStackTrace();
		}
//		console.addMouseMotionListener(new MouseMotionListener(){
//			@Override
//			public void mouseMoved(MouseEvent e){
//				Element msg = getElement(e);
//				
//				if (msg != null){
//					try{
//						String text = doc.getText(msg.getStartOffset(), (msg.getEndOffset()-msg.getStartOffset()));
//						String[] html = console.getText().split("\\</font>");
//						
//						for (int i = 0; i < html.length; i++){
//							if ((i+1) < html.length && !html[i].endsWith(AUTHOR_MSG)) ;
//						}
//					}catch (Exception ex){}
//				}
//			}
//			@Override
//			public void mouseDragged(MouseEvent e){}
//		});
		console.addMouseListener(new MouseListener(){
			@Override
			public void mouseReleased(MouseEvent e){
				if (e.isPopupTrigger()){
					try{
						Element msgElement = getElement(e);
						
//						System.out.println("msgElement: " + msgElement);
						Message msg = getMessage(msgElement);
						String text = doc.getText(msgElement.getStartOffset(), (msgElement.getEndOffset()-msgElement.getStartOffset()));
						
						if (text.trim().length() > 0 && !text.equals(AUTHOR_MSG)){
//							NLP.out.println("Msg: '" + msg.getText(ConsolePanel.this) + "'");
							JPopupMenu menu = new JPopupMenu(){
								private static final long serialVersionUID = 1L;
								
								@Override
								public void paintComponent(Graphics g){
									g.setColor(console.getBackground().darker());
									g.fillRect(0, 0, getWidth(), getHeight());
								}
							};
		//					menu.setBorder(BorderFactory.createSoftBevelBorder(BevelBorder.RAISED, console.getBackground().darker(), console.getBackground().darker().darker()));
							menu.setBorder(BorderFactory.createEmptyBorder());
							menu.setOpaque(false);
							menu.setFont(console.getFont());
							
							if (msg.isEditable){
								JButton edit = new JButton("Edit");
								edit.setBorderPainted(false);
								edit.setFocusPainted(false);
								edit.setContentAreaFilled(false);
								edit.setForeground(console.getForeground());
								edit.addActionListener(new ActionListener(){
									@Override
									public void actionPerformed(ActionEvent a){
										isEditing = true;
										menu.setVisible(false);
									}
								});
								menu.add(edit);
							
								JButton delete = new JButton("Delete");
								delete.setBorderPainted(false);
								delete.setFocusPainted(false);
								delete.setContentAreaFilled(false);
								delete.setForeground(console.getForeground());
								delete.addActionListener(new ActionListener(){
									@Override
									public void actionPerformed(ActionEvent a){
										MessageHistory.removeMessage(msg.getId());
										removeMessage(msg);
										menu.setVisible(false);
									}
								});
								menu.add(delete);
								
								JSeparator split = new JSeparator(JSeparator.HORIZONTAL);
								split.setOpaque(true);
								split.setBackground(console.getBackground().darker().darker());
								menu.add(split);
							}
							
							JButton copyId = new JButton("Copy ID");
							copyId.setBorderPainted(false);
							copyId.setFocusPainted(false);
							copyId.setContentAreaFilled(false);
							copyId.setFont(console.getFont());
							copyId.setForeground(console.getForeground());
							copyId.addActionListener(new ActionListener(){
								@Override
								public void actionPerformed(ActionEvent a){
									Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(String.valueOf(msg.getId())), null);
									menu.setVisible(false);
								}
							});
							menu.add(copyId);
							
							menu.show(console, e.getX(), e.getY());
							NLP.out.println(msg.getId());
						}
					}catch (Exception ex){}
				}
			}
			@Override
			public void mouseEntered(MouseEvent e){}
			@Override
			public void mouseExited(MouseEvent e){}
			@Override
			public void mousePressed(MouseEvent e){}
			@Override
			public void mouseClicked(MouseEvent e){}
		});
//		System.out.println(console.getText());
		
		scroll = new JScrollPane(console);
		scroll.getVerticalScrollBar().setUnitIncrement(getFontMetrics(getFont()).getHeight()*2);
		add(scroll, c);
		
		input.getDocument().addDocumentListener(new DocumentListener(){
			@Override
			public void insertUpdate(DocumentEvent e){
				try{
					if (acceptInput){
						prevInputs.add(e.getDocument().getText(0, e.getDocument().getLength()));
						if (prevInputs.size() > NLP.maxSavedInputs) prevInputs.remove(0);
						inputIndex = prevInputs.size()-1;
					}
				}catch (Exception ex){}
			}
			@Override
			public void removeUpdate(DocumentEvent e){
				try{
					if (acceptInput){
						prevInputs.add(e.getDocument().getText(0, e.getDocument().getLength()));
						if (prevInputs.size() > NLP.maxSavedInputs) prevInputs.remove(0);
						inputIndex = prevInputs.size()-1;
					}
				}catch (Exception ex){}
			}
			@Override
			public void changedUpdate(DocumentEvent e){}
		});
		if (cmdsEnabled) input.requestFocusInWindow();
		
		if (NLP.useCustomPrintStreams){
			try{
				System.setOut(new PrintStream(System.out){
					@Override
					public void println(Object obj){
						print(obj);
					}
					
					@Override
					public void print(Object obj){
						try{
							if (obj instanceof MessageBuilder) throw new UnbuiltMessageException();
							else if (obj instanceof Object[] && ((Object[]) obj).length == 2 && ((Object[]) obj)[0] instanceof Message && ((Object[]) obj)[1] instanceof String) 
								ConsolePanel.this.print((Message) ((Object[]) obj)[0], ((String) ((Object[]) obj)[1]), false, false, false);
							else if (obj instanceof Description) ConsolePanel.this.print(((Description) obj).toString().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("  ", "&nbsp;&nbsp;"), false, false, true);
							else ConsolePanel.this.print(obj.toString().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("  ", "&nbsp;&nbsp;"), false, false, true);
						}catch (UnbuiltMessageException e){
							e.printStackTrace(NLP.err);
						}
					}
					
					@SuppressWarnings("unused")
					@Override
					public void write(byte[] b, int off, int len){
						String out = new String(b, off, len);

						if (!NLP.useCustomPrintStreams || NLP.debugCustomPrintStreams) NLP.out.write(b, off, len);
						else{
							SwingUtilities.invokeLater(new Runnable(){
					            @Override
					            public void run(){
					            	ConsolePanel.this.print(out.replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("  ", "&nbsp;&nbsp;"), false, false, true);
					            }
					        });
						}
					}
				});
				
				System.setErr(new PrintStream(System.err){
					String exception = "";
					boolean isException;
					
					@Override
					public void println(Object obj){
						print(obj);
					}
					
					@Override
					public void print(Object obj){
						if (obj instanceof Exception){
							isException = true;
							PrintableException e = new PrintableException((Exception) obj);
							e.printStackTrace(NLP.out);
						}
//						try{
//							byte[] data = obj.toString().getBytes();
//							write(data, 0, data.length);
//						}catch (Exception e) {
//							e.printStackTrace(NLP.err);
//						}
//						NLP.out.println(obj.getClass().getSimpleName());
					}
					
					@SuppressWarnings("unused")
					@Override
					public void write(byte[] b, int off, int len){
						String out = new String(b, off, len);
						
						if (!NLP.useCustomPrintStreams || NLP.debugCustomPrintStreams) NLP.err.write(b, off, len);
						else{
							SwingUtilities.invokeLater(new Runnable(){
					            @Override
					            public void run(){
					            	ConsolePanel.this.print(out, true, false, true);
					            }
					        });
						}
					}
				});
			}catch (Exception ex){
				ex.printStackTrace(NLP.err);
			}
		}
	}

	/**
	 * Prints text to the current line in the console.
	 * 
	 * @param text The text to be printed to the console
	 * @param error Whether or not this text is an error message
	 * @param ts Whether or not to print a time stamp for this message
	 */
	private void print(String text, boolean error, boolean ts){
		print(text, error, ts, console.getForeground());
	}
	
	/**
	 * Prints a new line of text in the console.
	 * 
	 * @param text The text to be printed to the console
	 * @param error Whether or not this text is an error message
	 * @param ts Whether or not to print a time stamp for this message
	 */
	public void println(String text, boolean error, boolean ts){
		print((text+"\n"), error, ts);
	}
	
	/**
	 * Prints text to the current line in the console.
	 * 
	 * @param text The text to be printed to the console
	 * @param error Whether or not this text is an error message
	 * @param ts Whether or not to print a time stamp for this message
	 * @param doEscChars Whether or not to parse escape characters
	 */
	public void print(String text, boolean error, boolean ts, boolean doEscChars){
		print(text, error, ts, doEscChars, console.getForeground());
	}
	
	/**
	 * Prints a <code>Message</code> to the current line in the console.
	 * 
	 * @param msg The message to be printed to the console
	 * @param error Whether or not this text is an error message
	 * @param ts Whether or not to print a time stamp for this message
	 * @param doEscChars Whether or not to parse escape characters
	 */
	public void print(Message msg, String text, boolean error, boolean ts, boolean doEscChars){
		print(msg, text, error, ts, doEscChars, console.getForeground());
	}
	
	/**
	 * Prints text to the current line in the console.
	 * 
	 * @param text The text to be printed to the console
	 * @param error Whether or not this text is an error message
	 * @param ts Whether or not to print a time stamp for this message
	 * @param doEscChars Whether or not to parse escape characters
	 * @param color The color of the text to be printed
	 */
	private void print(String text, boolean error, boolean ts, boolean doEscChars, Color color){
		Object[] builtMsg = MessageBuilder.createNewBuilder(!error).add(text).build();
		print((Message) builtMsg[0], (String) builtMsg[1], error, ts, doEscChars, color);
	}
	
	/**
	 * Prints a <code>Message</code> to the current line in the console.
	 * 
	 * @param msg The message to be printed to the console
	 * @param error Whether or not this text is an error message
	 * @param ts Whether or not to print a time stamp for this message
	 * @param doEscChars Whether or not to parse escape characters
	 * @param color The color of the text to be printed
	 */
	private void print(Message msg, String text, boolean error, boolean ts, boolean doEscChars, Color color){
		try{
			NLP.out.println("text(" + doEscChars + "): '" + text + "'");
			String timestamp = "[" + new SimpleDateFormat("HH:mm:ss.ms").format(new Timestamp(new Date().getTime())) + "] ";
			
			if (error) color = this.error;
			else if (color == null) color = foreground;
			
			if (ts){
				Object[] builtMsg = MessageBuilder.createNewBuilder().add(timestamp).add(msg).build();
				msg = (Message) builtMsg[0];
				text = (String) builtMsg[1];
				console.setForeground(foreground);
			}
			
			if (text.replaceAll("\\n", "").replaceAll("\\r", "").length() > 0){
				String html = "";
				int index = 0;
				for (Message current = msg; true; current = current.getPrevious()){
					html = (current.getPopup() != null) ? "<a href=\""+index+"\" style=\"text-decoration: none;\">"+text.substring(current.getOffset(), (current.getOffset()+current.length()))+"</a>"+html :
						text.substring(current.getOffset(), (current.getOffset()+current.length()))+html;
					if (current.getPrevious() == null){
						html = "<span style=\"color: rgb("+color.getRed()+","+color.getGreen()+","+color.getBlue()+")\" id=\""+current.getId()+"\">"+html+"</span>";
						text = escapeCharacters(html);
						break;
					}
					index++;
				}
			}else{
				Message prevMessage = MessageHistory.getLastMessage();
				
				if (prevMessage != null){
					Object[] values = MessageBuilder.createNewBuilder().add(prevMessage.getText(this)).add(escapeCharacters(text)).build();
					msg = (Message) values[0];
					text = (String) values[1];
					
					NLP.out.println("New text: '" + text + "'");
					doc.removeElement(doc.getElement(String.valueOf(prevMessage.getId())));
					MessageHistory.removeMessage(prevMessage.getId());
					print(msg, text, error, ts, doEscChars, color);
					return;
				}
			}
			NLP.out.println("text: " + text);
			
			try{
				doc.insertBeforeEnd(paragraph, text);
			}catch (Exception e){
				e.printStackTrace(NLP.err);
			}
			NLP.out.println(console.getText());
			MessageHistory.addMessage(msg);
//			NLP.tabs.repaint();
		}catch (Exception e){
			e.printStackTrace(NLP.err);
		}
	}
	
	/**
	 * Prints text to the current line in the console.
	 * 
	 * @param text The text to be printed to the console
	 * @param error Whether or not this text is an error message
	 * @param ts Whether or not to print a time stamp for this message
	 * @param color The color of the text to be printed
	 */
	private void print(String text, boolean error, boolean ts, Color color){
		print(text, error, ts, false, color);
	}
	
	/**
	 * Prints a new line of text in the console.
	 * 
	 * @param text The text to be printed to the console
	 * @param error Whether or not this text is an error message
	 * @param ts Whether or not to print a time stamp for this message
	 * @param color The color of the text to be printed
	 */
	public void println(String text, boolean error, boolean ts, Color color){
		print((text+"\n"), error, ts, color);
	}
	
	/**
	 * Prints a debug message to the console.
	 * 
	 * @param msg The debug message to be printed
	 * 
	 * @see #println(String, boolean, boolean, Color)
	 */
	public void debug(String msg){
//		print("debug: ", false, true);
		println(msg, false, true, new Color(85, 85, 255));
	}
	
	/**
	 * Prints a warning message to the console.
	 * 
	 * @param warning The warning message to be printed
	 * 
	 * @see #println(String, boolean, boolean, Color)
	 */
	public void warn(String warning){
//		print("warn: ", false, true);
		println(warning, false, true, Color.YELLOW);
	}
	
	/**
	 * Runs commands.<p>
	 * 
	 * Handles whether or not the command is valid, prints why they're invalid if they are;
	 * checks the permissions of the <code>Client</code> to see if the <code>Client</code> can run it.
	 * 
	 * @param input The input from the console
	 */
	public void runCommand(String input){
		Commands cmd = Commands.byAlias(input.split(" ")[0]);
		String[] args = (input.split(" ").length > 1) ? input.substring(input.split(" ")[0].length()+1).split(" ") : null;
		
		if (args != null){
			List<String> newArgs = new ArrayList<String>();
			for (String arg : args){
				if (!arg.isEmpty()) newArgs.add(arg);
			}
			args = newArgs.toArray(new String[newArgs.size()]);
		}

		if (cmd != null && Client.hasPermission(cmd.getPermsRawValue())){
			try{
				if (!(cmd.get() instanceof PermissionCommand) && cmd != Commands.YES_NO) cmd.execute(args);
				else cmd.execute(input.split(" "));
			}catch (Exception e){
				e.printStackTrace();
			}
		}else if (Client.hasPermission(Commands.HELP.getPerm())){
				System.err.println("Error: Invalid command!");
				runCommand(Commands.HELP.getCommand());
		}else System.out.println("How did i getBool here?...");
	}
	
	public void removeMessage(Message msg){
		try{
			String[] html = console.getText().split("<span id=\"" + String.valueOf(msg.getId()) + "\">");
			String newHtml = html[0];
			
			newHtml += html[1].substring(html[1].split("</span>")[0].length()+7);
			
			console.setText(newHtml);
			paragraph = doc.getElement("messages");
			NLP.out.println(console.getText());
		}catch (Exception e){
			e.printStackTrace(NLP.err);
		}
	}
	
	/**
	 * Clears the console of all text.
	 */
	public void clearConsole(){
		try{
			doc.remove(0, doc.getLength());
			paragraph = doc.getElement("messages");
		}catch (Exception e){
			e.printStackTrace(NLP.err);
		}
	}
	
	/**
	 * Sets the visibility of the <code>ConsolePanel</code>.<br>
	 * Requests the focus in the window be given to the input text field.
	 */
	@Override
	public void setVisible(boolean isVisible){
		super.setVisible(isVisible);
		input.requestFocusInWindow();
	}
	
	/**
	 * Requests the focus in the window be given to the input text field.
	 */
	@Override
	public boolean requestFocusInWindow(){
		return input.requestFocusInWindow();
	}
	
	/**
	 * Returns a list of matching options based on the given argument.<br>
	 * Matching is based on if the option starts with the given arguemnt.
	 * 
	 * @param options The options to choose from
	 * @param arg The argument to match the options to
	 * @return a list of matching options based on the given argument
	 */
	private List<String> getMatching(List<String> options, String arg){
		List<String> matching = new ArrayList<String>();
		
//		debug("Arg: '" + arg + "'");
		if (options != null && !options.isEmpty()){
			if (!arg.equals("")){
				for (String option : options){
//					debug("Option: " + option);
					if (!option.toLowerCase().equals(arg.toLowerCase()) && option.toLowerCase().startsWith(arg.toLowerCase())){
						matching.add(option);
					}
				}
			}else matching = options;
		}
//		debug("Matches: " + matching.toString().substring(1).replace("]", ""));
		return matching;
	}
	
	public Element getElement(MouseEvent e){
//		NLP.out.println(console.getUI().viewToModel(console, e.getPoint()) + " | " + doc.getLength());
		return paragraph.getElement(paragraph.getElementIndex(console.getUI().viewToModel(console, e.getPoint())));
	}
	
	public Message getMessage(Element msg){
		try{
			String text = doc.getText(msg.getStartOffset(), (msg.getEndOffset()-msg.getStartOffset()));
			
			int index = 0;
			for (int i = 0; i < paragraph.getElementCount(); i++){
				Element e = paragraph.getElement(i);
				
				if (e.equals(msg)) break;
				else if (doc.getText(e.getStartOffset(), (e.getEndOffset()-e.getStartOffset())).equals(text)) index++;
			}
			
			String html[] = console.getText().replaceAll("\\n", "").replaceAll("  ", "").split("</span>");
			for (String piece : html){
				try{
					Message m = MessageHistory.getMessage(Long.parseLong(piece.split("span id=")[1].substring(1).split("\"")[0]));
					if (m.getText(this).equals(text)){
						if (index == 0) return m;
						index--;
					}
				}catch (Exception e){}
			}
		}catch (Exception e){
			e.printStackTrace(NLP.err);
		}
		return null;
	}
	
	@Override
	public Dimension getPreferredSize(){
		if (size != null) return size;
		
		Insets ins = NLP.mainFrame.getInsets();
		return new Dimension(NLP.mainFrame.getWidth()-(ins.left+ins.right), NLP.mainFrame.getHeight()-(ins.top+ins.bottom));
	}
	
	public String escapeCharacters(String text){
		String fixed = "";
		
		for (int i = 0; i < text.length(); i++){
			String charName = Character.getName((int) text.charAt(i));
			
			fixed += (charName.equals("LINE FEED (LF)")) ? "<br>" : ((!charName.equals("CARRIAGE RETURN (CR)")) ? String.valueOf(text.charAt(i)) : "");
		}
		return fixed;
	}
	
	/**
	 * Returns the appended input text line with the matching option.
	 * 
	 * @param in The input text field text split by a single space
	 * @param opt The option to append to the input text field
	 * @return the new input text field text
	 * 
	 * @see #getMatching(List, String)
	 */
	public String appendTabText(String[] in, String opt){
		String append = "";
		
		for (int i = 0; i < in.length-1; i++) append += in[i] + " ";
		return append + opt;
	}
	
	private static class PrintableException extends Exception{

		private static final long serialVersionUID = 1L;
		
		private Exception e;
		
		public PrintableException(Exception e){
			super(e);
			this.e = e;
			setStackTrace(e.getStackTrace());
		}
		
//		@Override
//		public StackTraceElement[] getStackTrace(){
//			List<StackTraceElement> elements = new ArrayList<StackTraceElement>();
//			StackTraceElement[] stackTrace = super.getStackTrace();
//			
//			for (int i = 0; i < stackTrace.length; i++){
////				if (i > 0) elements.add(stackTrace[i]);
//				elements.add(stackTrace[i]);
//			}
//			return elements.toArray(new StackTraceElement[elements.size()]);
//		}
		
		@Override
		public String toString(){
			return e.toString();
		}
	}

	/**
	 * Console and input text components.
	 * 
	 * @see nlp.panel.comp.TextComponents#CONSOLE_PANEL_CONSOLE
	 */
	@Override
	public int thingsToLoad(){
		return 1;
	}
}