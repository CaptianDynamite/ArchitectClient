package nlp.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.RoundRectangle2D;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;

import nlp.NLP;
import nlp.net.Client;
import nlp.net.LoginPacket;
import nlp.panel.Panel;
import nlp.panel.comp.ResizingComponent;
import nlp.panel.comp.TextComponents;
import nlp.util.Images;

/**
 * A <code>Panel</code> that displays components that are useful to a user trying to login to the server.
 */
public class LoginPanel extends Panel{

	private static final long serialVersionUID = 1L;
	
	private JPanel mainPanel;
	
	private JTextField user;
	private JPasswordField code;
	private JButton login;

	private final Color background = new Color(52, 57, 61, 245);
	private final Color textColor = background.brighter().brighter();
	
	/**
	 * Constructs a new <code>LoginPanel</code>.
	 * 
	 * @see nlp.panel.Panel
	 */
	public LoginPanel(){
		setBackground(new Color(0,0,0,0));
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.setPreferredSize(getPreferredSize());
		mainPanel.setOpaque(false);
		
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.LINE_END;
		
		JLabel userLabel = new JLabel("Username: ");
		userLabel.setFont(TextComponents.LOGIN_PANEL_LABEL.getFixedFont(this, userLabel));
		userLabel.setForeground(textColor);
		c.insets = new Insets(userLabel.getMinimumSize().height, 5, 5, 5);
		mainPanel.add(new ResizingComponent(userLabel), c);
		c.gridy++;
		
		JLabel passLabel = new JLabel("Password: ");
		passLabel.setFont(TextComponents.LOGIN_PANEL_LABEL.getFixedFont(this, passLabel));
		passLabel.setForeground(textColor);
		mainPanel.add(new ResizingComponent(passLabel), c);
		
		c.anchor = GridBagConstraints.LINE_START;
		c.gridx++;
		c.gridy = 0;
		
		user = new JTextField(NLP.USER_LENGTH);
		user.setFont(TextComponents.LOGIN_PANEL_LABEL.getFixedFont(this, user));
		user.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, getBackground().brighter(), getBackground().darker()));
		user.setBackground(background);
		user.setForeground(textColor);
		user.setFocusTraversalKeysEnabled(false);
		user.addKeyListener(new KeyListener(){
			public void keyPressed(KeyEvent e){
				if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_TAB) code.requestFocusInWindow();
			}
			public void keyReleased(KeyEvent e){}
			public void keyTyped(KeyEvent e){}
		});
		mainPanel.add(new ResizingComponent(user), c);
		
		code = new JPasswordField(NLP.USER_LENGTH);
		code.setFont(TextComponents.LOGIN_PANEL_LABEL.getFixedFont(this, code));
		code.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, getBackground().brighter(), getBackground().darker()));
		code.setBackground(background);
		code.setForeground(textColor);
		code.setFocusTraversalKeysEnabled(false);
		code.addKeyListener(new KeyListener(){
			public void keyPressed(KeyEvent e){
				if (e.getKeyCode() == KeyEvent.VK_TAB) login.requestFocusInWindow();
				else if (e.getKeyCode() == KeyEvent.VK_ENTER) login.doClick();
			}
			public void keyReleased(KeyEvent e){}
			public void keyTyped(KeyEvent e){}
		});
		c.gridy++;
		mainPanel.add(new ResizingComponent(code), c);
		
		c.gridx = 0;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		login = new JButton("Login");
		login.setFont(TextComponents.LOGIN_PANEL_LOGIN.getFixedFont(this, login));
		login.setFocusPainted(false);
		login.setBackground(new Color(85, 85, 255));
		login.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (user.getText().length() > 0 && !user.getText().isEmpty() && code.getPassword().length > 0 && !String.valueOf(code.getPassword()).isEmpty()) 
					Client.sendPacket(new LoginPacket(user.getText(), String.valueOf(code.getPassword())));
			}
		});
		c.insets = new Insets(login.getMinimumSize().height, 5, 5, 5);
		c.gridy++;
		mainPanel.add(new ResizingComponent(login), c);
		
		add(new ResizingComponent(mainPanel));
	}
	
	@Override
	public Dimension getPreferredSize(){
//		System.out.println("Login: " + new Dimension((int)(NLP.mainFrame.getWidth()*(1.0F/5F)), (int)(NLP.mainFrame.getHeight()*(2.0F/6F))) + " | mainFrame: " + NLP.mainFrame.getSize());
		return new Dimension((int)(NLP.mainFrame.getWidth()*(1.0F/5F)), (int)(NLP.mainFrame.getHeight()*(2.0F/6F)));
	}
	
	/**
	 * Paints the rounded rectangle background and all other components.
	 */
	@Override
	public void paintComponent(Graphics g){
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.drawImage(Images.BACKGROUND.get(NLP.mainFrame.getExtendedState() == JFrame.MAXIMIZED_BOTH), null, 0, 0);
		
		RoundRectangle2D panel = new RoundRectangle2D.Double(getComponent(0).getX(), getComponent(0).getY(), mainPanel.getWidth(), mainPanel.getHeight(), 20, 20);
		g2d.setColor(background);
		g2d.fill(panel);
	}
	
	/**
	 * Sets the visibility of the login panel.<br>
	 * Requests the focus in the window be given to the user text field.
	 */
	@Override
	public void setVisible(boolean isVisible){
		super.setVisible(isVisible);
		if (isVisible) user.requestFocusInWindow();
	}

	/**
	 * UserLabel, passLabel, user, code, and login text components.
	 * 
	 * @see TextComponents#LOGIN_PANEL_LABEL
	 * @see TextComponents#LOGIN_PANEL_LOGIN
	 */
	@Override
	public int thingsToLoad(){
		return 5;
	}
}
