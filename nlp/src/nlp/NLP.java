package nlp;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import nlp.desktop.Desktop;
import nlp.desktop.apps.Application;
import nlp.desktop.apps.Applications;
import nlp.net.Client;
import nlp.net.LogoutPacket;
import nlp.net.PingPacket;
import nlp.util.Images;
import nlp.util.Images.IconSizes;
import nlp.util.LoadingPanel;

/**
 * The main class of the Architect client.
 */
public class NLP{

	private static final NLP instance = new NLP();
	
	/* NLP constants */
	public static final PrintStream out = System.out;
	public static final PrintStream err = System.err;
	/* NLP variables */
	private static Dimension screen;
	private static Dimension prevSize;
	private static Timer dragTimer = getDragTimer();
	private static int prevState = JFrame.NORMAL;
	private static double dragRatioW;
	private static double dragRatioH;
	private static boolean hasMovedScreens = false;
	private static boolean isFullScreen = false;
	private static boolean isClosing = false;
	
	public static final JFrame mainFrame = new JFrame();
//	public static final DragTabPane tabs = new DragTabPane();
	public static final String TITLE = "Architect";
	public static final double ratio = 2.0/3;
	private static final boolean demoMode = false;
	
	/* LoadingPanel constants */
	public static LoadingPanel loading;
	
	/* Hidden app constants */
	public static boolean hideApps = true;
	
	/* Login app constants */
	public static final int USER_LENGTH = 12;
	public static final int CODE_LENGTH = 32;
	
	/* Console app constants */
	public static final boolean useTimestamps = false;
	public static final boolean useCustomPrintStreams = true;
	public static final boolean debugCustomPrintStreams = false;
	/* Console app variables */
	public static int maxSavedCmds = 5;
	public static int maxSavedInputs = 10;
	public static int maxSavedLines = 36;
	
	/**
	 * Creates the main display also know as the {@link #mainFrame} then loads everything.
	 * 
	 * @param args The arguments the program started with
	 * 
	 * @see nlp.util.LoadingPanel#load()
	 */
	public static void main(String[] args){
		new NLP();
		DevModeArgs.loadDevModeArgs(args);
		
		ToolTipManager.sharedInstance().setInitialDelay(0);
		ToolTipManager.sharedInstance().setReshowDelay(0);
		
		GraphicsConfiguration screenConfig = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
		if (DevModeArgs.SCREEN.getInt() >= 0 && DevModeArgs.SCREEN.getInt() < GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices().length)
			screenConfig = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[DevModeArgs.SCREEN.getInt()].getDefaultConfiguration();
		
		JFrame mover = new JFrame(screenConfig);
		screen = mover.getGraphicsConfiguration().getBounds().getSize();
		mainFrame.setPreferredSize(new Dimension((int)(screen.width*ratio), (int)(screen.height*ratio)));
		mainFrame.pack();
		mainFrame.setLocationRelativeTo(mover);
		
		loading = new LoadingPanel();
		loading.setLocationRelativeTo(mover);
		loading.load();
		
		mover.dispose();
	}
	
	/**
	 * Initializes the settings of the {@link #mainFrame}, adds the applications to {@link nlp.desktop.Desktop#apps}, and starts the client.
	 * 
	 * @see nlp.desktop.Desktop
	 * @see nlp.desktop.TaskBar
	 * @see nlp.net.Client
	 */
	public static void initialize(){
		System.out.println("mainFrame: " + mainFrame.getSize().toString());
		System.out.println("screen: " + screen.toString());
		
		List<BufferedImage> icons = new ArrayList<BufferedImage>();
		for (IconSizes size : IconSizes.values()){
			icons.add(Images.iconSets.get(Images.MAIN_ICON).get(size));
		}
		mainFrame.setIconImages(icons);
		mainFrame.setTitle(TITLE);
//		mainFrame.setUndecorated(true);
//		mainFrame.setAlwaysOnTop(true);
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(new WindowAdapter(){
	        @Override
	        public void windowClosing(WindowEvent event){
	            cleanExit();
	        }
	    });
		mainFrame.addComponentListener(new ComponentAdapter(){
			@Override
			public void componentMoved(ComponentEvent e){
				dragTimer.restart();
			}
		});
		mainFrame.getRootPane().registerKeyboardAction(toggleFullScreen(KeyEvent.VK_F11), KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
		mainFrame.getRootPane().registerKeyboardAction(toggleFullScreen(KeyEvent.VK_ESCAPE), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
		
//		try{
//			for (int i = 0; i < Panels.values().length; i++){
//				Panels p = Panels.values()[i];
//				
//				if (p != Panels.INVALID){
//					Panel panel = (Panel) p.getPanelClass().newInstance();
//					String title = p.name().substring(0, 1) + p.name().substring(1, p.name().length()).toLowerCase();
//					
//					tabs.addTab(title, panel);
//					tabs.setTabComponentAt(i, new TabComponent(tabs.getTabPlacement(), tabs.getTitleAt(i)));
//					
//					try{
//						Panels.add(panel);
//					}catch (IllegalArgumentException e){
//						if (!e.getMessage().equalsIgnoreCase("Invalid panel")) throw e;
//					}
//				}
//			}
//		}catch (Exception e){
//			e.printStackTrace(err);
//		}
//		mainFrame.add(tabs);
		Desktop.loadApplications();
		mainFrame.setContentPane(Desktop.getDesktop());
//		System.out.println("Threads: " + Arrays.toString(Thread.getAllStackTraces().keySet().toArray()));
	}
	
	public static void start(){
		mainFrame.setVisible(true);
		
		new Client().start();
		Client.sendPacket(new PingPacket());
		
//		for (Packets packet : Packets.values()){
//			System.out.println(packet.name() + ".isRegistered(): " + packet.isRegistered());
//		}
		
//		if (demoMode){
//			new Timer(15000, e -> {
//				int index = (tabs.getSelectedIndex() == tabs.getTabCount()-1) ? 0 : tabs.getSelectedIndex()+1;
//				Panels.setPanel(Panels.values()[index]);
//			}).start();
//		}
		/* Testing area */
		
//		System.out.println("hasPermissibleRank(" + Ranks.DEVELOPER.getName() + "): " + Client.hasPermissibleRank(Ranks.DEVELOPER));
//		System.out.println("ratio: " + ratio);
//		System.out.println("Perms: " + Permissions.ALL_PERMISSIONS + " | " + Ranks.ADMINISTRATOR.getDefaultPermissions());
//		Client.sendPacket(new LoginPacket("J4xM895", "test"));
//		Panels.setPanel(Panels.CONSOLE);
//		try{
//			Integer.parseInt("a");
//		}catch (Exception e){
//			e.printStackTrace();
//			Client.sendPacket(new ExceptionPacket(e));
//		}
		
//		Timer delayed = new Timer(1000, e -> {
//			JPanel conPanel = Panels.CONSOLE.getBool();
//			BufferedImage console = new BufferedImage(conPanel.getWidth(), conPanel.getHeight(), BufferedImage.TYPE_INT_ARGB);
//			Graphics2D g = console.createGraphics();
//			conPanel.paint(g);
//			try {
//				ImageIO.write(console, "png", new File(FileSystemView.getFileSystemView().getHomeDirectory(), "console.png"));
//			}catch (Exception ex) {
//				ex.printStackTrace();
//			}
//			g.dispose();
//		});
//		delayed.setRepeats(false);
//		delayed.start();
	}
	
	/**
	 * Cleans up the <code>Client</code> before exiting.
	 */
	public static void cleanExit(){
		isClosing = true;
		System.setOut(out);
		System.setErr(err);
		loading.dispose();
		mainFrame.dispose();
		
		try{
			if (Client.hasLoggedIn()) Client.sendPacket(new LogoutPacket());
			
			Timer timer = new Timer(Client.TIMEOUT, new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e){}
			});
			timer.setRepeats(false);
			timer.start();
			
			while (Client.hasLoggedIn() && timer.isRunning());
		}catch (Exception e){
			e.printStackTrace();
		}
		if (Client.socket != null && !Client.socket.isClosed()) Client.socket.close();
		System.exit(0);
	}
	
	public static Dimension getScreen(){
		return screen;
	}
	
	public static boolean isClosing(){
		return isClosing;
	}
	
	private static ActionListener toggleFullScreen(int keyCode){
		return new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				if (keyCode == KeyEvent.VK_F11 || isFullScreen){
					if (!isFullScreen){
						prevState = mainFrame.getExtendedState();
						prevSize = mainFrame.getSize();
					}
					
					mainFrame.dispose();
					mainFrame.setUndecorated(!isFullScreen);
					mainFrame.setVisible(true);
					
					if (!isFullScreen) mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					else{
						mainFrame.setExtendedState(prevState);
						mainFrame.setSize(prevSize);
						mainFrame.setLocationRelativeTo(null);
					}
					isFullScreen = !isFullScreen;
				}
			}
		};
	}
	
	private static Timer getDragTimer(){
		Timer timer = new Timer(50, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				Rectangle bounds = mainFrame.getGraphicsConfiguration().getBounds();
				
				if (screen.width != bounds.width || screen.height != bounds.height){
					dragRatioW = bounds.getWidth()/screen.width;
					dragRatioH = bounds.getHeight()/screen.height;
					screen = bounds.getSize();
					hasMovedScreens = true;
				}
				
				bounds = MouseInfo.getPointerInfo().getDevice().getDefaultConfiguration().getBounds();
				if (hasMovedScreens && screen.width == bounds.width && screen.height == bounds.height){
					JFrame mover = new JFrame(mainFrame.getGraphicsConfiguration());
					
					mainFrame.setPreferredSize(new Dimension((int)(screen.width*ratio), (int)(screen.height*ratio)));
					mainFrame.pack();
					mainFrame.setLocationRelativeTo(mover);
					hasMovedScreens = false;
					mover.dispose();
					
					for (Applications apps : Applications.values()){
						if (!apps.isHiddenApp())
							apps.getApp().getAppFrame().setSize((int) (apps.getApp().getAppFrame().getWidth()*dragRatioW), (int) (apps.getApp().getAppFrame().getHeight()*dragRatioH));
					}
				}
			}
		});
		timer.setRepeats(false);
		return timer;
	}
	
	public enum DevModeArgs{
		DEV_MODE(false), CULL_ASSETS(false), ALLOW_FORCED(true), SCREEN(0);
		
		private Class cls;
		private Object value;
		
		DevModeArgs(Boolean defaultValue){
			this.value = defaultValue;
			cls = Boolean.class;
		}
		
		DevModeArgs(Integer defaultValue){
			this.value = defaultValue;
			cls = Integer.class;
		}
		
		public Object get(){
			return value;
		}
		
		public Boolean getBool(){
			if (cls.equals(Boolean.class)) return (Boolean) value;
			return null;
		}

		public Integer getInt(){
			if (cls.equals(Integer.class)) return (Integer) value;
			return null;
		}
		
		static void loadDevModeArgs(String[] args){
			if (args != null && args.length > 0){
				args = args[0].split("\\|");
				
				for (int i = 0; i < values().length; i++){
					DevModeArgs devArg = values()[i];
					
					try{
						String val = args[i];
						
						switch(devArg.cls.getSimpleName()){
							case "Boolean":
								devArg.value = ((boolean) devArg.value) ? val.equals("0") : val.equals("1");
								break;
							case "Integer":
								devArg.value = Integer.parseInt(val);
								break;
							default:
								devArg.value = val;
								break;
						}
					}catch (Exception e){}
					System.out.println(devArg + ": " + devArg.value);
				}
			}
		}
	}
}
