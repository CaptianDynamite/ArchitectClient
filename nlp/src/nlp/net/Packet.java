package nlp.net;

/**
 * Represents a packet, which contains data in the form of bytes.
 * This data is information relevant to either the server or the <code>Client</code>.
 */
public abstract class Packet{
	
	private final int id;
	private final byte[] data;
	private final String[] info;
	
	private final boolean dcAfterSend;
	
	/**
	 * Creates a new packet from received data.
	 * 
	 * @param data The byte data
	 */
	@PacketConstruct
	public Packet(byte[] data){
		this.data = new String(data).trim().getBytes();
		info = new String(data).trim().split("~")[1].split("\\|");
		dcAfterSend = false;
		
		int tId = -1;
		try{
			tId = Integer.parseInt(new String(data).trim().split("~")[0]);
		}catch (Exception e){}
		this.id = tId;
	}
	
	/**
	 * Creates a new packet from the given objects.
	 * 
	 * @param objs The values to be converted into strings
	 */
	@PacketConstruct
	public Packet(Object... objs){
		this(false, objs);
	}
	
	/**
	 * Creates a new <code>ExceptionPacket</code>.
	 * 
	 * @param dcAfterSend Whether or not the <code>Client</code> should disconnect after sending the <code>ExceptionPacket</code>
	 * @param objs The values to be converted into strings
	 * 
	 * @see nlp.net.ExceptionPacket
	 */
	public Packet(boolean dcAfterSend, Object... objs){
		id = Packets.byClass(getClass()).getId();
		info = new String[objs.length];
		this.dcAfterSend = dcAfterSend;
		
		String msg = String.valueOf(id + "~");
		
		for (int i = 0; i < objs.length; i++){
			if (objs[i] != null && !String.valueOf(objs[i]).isEmpty()){
				info[i] = String.valueOf(objs[i]);
				msg += info[i] + (((i+1) != objs.length) ? "|" : "");
			}
		}
		this.data = msg.getBytes();
	}
	
	/**
	 * Returns the packet byte data.
	 * 
	 * @return the byte data of the packet
	 */
	public byte[] getData(){
		return data;
	}
	
	/**
	 * Returns the pieces of info in the packet.
	 * 
	 * @return the individual pieces of information in the packet
	 */
	public String[] getInfo(){
		return info;
	}
	
	/**
	 * Returns the packet id.
	 * 
	 * @return the id of the packet
	 */
	public int getId(){
		return id;
	}
	
	@Override
	public String toString(){
		return new String(data);
	}
	
	/**
	 * Returns whether or not it's registered.
	 * 
	 * @return whether or not the packet has been registered
	 */
	public boolean isRegistered(){
		return getClass() != Packet.class && Packets.byClass(getClass()).isRegistered();
	}
	
	/**
	 * Returns whether or not the <code>Client</code> should disconnect after sending.
	 * 
	 * @return whether or not the <code>Client</code> should disconnect after sending the packet
	 */
	public boolean dcAfterSend(){
		return dcAfterSend;
	}
	
	protected long parseLong(String data){
		try{
			long l = Long.parseLong(data);
			return l;
		}catch (Exception e){
			e.printStackTrace();
		}
		return -1L;
	}
}
