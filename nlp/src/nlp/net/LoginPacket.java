package nlp.net;

/**
 * A <code>Packet</code> that contains information about a user logging in to the server.
 */
public class LoginPacket extends Packet{

	private final String user;
	private final String code;
	
	private final boolean loginSuccess;
	
	/**
	 * Constructs a new <code>LoginPacket</code> from byte data.
	 * 
	 * @param data The received packet byte data
	 * 
	 * @see nlp.net.Packet#Packet(byte[])
	 */
	@PacketConstruct
	public LoginPacket(byte[] data){
		super(data);
		loginSuccess = Boolean.parseBoolean(getInfo()[0]);		
		user = (isBoolean(getInfo()[0])) ? getInfo()[1] : getInfo()[0];
		code = (isBoolean(getInfo()[0])) ? null : getInfo()[1];
	}

	/**
	 * Constructs a new <code>LoginPacket</code>.
	 * 
	 * @param user The user trying to login
	 * @param code The pass code for the given user
	 * 
	 * @see nlp.net.Packet#Packet(Object[])
	 */
	@PacketConstruct
	public LoginPacket(String user, String code){
		super(user, code);
		this.user = user;
		this.code = code;
		loginSuccess = false;
	}
	
	/**
	 * Returns the name of the user.
	 * 
	 * @return the name of the user
	 */
	public String getUser(){
		return user;
	}
	
	/**
	 * Returns the pass code.
	 * 
	 * @return the pass code of the user
	 */
	public String getCode(){
		return code;
	}
	
	/**
	 * Returns whether or not the user logged in.
	 * 
	 * @return whether or not the user successfully logged in
	 */
	public boolean hasLoggedIn(){
		return loginSuccess;
	}
	
	/**
	 * Returns whether or not the info is a boolean.
	 * 
	 * @param info A piece of info in the packet
	 * @return whether or not the piece of info is a boolean
	 */
	private boolean isBoolean(String info){
		return (info.equalsIgnoreCase("true") || info.equalsIgnoreCase("false"));
	}
}
