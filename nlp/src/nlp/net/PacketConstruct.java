package nlp.net;

import java.lang.annotation.*;

/**
 * Used to define <code>Packet</code> constructors.
 * 
 * @see nlp.net.Packet
 */
@Target({ElementType.CONSTRUCTOR})
@Retention(RetentionPolicy.RUNTIME)
public @interface PacketConstruct{}
