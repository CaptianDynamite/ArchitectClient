package nlp.net;

import java.util.Date;

/**
 * A <code>Packet</code> that contains information about the connection speed of the <code>Client</code> to the server.
 */
public class PingPacket extends Packet{
	
	private final String sendTime;
	
	/**
	 * Constructs a new <code>PingPacket</code> from byte data.
	 * 
	 * @param data The received packet byte data
	 * 
	 * @see nlp.net.Packet#Packet(byte[])
	 */
	@PacketConstruct
	public PingPacket(byte[] data){
		super(data);
		sendTime = new String(getInfo()[0]);
	}
	
	/**
	 * Constructs a new <code>PingPacket</code>.
	 * 
	 * @see nlp.net.Packet#Packet(Object[])
	 */
	@PacketConstruct
	public PingPacket(){
		super(new Date().getTime());
		sendTime = null;
	}
	
	/**
	 * Returns when the packet was sent.
	 * 
	 * @return the time the packet was sent
	 */
	public long getTimeSent(){
		return Long.parseLong(sendTime);
	}
}
