package nlp.net;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import nlp.NLP;

/**
 * A static list of <code>Packet</code> objects which handles various aspects of the <code>Packet</code>.
 * This includes whether or not a <code>Packet</code> has been correctly defined.
 */
public enum Packets{
	
	/** A <code>Packet</code> that contains information about a thrown exception. */
	EXCEPTION(0, ExceptionPacket.class), 
	/** A <code>Packet</code> that contains information about a user logging in to the server. */
	LOGIN(1, LoginPacket.class), 
	/** A <code>Packet</code> that contains information about a user logging off of the server. */
	LOGOUT(2, LogoutPacket.class),
	/** A <code>Packet</code> that contains information about a message sent to the server. */
	MESSAGE(3, MessagePacket.class),
	/** A <code>Packet</code> that contains information about a user's permissions. */
	PERMISSION(4, PermissionPacket.class),
	/** A <code>Packet</code> that contains information about the connection speed of the <code>Client</code> to the server. */
	PING(5, PingPacket.class),
	
	/** Any invalid packet. */
	INVALID(-1, Packet.class);
	
	private Class<? extends Packet> packet;
	private int id;
	private boolean isRegsitered;
	
	/**
	 * Registers new packets.<p>
	 * 
	 * Handles whether or not a <code>Packet</code> has been correctly defined and should be registered.
	 * 
	 * @param id The <code>Packet</code> id
	 * @param packet The <code>Packet</code> class
	 * 
	 * @see nlp.net.Packet
	 */
	Packets(int id, Class<? extends Packet> packet){
		this.id = id;
		this.packet = packet;
		int minConstructs = 0, validConstructs = 0, annotatedConsts = 0, finalFields = 0;
		
		try{
			for (int i = 0; i < Packet.class.getDeclaredConstructors().length; i++){
				Constructor<?> con = Packet.class.getDeclaredConstructors()[i];
				minConstructs += (con.isAnnotationPresent(PacketConstruct.class)) ? 1 : 0;
			}
			
			for (int i = 0; i < packet.getDeclaredConstructors().length; i++){
				Constructor<?> con = packet.getDeclaredConstructors()[i];
				if (con.isAnnotationPresent(PacketConstruct.class)){
					annotatedConsts++;
					if (con.getParameterCount() == 1 && con.getParameterTypes()[0].equals(byte[].class)) validConstructs++;
					if (i > 0 && annotatedConsts >= minConstructs) validConstructs++;
				}
			}
			
			for (Field f : packet.getDeclaredFields()){
				finalFields += ((f.getModifiers() & Modifier.FINAL) == Modifier.FINAL) ? 1 : 0;
			}
			this.isRegsitered = (validConstructs >= minConstructs && finalFields == packet.getDeclaredFields().length);
			
			if (!isRegsitered){
				if (annotatedConsts < minConstructs) throw new InvalidPacketConstructException(packet.getName() + ": Too few annotated constructors!");
				else if (finalFields < packet.getDeclaredFields().length) throw new InvalidPacketConstructException(packet.getName() + ": Not all declared fields are final!");
				else throw new InvalidPacketConstructException(packet.getName() + ": Invalid constructor parameters!");
			}
		}catch (InvalidPacketConstructException e){
			e.printStackTrace(NLP.err);
		}
	}
	
	/**
	 * Returns a <code>Packet</code> version of the <code>Packets</code> object.
	 * 
	 * @param data The byte data of the received packet
	 * @return a <code>Packet</code> object based on the given byte data
	 */
	Packet get(byte[] data){
		try{
			return packet.getConstructor(byte[].class).newInstance(data);
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Returns the name of the <code>Packets</code> object.
	 * 
	 * @return the class simple name of the <code>Packets</code> object
	 * 
	 * @see java.lang.Class#getSimpleName()
	 */
	private String getName(){
		return packet.getSimpleName();
	}
	
	/**
	 * Returns a <code>Packets</code> version of the given <code>Packet</code> class.
	 * 
	 * @param cls The <code>Packet</code> class
	 * @return a <code>Packets</code> object based on the given <code>Packet</code> class
	 */
	public static Packets byClass(Class<? extends Packet> cls){
		for (Packets value : values()){
			if (value != INVALID && value.getName().equals(cls.getSimpleName())) return value;
		}
		return INVALID;
	}
	
	/**
	 * Returns a <code>Packets</code> version of the given <code>Packet</code> byte data.
	 * 
	 * @param data The byte data of the <code>Packet</code>
	 * @return a <code>Packets</code> object based on the given <code>Packet</code> byte data
	 */
	public static Packets byData(byte[] data){
		try{
			String type = new String(data);
			int id = Integer.parseInt(type.split("~")[0]);
			return values()[id];
		}catch (Exception e){}
		return INVALID;
	}
	
	/**
	 * Returns whether or not the packet is registered.
	 * 
	 * @return whether or not the <code>Packets</code> object is registered
	 */
	public boolean isRegistered(){
		return isRegsitered;
	}
	
	/**
	 * Returns the id of the packet.
	 * 
	 * @return the id of the <code>Packets</code> object
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * An exception thrown if a <code>Packet</code> isn't properly defined.
	 * 
	 * @see nlp.net.Packet
	 * @see nlp.net.PacketConstruct
	 */
	private class InvalidPacketConstructException extends Exception{
		
		private static final long serialVersionUID = 1L;
		
		/**
		 * Constructs a new <code>InvalidPacketConstructException</code>.
		 * 
		 * @param exception How the <code>Packet</code> isn't properly defined
		 */
		public InvalidPacketConstructException(String exception) {
			super(exception);
		}
	}
}
