package nlp.net;

/**
 * A <code>Packet</code> that contains information about a user's permissions.
 */
public class PermissionPacket extends Packet{

	private final String user;
	private final long perms;
	
	/**
	 * Constructs a new <code>PermissionsPacket</code> from byte data.
	 * 
	 * @param data The received packet byte data
	 * 
	 * @see nlp.net.Packet#Packet(byte[])
	 */
	@PacketConstruct
	public PermissionPacket(byte[] data){
		super(data);
		user = Client.username;
		perms = parseLong(getInfo()[0]);
	}
	
	/**
	 * Constructs a new <code>PermissionsPacket</code>.<p>
	 * 
	 * Must pre-apply changes to the <i>perms</i> parameter to affect the permissions of the user on the server.<br>
	 * The <i>user</i> 'me', as well as 'null' will send the client permissions.
	 * 
	 * @param user The user to whom the permissions belong
	 * @param perms The user permissions contained inside the packet
	 * 
	 * @see nlp.net.Packet#Packet(Object[])
	 */
	@PacketConstruct
	public PermissionPacket(String user, long perms){
		super(user, perms);
		this.user = user;
		this.perms = perms;
	}
	
//	/**
//	 * Constructs a new <code>PermissionsPacket</code>.<p>
//	 * 
//	 * Must pre-apply changes to the <i>perms</i> parameter to affect the permissions of the user on the server.<br>
//	 * Used for sending the client permissions.
//	 * 
//	 * @param perms The client permissions contained inside the packet
//	 * 
//	 * @see nlp.net.Packet#Packet(Object[])
//	 */
//	@PacketConstruct
//	public PermissionPacket(long perms){
//		super(perms);
//	}
	
	/**
	 * Returns the user to whom the permissions belong.
	 * 
	 * @return The user to whom the contained permissions belong
	 */
	public String getUser(){
		return user;
	}
	
	/**
	 * Returns the user permissions contained inside the packet.
	 * 
	 * @return The user permissions contained inside the packet to possibly set as the user's current permissions
	 */
	public long getPermissions(){
		return perms;
	}
}
