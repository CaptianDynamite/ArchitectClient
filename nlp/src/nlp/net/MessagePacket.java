package nlp.net;

/**
 * A <code>Packet</code> that contains information about a message sent to the server.
 */
public class MessagePacket extends Packet{

	private final String text;
	private final long id;
	
	/**
	 * Constructs a new <code>MessagePacket</code> from byte data.
	 * 
	 * @param data The received packet byte data
	 * 
	 * @see nlp.net.Packet#Packet(byte[])
	 */
	@PacketConstruct
	public MessagePacket(byte[] data){
		super(data);
		text = getInfo()[0];
		id = parseLong(getInfo()[1]);
	}
	
	/**
	 * Constructs a new <code>MessagePacket</code>.
	 * 
	 * @see nlp.net.Packet#Packet(Object[])
	 */
	@PacketConstruct
	public MessagePacket(String text){
		super(text);
		this.text = text;
		id = -1L;
	}
	
	/**
	 * Returns the message text.
	 * 
	 * @return the text of the message
	 */
	public String getText(){
		return text;
	}
	
	/**
	 * Returns the message id.
	 * 
	 * @return the id of the message
	 */
	public long getMessageId(){
		return id;
	}
}
