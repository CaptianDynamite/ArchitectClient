package nlp.net;

import java.net.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import nlp.NLP;
import nlp.cmd.perm.Permissions;
import nlp.cmd.perm.Ranks;
import nlp.desktop.apps.Applications;
import nlp.desktop.apps.Console;
import nlp.desktop.apps.Login;
import nlp.panel.ConsolePanel;
import nlp.panel.Panels;
import nlp.panel.comp.Message;
import nlp.panel.comp.MessageBuilder;

/**
 * Represents a client, which sends and receives data on a separate <code>Thread</code> to the server.
 */
public class Client extends Thread{

	public static final int TIMEOUT = 3000;
	
	private final String SERVER_IP = "127.0.0.1";
	private static final int SERVER_PORT = 1365; /* MUST be higher than 1024! */
	
	private static InetAddress serverIP;
	public static DatagramSocket socket;
	
	public static String username;
	public static String userToLogin;
	public static String userToLoginCode;
	public static long permissions = ((Permissions.TRUSTED_PERMISSIONS | Permissions.VIEW_CONSOLE.getRawValue()) | Permissions.PROMOTE.getRawValue());
//	public static long permissions = Permissions.ALL_PERMISSIONS;
	
	public static Map<Ranks, String> users = new HashMap<Ranks, String>();
	
	/**
	 * Constructs a new <code>Client</code>.
	 */
	public Client(){
		try{
			serverIP = InetAddress.getByName(SERVER_IP);
			socket = new DatagramSocket();
			socket.connect(serverIP, SERVER_PORT);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * The main loop for the <code>Client</code> thread.
	 * 
	 * @see #receiveData()
	 */
	@Override
	public void run(){
		while(true){
			if (socket != null && !socket.isClosed()) receiveData();
		}
	}
	
	/**
	 * Receives all incoming data from the server.<p>
	 * 
	 * Handles all incoming packets and server connection issues.
	 * 
	 * @see nlp.net.Packet
	 * @see java.net.DatagramPacket
	 */
	private void receiveData(){
//		System.out.println("Waiting for data...");
		try{
			byte[] data = new byte[1024];
			DatagramPacket packet = new DatagramPacket(data, data.length);
			socket.receive(packet);
			
			Packets received = Packets.byData(packet.getData());
			switch(received){
				case INVALID:
					System.out.println("INVALID: " + new String(packet.getData()));
					break;
				case EXCEPTION:
					ExceptionPacket exception = new ExceptionPacket(packet.getData());
					exception.getException().printStackTrace();
					break;
				case LOGIN:
					LoginPacket login = new LoginPacket(packet.getData());
//					System.out.println(received.name() + ": user=" + login.getUser() + ", code=" + login.getCode());
					System.out.println("loggedIn: " + login.hasLoggedIn());
					
					if (login.hasLoggedIn()){
						username = login.getUser();
						userToLogin = null;
						userToLoginCode = null;
					}
					if (hasLoggedIn()){
						System.out.println("user: " + username);
						Applications.LOGIN.getApp().getAppFrame().setVisible(false);
						((Console) Applications.CONSOLE.getApp()).redirectStreams();
						NLP.mainFrame.setResizable(true);
					}
					break;
				case LOGOUT:
					username = null;
					((Console) Applications.CONSOLE.getApp()).redirectStreams();
					if (userToLogin != null && userToLoginCode != null) sendPacket(new LoginPacket(userToLogin, userToLoginCode));
					break;
				case MESSAGE:
					System.out.println(((Message) MessageBuilder.createNewBuilder().add(new MessagePacket(packet.getData()).getText()).build()[0]));
					break;
				case PING:
					PingPacket pong =  new PingPacket(packet.getData());
					System.out.println("Ping: " + String.valueOf(new Date().getTime()-pong.getTimeSent()) + "ms");
					break;
				default:
					System.out.println(received.name() + ": " + received.get(packet.getData()).toString());
					break;
			}
		}catch (PortUnreachableException e){
			System.err.println("Error: Couldn't connect to server!");
		}catch (Exception e){
			if (!NLP.isClosing() || (NLP.isClosing() && !(e instanceof SocketException))) e.printStackTrace();
		}
	}
	
	/**
	 * Sends a packet to the server.<p>
	 * 
	 * Handles whether or not the <code>Packet</code> is registered.
	 * 
	 * @param packet The <code>Packet</code> to be sent
	 * 
	 * @see #sendData(byte[])
	 */
	public static void sendPacket(Packet packet){
		if (packet.isRegistered()){
			sendData(packet.getData());
			if (packet.dcAfterSend()) socket.close();
		}else System.err.println("Error: " + packet.getClass().getName() + " is not a registered packet!");
	}
	
	/**
	 * Sends byte data to the server.
	 * 
	 * @param data The data to send to the server
	 */
	private static void sendData(byte[] data){
		try{
			DatagramPacket packet = new DatagramPacket(data, data.length, serverIP, SERVER_PORT);
			socket.send(packet);
//			System.out.println("Sent: " + new String(data));
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static boolean hasLoggedIn(){
		return username != null;
	}
	
	/**
	 * Returns whether or not the client has permission.
	 * 
	 * @param perm The permission to check the client for
	 * @return whether or not the client has permission for something
	 */
	public static boolean hasPermission(Permissions perm){
//		System.out.println(perm.getName() + ": (" + permissions + " & " + perm.getRawValue() + ") == " + perm.getRawValue() + " | " + (permissions & perm.getRawValue()));
		return (perm == Permissions.INVALID || (permissions & perm.getRawValue()) == perm.getRawValue());
	}
	
	public static boolean hasPermission(long perms){
		for (Permissions perm : Permissions.getPermissions(perms)){
			if (perm == Permissions.INVALID || (permissions & perm.getRawValue()) == perm.getRawValue()) return true;
		}
		return false;
	}
	
	/**
	 * Returns whether or not a user has a certain rank.
	 * 
	 * @param rank The rank to check the client for
	 * @return whether or not the client has the specified permissible rank
	 */
	public static boolean hasPermissibleRank(Ranks rank){
		return (permissions & rank.getDefaultPermissions()) == rank.getDefaultPermissions();
	}
}
