package nlp.net;

/**
 * A <code>Packet</code> that contains information about a thrown exception.
 */
public class ExceptionPacket extends Packet{

	private final String cause;
	private final StackTraceElement[] stackTrace;
	
	/**
	 * Constructs a new <code>ExceptionPacket</code> from byte data.
	 * 
	 * @param data The received packet byte data
	 * 
	 * @see nlp.net.Packet#Packet(byte[])
	 */
	@PacketConstruct
	public ExceptionPacket(byte[] data){
		super(data);
		
		cause = getInfo()[0];
		stackTrace = new StackTraceElement[getInfo().length-1];
		
		try{
			for (int i = 0; i < stackTrace.length; i++){
				String[] stack = getInfo()[i+1].split("\\(");
				String[] methodPath = stack[0].split("\\.");
				String className = stack[0].substring(0, (stack[0].length()-methodPath[methodPath.length-1].length())-1);
				String javaName = stack[1].split("\\.")[0] + ".java";
				int line = (stack[1].split(":").length > 1) ? Integer.parseInt(stack[1].split(":")[1].replace(")", "")) : -1;
				
				stackTrace[i] = new StackTraceElement(className, methodPath[methodPath.length-1], javaName, line);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Constructs a new <code>ExceptionPacket</code>.
	 * 
	 * @param e The <code>Exception</code> thrown
	 * @param dcAfterSend Whether or not the <code>Client</code> should disconnect after sending the packet
	 * 
	 * @see nlp.net.Packet#Packet(boolean, Object[])
	 */
	@PacketConstruct
	public ExceptionPacket(Exception e, boolean dcAfterSend){
		super(dcAfterSend, getException(e));
		cause = e.getCause().getMessage();
		stackTrace = e.getStackTrace();
	}
	
	/**
	 * Returns the <code>Exception</code>.
	 * 
	 * @return the exception in the packet
	 * 
	 * @see java.lang.Exception
	 */
	public Exception getException(){
		if (stackTrace != null) return new PacketException();
		return null;
	}
	
	/**
	 * Returns the <code>Exception</code> stack trace.<p>
	 * 
	 * Used to construct a <code>Packet</code> object.
	 * 
	 * @param e The <code>Exception</code> thrown
	 * @return the <code>Object[]</code> used to construct a <code>Packet</code>
	 */
	private static Object[] getException(Exception e){
		Object[] exception = new Object[e.getStackTrace().length+1];
		
		exception[0] = e.toString();
		for (int i = 1; i < exception.length; i++) exception[i] = e.getStackTrace()[i-1];
		return exception;
	}
	
	/**
	 * An <code>Exception</code> for the <code>ExceptionPacket</code>.<p>
	 * 
	 * Handles the printing of the stack trace.
	 * 
	 * @see nlp.net.ExceptionPacket
	 * @see java.lang.Exception
	 */
	private class PacketException extends Exception{

		private static final long serialVersionUID = 1L;

		@Override
		public void printStackTrace(){
			System.err.println(cause);
			for (StackTraceElement stack : stackTrace){
				System.err.println("	at " + stack);
			}
		}
	}
}
