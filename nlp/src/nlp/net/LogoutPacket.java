package nlp.net;

/**
 * A <code>Packet</code> that contains information about a user logging off of the server.
 */
public class LogoutPacket extends Packet{
	
	/**
	 * Constructs a new <code>LogoutPacket</code> from byte data.
	 * 
	 * @param data The received packet byte data
	 * 
	 * @see nlp.net.Packet#Packet(byte[])
	 */
	@PacketConstruct
	public LogoutPacket(byte[] data){
		super(data);
	}

	/**
	 * Constructs a new <code>LogoutPacket</code>.
	 * 
	 * @see nlp.net.Packet#Packet(Object[])
	 */
	@PacketConstruct
	public LogoutPacket(){}
}
